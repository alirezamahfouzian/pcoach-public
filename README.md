## Used technologies:

* Mvvm architecture
* Retrofit 2 and Gson	
* Dagger hilt
* Android Room
* Database Caching
* RxJava
* kotlin Coroutine
* Android navigation Component
* Leak canary
* LiveData
* Android material design
* Dark mode

## Description:

pCoach is an easy and simple app that can automatically generate personalized training and diet programs
based on user information and input.

## points:

This program has been developed for a startup that for some reasons never made it to the release phase
all codes, designs, Color types and etc... have been developed and designed by me. (xd files are available).

This program is still under development and is not fully finished and may have some bugs and in this version
all generated data are fake and is just for demonstration purposes.

## Screenshot:

Screenshots are placed in 'Screenshot' folder.
