package com.e.pcoach.repository;

import android.app.Application;

import com.e.pcoach.db.PcoachDataBase;
import com.e.pcoach.db.dao.UserGeneralInfoDao;
import com.e.pcoach.db.entities.UserGeneralInfoEntity;

import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class GetInfoRepository {

    private UserGeneralInfoDao mUserGeneralInfoDao;
    private CompositeDisposable mDisposable;

    public GetInfoRepository(Application application, CompositeDisposable disposable) {
        PcoachDataBase dataBase = PcoachDataBase.getInstance(application);
        mUserGeneralInfoDao = dataBase.userGeneralInfoDao();
        mDisposable = disposable;
    }

    public void insertGeneralInfo(UserGeneralInfoEntity generalInfoEntity) {
        Disposable disposable = Observable
                .create(emitter -> {
                    mUserGeneralInfoDao.insertGeneralInfo(generalInfoEntity);
                })
                .subscribeOn(Schedulers.io())
                .subscribe();
        mDisposable.add(disposable);
    }

    public void updateGeneralInfo(UserGeneralInfoEntity generalInfoEntity) {
        Disposable disposable = Observable
                .create(emitter -> {
                    mUserGeneralInfoDao.updateGeneralInfo(generalInfoEntity);
                })
                .subscribeOn(Schedulers.io())
                .subscribe();
        mDisposable.add(disposable);
    }

    public Maybe<UserGeneralInfoEntity> getGeneralInfo(int id) {
        return mUserGeneralInfoDao.getGeneralInfo(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Maybe<List<UserGeneralInfoEntity>> getAllGeneralInfo() {
        return mUserGeneralInfoDao.getAllGeneralInfo()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
