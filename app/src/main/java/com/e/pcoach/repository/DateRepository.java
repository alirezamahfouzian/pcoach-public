package com.e.pcoach.repository;

import android.app.Application;

import com.e.pcoach.db.PcoachDataBase;
import com.e.pcoach.db.dao.DateDao;
import com.e.pcoach.db.dao.UserGeneralInfoDao;
import com.e.pcoach.db.entities.UserGeneralInfoEntity;
import com.e.pcoach.db.entities.WorkoutDateEntity;

import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class DateRepository {

    private DateDao mDateDao;

    public DateRepository(Application application) {
        PcoachDataBase dataBase = PcoachDataBase.getInstance(application);
        mDateDao = dataBase.dateDao();
    }

    public Maybe<Integer> getWorkoutByDayInMonth(int positionInMonth) {
        return mDateDao.getWorkoutByDayInMonth(positionInMonth);
    }

}
