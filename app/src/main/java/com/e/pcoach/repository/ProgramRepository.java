package com.e.pcoach.repository;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.e.pcoach.db.PcoachDataBase;
import com.e.pcoach.db.dao.DietProgramDao;
import com.e.pcoach.db.dao.ProgramDao;
import com.e.pcoach.db.dao.WorkoutProgramDao;
import com.e.pcoach.db.entities.DietProgramEntity;
import com.e.pcoach.db.entities.ProgramsEntity;
import com.e.pcoach.db.entities.WorkoutProgramEntity;
import com.e.pcoach.ui.main.history.HistoryModel;
import com.e.pcoach.ui.main.model.MealModel;
import com.e.pcoach.ui.main.model.WorkoutModel;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ProgramRepository {
    private ProgramDao mProgramDao;
    private WorkoutProgramDao mWorkoutProgramDao;
    private DietProgramDao mDietProgramDao;
    private CompositeDisposable mDisposable;

    public ProgramRepository(Application application, CompositeDisposable disposable) {
        PcoachDataBase dataBase = PcoachDataBase.getInstance(application);
        mProgramDao = dataBase.programDao();
        mWorkoutProgramDao = dataBase.workoutProgramDao();
        mDietProgramDao = dataBase.dietProgramDao();
        mDisposable = disposable;
    }

    public void insertProgram(ProgramsEntity programsEntity) {
        Disposable disposable =
                mProgramDao.insertProgram(programsEntity)
                        .subscribeOn(Schedulers.io())
                        .subscribe();
        mDisposable.add(disposable);
    }

    public void updateProgram(ProgramsEntity programsEntity) {
        Disposable disposable =
                mProgramDao.updateProgram(programsEntity)
                        .subscribeOn(Schedulers.io())
                        .subscribe();
        mDisposable.add(disposable);
    }

    public LiveData<ProgramsEntity> getEnabledPrograms() {
        return mProgramDao.getEnabledPrograms();
    }

    public Maybe<ProgramsEntity> getEnabledProgramsObservable() {
        return mProgramDao.getEnabledProgramsObservable();
    }

    public Maybe<ProgramsEntity> getProgramById(int programId) {
        return mProgramDao.getProgramById(programId);
    }

    public Maybe<Integer> getLastProgramId() {
        return mProgramDao.getLastProgramId()
                .subscribeOn(Schedulers.io());
    }

    public Maybe<List<ProgramsEntity>> getAllPrograms() {
        return mProgramDao.getAllPrograms()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Maybe<List<ProgramsEntity>> getAllProgramsLimited() {
        return mProgramDao.getAllPrograms()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public LiveData<List<HistoryModel>> getHistoryPrograms() {
        return mProgramDao.getHistoryPrograms();
    }

    public void insertWorkoutProgram(WorkoutProgramEntity workoutProgramEntity) {
        Disposable disposable = mWorkoutProgramDao.insertWorkoutProgram(workoutProgramEntity)
                .subscribeOn(Schedulers.io())
                .subscribe();
        mDisposable.add(disposable);
    }

    public Maybe<List<WorkoutProgramEntity>> getWorkoutPrograms(int id) {
        return mWorkoutProgramDao.getWorkoutPrograms(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public LiveData<List<WorkoutModel>> getDayMoves(int dayPosition) {
        return mWorkoutProgramDao.getDayMoves(dayPosition);
    }

    public Maybe<List<WorkoutModel>> getDayMovesObservable(int dayPosition) {
        return mWorkoutProgramDao.getDayMovesObservable(dayPosition);
    }

    public LiveData<List<WorkoutModel>> getDayByProgramId(int dayPosition, int programId) {
        return mWorkoutProgramDao.getDayByProgramId(dayPosition, programId);
    }

    public Maybe<List<WorkoutProgramEntity>> getAllWorkoutPrograms() {
        return mWorkoutProgramDao.getAllWorkoutPrograms()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Completable insertDietProgram(DietProgramEntity dietProgramEntity) {
        Completable completable = mDietProgramDao.insertDietProgram(dietProgramEntity)
                .subscribeOn(Schedulers.io());
        Disposable disposable = completable.subscribe();
        mDisposable.add(disposable);
        return completable;
    }

    public LiveData<List<MealModel>> getDayMeals(int dayPosition) {
        return mDietProgramDao.getDayMeals(dayPosition);
    }

    public LiveData<List<MealModel>> getDayMealsByType(int dayPosition, int mealType) {
        return mDietProgramDao.getDayMealsByType(dayPosition, mealType);
    }

    public LiveData<List<MealModel>> getDayMealsByTypeByProgramId(int dayPosition, int programId) {
        return mDietProgramDao.getDayMealsByTypeByProgramId(dayPosition, programId);
    }

    public Maybe<List<DietProgramEntity>> getDietPrograms(int id) {
        return mDietProgramDao.getDietPrograms(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Maybe<List<DietProgramEntity>> getAllDietPrograms() {
        return mDietProgramDao.getAllDietPrograms()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public LiveData<List<Integer>> getDietProgramDays() {
        return mDietProgramDao.getDietProgramDays();
    }

    public LiveData<List<Integer>> getWorkoutProgramDays() {
        return mWorkoutProgramDao.getWorkoutProgramDays();
    }
    public LiveData<List<Integer>> getWorkoutProgramDaysById(int programId) {
        return mWorkoutProgramDao.getWorkoutProgramDaysById(programId);
    }

}
