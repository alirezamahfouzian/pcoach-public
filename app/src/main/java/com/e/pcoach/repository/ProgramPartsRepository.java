package com.e.pcoach.repository;

import android.app.Application;

import com.e.pcoach.db.PcoachDataBase;
import com.e.pcoach.db.dao.MealsDao;
import com.e.pcoach.db.dao.MovesDao;
import com.e.pcoach.db.entities.MealsEntity;
import com.e.pcoach.db.entities.MovesEntity;
import com.e.pcoach.db.entities.UserEntity;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class ProgramPartsRepository {

    private final CompositeDisposable mDisposable;
    private MovesDao movesDao;
    private MealsDao mealsDao;

    public ProgramPartsRepository(Application application, CompositeDisposable disposable) {
        PcoachDataBase dataBase = PcoachDataBase.getInstance(application);
        movesDao = dataBase.movesDao();
        mealsDao = dataBase.mealsDao();
        mDisposable = disposable;
    }

    public Maybe<MealsEntity> getMeal(int mealId) {
        return mealsDao.getMeal(mealId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Maybe<MovesEntity> getMove(int moveId) {
        return movesDao.getMove(moveId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public void insertMeals(MealsEntity mealsEntity) {
        mealsDao.insertMeals(mealsEntity);
    }

    public void insertMoves(MovesEntity movesEntity) {
        movesDao.insertMoves(movesEntity);
    }
}
