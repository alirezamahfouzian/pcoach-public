package com.e.pcoach.repository;

import android.app.Application;

import com.e.pcoach.db.PcoachDataBase;
import com.e.pcoach.db.dao.UserDao;
import com.e.pcoach.db.entities.UserEntity;

import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class UserRepository {
    private UserDao mUserDao;
    private CompositeDisposable mDisposable;

    public UserRepository(Application application, CompositeDisposable disposable) {
        mDisposable = disposable;
        PcoachDataBase dataBase = PcoachDataBase.getInstance(application);
        mUserDao = dataBase.userDao();
    }

    public void insertUser(UserEntity userEntity) {
        Disposable disposable = Observable
                .create(emitter -> {
                    mUserDao.insertUser(userEntity);
                })
                .subscribeOn(Schedulers.io())
                .subscribe();
        mDisposable.add(disposable);
    }

    public void updateUser(UserEntity userEntity) {
        Observable.create(emitter -> {
            mUserDao.updateUser(userEntity);
        }).subscribeOn(Schedulers.io())
                .subscribe(new Observer<Object>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        mDisposable.add(d);
                    }

                    @Override
                    public void onNext(Object o) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public Maybe<UserEntity> getUser() {
        return mUserDao.getUser()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
