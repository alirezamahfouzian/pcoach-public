package com.e.pcoach.ui.getinfo.fragments.bottomsheet;

public class ExperienceType {

    public static final int NONE = -1;
    public static final int BEGINER = 0;
    public static final int LITLE_PRO = 1;
    public static final int PRO = 2;
    public static final int SUPER_PRO = 3;
}
