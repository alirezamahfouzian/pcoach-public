package com.e.pcoach.ui.helper

import android.graphics.Color
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.Nullable
import com.e.pcoach.R

class AddEditTextFocusEffect(
        @Nullable var mEditTextName: EditText? = null,
        @Nullable var mTextViewNameHint: TextView? = null,
        @Nullable var mEditTextFamilyName: EditText? = null,
        @Nullable var mTextViewFamilyNameHint: TextView? = null,
        private var mEditTextPhoneNumber: EditText?,
        private var mTextViewPhoneNumberHint: TextView?,
        private var mEditTextPassword: EditText?,
        private var mTextViewPasswordHint: TextView,
        private var mButtonComit: Button?
) {

    private var isName: Boolean = true
    private var isFamilyName: Boolean = true
    private var isNumber: Boolean = false
    private var isPassword: Boolean = false

    private var type: FragmentType? = null

    fun execute(type: FragmentType) {
        this.type = type

        if (type == FragmentType.LOGIN_FRAGMENT) {
            createEditTextFocusEffectBackPressedLogin()
            return
        } else {
            createEditTextFocusEffectBackPressedSignUp()
        }

    }

    /** if we call back button from sign up we gonna come to this method **/
    private fun createEditTextFocusEffectBackPressedLogin() {

        if (!mEditTextPhoneNumber?.text.isNullOrEmpty()) {
            mTextViewPhoneNumberHint?.visibility = View.VISIBLE
            checkEditTextInput(mEditTextPhoneNumber!!, mTextViewPhoneNumberHint!!, mEditTextPhoneNumber?.text.toString())
        }

        if (!mEditTextPassword?.text.isNullOrEmpty()) {
            mTextViewPasswordHint.visibility = View.VISIBLE
            checkEditTextInput(mEditTextPassword!!, mTextViewPasswordHint, mEditTextPassword?.text.toString())
        }

        setOnFocusChangeListener(mEditTextPhoneNumber!!, mTextViewPhoneNumberHint!!, "شماره تلفن")

        setOnFocusChangeListener(mEditTextPassword!!, mTextViewPasswordHint, "رمز عبور")

        setTextChangedListener(mEditTextPhoneNumber!!, mTextViewPhoneNumberHint!!)

        setTextChangedListener(mEditTextPassword!!, mTextViewPasswordHint)

    }

    /** if we call back button from sign up we gonna come to this method **/
    private fun createEditTextFocusEffectBackPressedSignUp() {

        if (!mEditTextName?.text.isNullOrEmpty()) {
            mTextViewNameHint?.visibility = View.VISIBLE
            checkEditTextInput(mEditTextName!!, mTextViewNameHint!!, mEditTextName?.text.toString())
        }

        if (!mEditTextFamilyName?.text.isNullOrEmpty()) {
            mTextViewFamilyNameHint?.visibility = View.VISIBLE
            checkEditTextInput(mEditTextFamilyName!!, mTextViewFamilyNameHint!!, mEditTextName?.text.toString())
        }

        if (!mEditTextPhoneNumber?.text.isNullOrEmpty()) {
            mTextViewPhoneNumberHint?.visibility = View.VISIBLE
            checkEditTextInput(mEditTextPhoneNumber!!, mTextViewPhoneNumberHint!!, mEditTextPhoneNumber?.text.toString())
        }

        if (!mEditTextPassword?.text.isNullOrEmpty()) {
            mTextViewPasswordHint.visibility = View.VISIBLE
            checkEditTextInput(mEditTextPassword!!, mTextViewPasswordHint, mEditTextPassword?.text.toString())
        }

        setOnFocusChangeListener(mEditTextName!!, mTextViewNameHint!!, "نام")

        setOnFocusChangeListener(mEditTextFamilyName!!, mTextViewFamilyNameHint!!, "نام خانوادگی")


        setOnFocusChangeListener(mEditTextPhoneNumber!!, mTextViewPhoneNumberHint!!, "شماره تلفن")

        setOnFocusChangeListener(mEditTextPassword!!, mTextViewPasswordHint, "رمز عبور")

        setTextChangedListener(mEditTextName!!, mTextViewNameHint!!)

        setTextChangedListener(mEditTextFamilyName!!, mTextViewFamilyNameHint!!)

        setTextChangedListener(mEditTextPhoneNumber!!, mTextViewPhoneNumberHint!!)

        setTextChangedListener(mEditTextPassword!!, mTextViewPasswordHint)

    }

    /** makes the effect of editTexts */
    private fun addEditTextFocusEffect(isEmpty: Boolean, hasFocus: Boolean, editText: EditText,
                                       textViewHint: TextView, _hint: String) {

        if (!hasFocus && isEmpty) {
            if (textViewHint.id == R.id.textViewFamilyNameHint) {

                editText.hint = _hint
                textViewHint.visibility = View.INVISIBLE

            } else {
                editText.hint = _hint
                textViewHint.visibility = View.GONE
            }
            return
        }

        if (hasFocus && !isEmpty) {
            textViewHint.visibility = View.VISIBLE
            return
        }

        if (!hasFocus && !isEmpty) {
            textViewHint.visibility = View.VISIBLE
            return
        }

        if (hasFocus && isEmpty) {
            editText.hint = ""
            textViewHint.visibility = View.VISIBLE
            return
        }


    }

    /** every time editText input changes it get called */
    private fun setTextChangedListener(editText: EditText, textView: TextView) {

        editText.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                checkEditTextInput(editText, textView, s)
            }
        })
    }

    /** calls focusChange fun and notifies every time it changes **/
    private fun setOnFocusChangeListener(editText: EditText, textViewHint: TextView, hint: String) {

        editText.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->

            val isEmpty: Boolean = editText.text!!.isEmpty()

            addEditTextFocusEffect(
                    isEmpty,
                    hasFocus,
                    editText,
                    textViewHint,
                    hint
            )
        }
    }

    /**checks  which editText is selected and verify the input to be ok for sending to server
     * and making button update it alpha to be disabled or not
     */
    private fun checkEditTextInput(editText: EditText, textView: TextView, s: CharSequence) {
        if (type == FragmentType.SIGNUP_FRAGMENT)
            if (editText == mEditTextName) {
                if (editText.text!!.trim().length > 2) {
                    isName = true
                    addHintToTextView(isName, textView)
                } else {
                    isName = false
                    addHintToTextView(isName, textView)
                }
                checkButton()
                return
            }

        if (type == FragmentType.SIGNUP_FRAGMENT)
            if (editText == mEditTextFamilyName) {
                if (editText.text!!.trim().length > 2) {
                    isFamilyName = true
                    addHintToTextView(isFamilyName, textView)
                } else {
                    isFamilyName = false
                    addHintToTextView(isFamilyName, textView)
                }
                checkButton()
                return
            }

        if (editText == mEditTextPhoneNumber) {
            isNumber = isNumber(s, editText)
            addHintToTextView(isNumber, textView)
            checkButton()
            return
        }

        if (editText == mEditTextPassword) {
            if (editText.text!!.trim().length > 6) {
                isPassword = true
                addHintToTextView(isPassword, textView)
            } else {
                isPassword = false
                addHintToTextView(isPassword, textView)
            }
            checkButton()
        }
    }

    /** checks that every input be Ok and make button enable */
    private fun checkButton() {

        if (isNumber && isFamilyName && isPassword && isName)
            mButtonComit?.apply {
                isClickable = true
                alpha = 1.0f
                isEnabled = true
            }
        else
            mButtonComit?.apply {
                isClickable = false
                alpha = 0.4f
                isEnabled = false
            }

    }

    /** check that the input number is iranian number **/
    private fun isNumber(s: CharSequence, numberEditText: EditText): Boolean {
        // check that number have 11 character and 09 at start
        return if (numberEditText.text.isNotEmpty() && numberEditText.text.length == 11) {
            s[0].toString().toInt() == 0 && s[1].toString().toInt() == 9
        } else false
    }

    /** Adds red or green COLOR to hint when client puts value in editText **/
    private fun addHintToTextView(isOk: Boolean, hintTextView: TextView) {

        if (isOk) hintTextView.setTextColor(Color.parseColor("#1FBC8F"))
        else hintTextView.setTextColor(Color.parseColor("#FB3449"))
    }
}