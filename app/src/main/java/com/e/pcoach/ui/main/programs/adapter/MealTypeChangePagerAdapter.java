package com.e.pcoach.ui.main.programs.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;

import com.e.pcoach.R;
import com.e.pcoach.ui.helper.ActivityHelper;
import com.e.pcoach.ui.helper.MealTypeCaster;
import com.e.pcoach.ui.main.model.MealModel;

import java.util.List;

public class MealTypeChangePagerAdapter extends PagerAdapter {

    private LayoutInflater inflater;
    private Context context;
    private RecyclerView mRecyclerViewProgram;
    private ProgramMealRecAdapter mProgramMealRecAdapter;
    private ActivityHelper.Helper mHelper;
    private MealTypeCaster mCaster;
    private List<MealModel> mMealModels;
    private View mView;
    public MealTypeChangePagerAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (mView == null) {
            mView = inflater.inflate(R.layout.row_custom_view_meals, container, false);
        }
        if (mRecyclerViewProgram == null) {
            mRecyclerViewProgram = mView.findViewById(R.id.recyclerViewProgram);
        }
        mRecyclerViewProgram.setNestedScrollingEnabled(false);
        mHelper.setUpRecyclerView(context, mRecyclerViewProgram, mProgramMealRecAdapter, false);
        container.addView(mView);
        return mView;
    }

    public void setObjects(List<MealModel> mealModels, ActivityHelper.Helper helper,
                      MealTypeCaster caster) {
        mHelper = helper;
        mCaster = caster;
        mMealModels = mealModels;
        mProgramMealRecAdapter = new ProgramMealRecAdapter(context, mMealModels);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View v = (View) object;
        container.removeView(v);
    }
}
