package com.e.pcoach.ui.main.model;

import androidx.room.ColumnInfo;

public class WorkoutModel {

    @ColumnInfo(name = "workout_day_position")
    private int positionInWeek;

    @ColumnInfo(name = "move_title")
    private String workoutTitle;

    @ColumnInfo(name = "workout_rep")
    private int workoutRep;

    @ColumnInfo(name = "workout_set")
    private int workoutSet;

    @ColumnInfo(name = "workout_percentage")
    private int workoutPercentage;

    @ColumnInfo(name = "move_url")
    private String workoutPic;

    public int getPositionInWeek() {
        return positionInWeek;
    }

    public void setPositionInWeek(int positionInWeek) {
        this.positionInWeek = positionInWeek;
    }

    public String getWorkoutTitle() {
        return workoutTitle;
    }

    public void setWorkoutTitle(String workoutTitle) {
        this.workoutTitle = workoutTitle;
    }

    public int getWorkoutRep() {
        return workoutRep;
    }

    public void setWorkoutRep(int workoutRep) {
        this.workoutRep = workoutRep;
    }

    public int getWorkoutSet() {
        return workoutSet;
    }

    public void setWorkoutSet(int workoutSet) {
        this.workoutSet = workoutSet;
    }

    public int getWorkoutPercentage() {
        return workoutPercentage;
    }

    public void setWorkoutPercentage(int workoutPercentage) {
        this.workoutPercentage = workoutPercentage;
    }

    public String getWorkoutPic() {
        return workoutPic;
    }

    public void setWorkoutPic(String workoutPic) {
        this.workoutPic = workoutPic;
    }
}
