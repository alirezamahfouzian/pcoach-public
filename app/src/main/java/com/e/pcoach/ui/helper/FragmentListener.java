package com.e.pcoach.ui.helper;

import android.os.Bundle;

public interface FragmentListener {

    String NAME_KEY = "NAME_KEY";
    String FAMILY_NAME_KEY = "FAMILY_NAME_KEY";
    String NUMBER_KEY = "NUMBER_KEY";
    String PASSWORD_KEY = "PASSWORD_KEY";

    void openLoginFragment();

    void openSignUpFragment();

    void onBackPressedInstance(Bundle saveInstance);

}
