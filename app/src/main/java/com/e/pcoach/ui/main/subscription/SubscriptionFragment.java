package com.e.pcoach.ui.main.subscription;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.e.pcoach.R;
import com.e.pcoach.ui.helper.ActivityHelper;
import com.e.pcoach.ui.main.MainActivity;
import com.e.pcoach.ui.main.subscription.cardchange.CardFragmentPagerAdapter;
import com.e.pcoach.ui.main.subscription.cardchange.ShadowTransformer;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;

public class SubscriptionFragment extends DaggerFragment implements View.OnClickListener {

    @Inject
    ActivityHelper.Helper mHelper;
    @BindView(R.id.imageViewBack)
    ImageView imageViewBack;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    private SubscriptionViewModel subscriptionFragment;
    private MainActivity mActivity;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        subscriptionFragment =
                ViewModelProviders.of(this).get(SubscriptionViewModel.class);
        View root = inflater.inflate(R.layout.fragment_subscription, container, false);
        mActivity = (MainActivity) getActivity();
        ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        build();
    }

    private void build() {
        init();
    }

    private void init() {
        imageViewBack.setOnClickListener(this);
        setViewPagerSub();
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.setViewPagerVisibility(false);
        mActivity.setNavViewVisibility(false);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mActivity.setNavViewVisibility(true);
        mActivity.setViewPagerVisibility(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageViewBack:
                mHelper.removeFragment(mActivity, this);
                return;
        }
    }

    private void setViewPagerSub() {
        CardFragmentPagerAdapter pagerAdapter = new CardFragmentPagerAdapter(getChildFragmentManager(), dpToPixels(2, mActivity));
        ShadowTransformer fragmentCardShadowTransformer = new ShadowTransformer(viewPager, pagerAdapter);
        fragmentCardShadowTransformer.enableScaling(true);

        viewPager.setAdapter(pagerAdapter);
        viewPager.setPageTransformer(false, fragmentCardShadowTransformer);
        viewPager.setOffscreenPageLimit(3);
    }

    /**
     * Change value in dp to pixels
     *
     * @param dp
     * @param context
     * @return
     */
    public static float dpToPixels(int dp, Context context) {
        return dp * (context.getResources().getDisplayMetrics().density);
    }
}