package com.e.pcoach.ui.helper;

public interface TabChangeListener {

    String DATE_KEY = "DATE_KEY";
    String DAY_NAME_KEY = "DAY_NAME_KEY";
    String MONTH_KEY = "MONTH_KEY";
    String YEAR_KEY = "YEAR_KEY";

    String PROGRAM_TYPE_KEY = "PROGRAM_TYPE_KEY";

    void onDateChangeListener(int position);

}
