package com.e.pcoach.ui.main.tools.rm;

public class RmModel {

    private int rm;
    private int percentage;

    public int getRm() {
        return rm;
    }

    public void setRm(int rm) {
        this.rm = rm;
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }

}
