package com.e.pcoach.ui.getinfo;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.e.pcoach.db.entities.UserEntity;
import com.e.pcoach.db.entities.UserGeneralInfoEntity;
import com.e.pcoach.repository.GetInfoRepository;
import com.e.pcoach.repository.UserRepository;

import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.disposables.CompositeDisposable;

public class GetInfoViewModel extends AndroidViewModel{

    GetInfoRepository getInfoRepository;

    public GetInfoViewModel(@NonNull Application application) {
        super(application);
        getInfoRepository = new GetInfoRepository(application, new CompositeDisposable());
    }

    public void insertGeneralInfo(UserGeneralInfoEntity generalInfoEntity) {
        getInfoRepository.insertGeneralInfo(generalInfoEntity);
    }

    public void updateGeneralInfo(UserGeneralInfoEntity generalInfoEntity) {
        getInfoRepository.updateGeneralInfo(generalInfoEntity);
    }

    public Maybe<UserGeneralInfoEntity> getGeneralInfo(int generalInfoId) {
        return getInfoRepository.getGeneralInfo(generalInfoId);
    }

    public Maybe<List<UserGeneralInfoEntity>> getAllGeneralInfo() {
        return getInfoRepository.getAllGeneralInfo();
    }

}
