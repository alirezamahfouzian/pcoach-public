package com.e.pcoach.ui.getinfo.fragments.bottomsheet;

public class ActivityType {

    public static final int NONE = -1;
    public static final int LOW = 0;
    public static final int MEDIUME = 1;
    public static final int MUCH = 2;
    public static final int VERY_MUCH = 3;
}

