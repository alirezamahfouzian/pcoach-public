package com.e.pcoach.ui.main.question;

import android.os.Bundle;
import android.os.SystemClock;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.e.pcoach.R;
import com.e.pcoach.ui.helper.ActivityHelper;
import com.e.pcoach.ui.helper.DataSource;
import com.e.pcoach.ui.main.MainActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;

public class QuestionFragment extends DaggerFragment implements View.OnClickListener,
        QuestionRecAdapter.QuestionClickListener, MainActivity.IsAnswer {

    private static final String TAG = "QuestionFragment";
    public boolean isAnswer = false;
    @Inject
    ActivityHelper.Helper helper;
    @BindView(R.id.recyclerViewQuestions)
    RecyclerView recyclerViewQuestion;
    @BindView(R.id.imageViewBack)
    ImageView imageViewBack;
    @BindView(R.id.textViewQuestion)
    TextView textViewQuestion;
    @BindView(R.id.textViewAnswer)
    TextView textViewAnswer;
    @BindView(R.id.constraintLayoutAnswers)
    ConstraintLayout constraintLayoutAnswers;
    private QuestionRecAdapter adapter;
    private MainActivity mActivity;
    private List<String> mQuestions;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        mActivity = (MainActivity) getActivity();
        View root = inflater.inflate(R.layout.fragment_question, container, false);
        ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.setViewPagerVisibility(false);
        mActivity.setNavViewVisibility(false);
//        Toast.makeText(mActivity, "question", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        build();
    }

    private void build() {
        init();
        setUpRecyclerView();
    }

    private void init() {
        mActivity.isAnswerListener = this;
        mQuestions = DataSource.getQuestions(mActivity);
        imageViewBack.setOnClickListener(this);
    }

    private void setLayoutsVisibility(boolean isAnswer, Integer position) {
        if (isAnswer) {
            constraintLayoutAnswers.setVisibility(View.GONE);
            recyclerViewQuestion.setVisibility(View.VISIBLE);
            textViewAnswer.setText("");
            textViewQuestion.setText("");
            this.isAnswer = false;
            mActivity.isAnswer = false;
            return;
        }
        textViewQuestion.setText(mQuestions.get(position));
        textViewAnswer.setText(DataSource.getAnswers(mActivity, position));
        recyclerViewQuestion.setVisibility(View.GONE);
        constraintLayoutAnswers.setVisibility(View.VISIBLE);
        this.isAnswer = true;
        mActivity.isAnswer = true;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mActivity.setNavViewVisibility(true);
        mActivity.setViewPagerVisibility(true);
//        Toast.makeText(mActivity, "question des", Toast.LENGTH_SHORT).show();
    }

    private void setUpRecyclerView() {
        adapter = new QuestionRecAdapter(mActivity);
        adapter.setData(mQuestions, this);
        helper.setUpRecyclerView(mActivity, recyclerViewQuestion, adapter, false);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.imageViewBack) {
            if (isAnswer) {
              setLayoutsVisibility(true, null);
              return;
            }
            helper.removeFragment(mActivity, this);
        }
    }

    @Override
    public void onRecyclerViewItemClickListener(int position) {
        setLayoutsVisibility(false, position);
    }

    @Override
    public void setIsAnswer(boolean isAnswer) {
        setLayoutsVisibility(true, null);
    }
}


