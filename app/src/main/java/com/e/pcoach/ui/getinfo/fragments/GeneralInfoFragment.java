package com.e.pcoach.ui.getinfo.fragments;


import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.e.pcoach.R;
import com.e.pcoach.ui.getinfo.GetInformationActivity;
import com.e.pcoach.ui.getinfo.fragments.bottomsheet.ActivityType;
import com.e.pcoach.ui.getinfo.fragments.bottomsheet.ExperienceType;
import com.e.pcoach.ui.getinfo.fragments.bottomsheet.GenderType;
import com.e.pcoach.ui.getinfo.fragments.bottomsheet.GeneralInfoType;
import com.e.pcoach.ui.getinfo.fragments.bottomsheet.GiModel;
import com.e.pcoach.ui.getinfo.fragments.bottomsheet.InfoBottomSheetFragment;
import com.e.pcoach.ui.helper.ActivityHelper;
import com.e.pcoach.ui.main.adapter.customtabchanger.CustomTabChanger;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;


public class GeneralInfoFragment extends DaggerFragment implements
        CustomTabChanger.MealTabChangeListener, View.OnClickListener,
        InfoBottomSheetFragment.SubmitListener {

    @Inject
    ActivityHelper.Helper helper;
    @Inject
    CustomTabChanger customTabChanger;
    @Inject
    InfoBottomSheetFragment bottomSheetFragment;
    @BindView(R.id.linearLayoutAge)
    LinearLayout linearLayoutAge;
    @BindView(R.id.linearLayoutWeight)
    LinearLayout linearLayoutWeight;
    @BindView(R.id.linearLayoutHeight)
    LinearLayout linearLayoutHeight;
    @BindView(R.id.linearLayoutWrist)
    LinearLayout linearLayoutWrist;
    @BindView(R.id.linearLayoutExperience)
    LinearLayout linearLayoutExperience;
    @BindView(R.id.linearLayoutActivity)
    LinearLayout linearLayoutActivity;
    @BindView(R.id.textViewAgeInput)
    TextView textViewAgeInput;
    @BindView(R.id.textViewWeightInput)
    TextView textViewWeightInput;
    @BindView(R.id.textViewHeightInput)
    TextView textViewHeightInput;
    @BindView(R.id.textViewWirstInput)
    TextView textViewWirstInput;
    @BindView(R.id.textViewExperienceInput)
    TextView textViewExperienceInput;
    @BindView(R.id.textViewActivityInput)
    TextView textViewActivityInput;
    private int position;

    private GetInformationActivity mActivity;
    private ActivityHelper.Helper mHelper;
    private View mView;
    private long mLastClickTime = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mActivity = (GetInformationActivity) getActivity();
        mView = inflater.inflate(R.layout.fragment_general_info, container, false);
        ButterKnife.bind(this, mView);
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        build();
    }

    private void build() {
        init();
        setUpProgramTabChanger();
    }

    /**
     * TODO: unComment the stuff
     */
    private void init() {
        mActivity.generalInformation.setGenderType(0);
        mActivity.setFabForwardClick(false);
        bottomSheetFragment.setSubmitListener(this);
        linearLayoutAge.setOnClickListener(this);
        linearLayoutHeight.setOnClickListener(this);
        linearLayoutWeight.setOnClickListener(this);
        linearLayoutWrist.setOnClickListener(this);
        linearLayoutExperience.setOnClickListener(this);
        linearLayoutActivity.setOnClickListener(this);
    }

    private void setUpProgramTabChanger() {
        String[] titles = new String[]{"آقا", "خانم"};
        customTabChanger.build(mActivity, mView, this, titles);
    }

    @Override
    public void onTabChangeListener(int position) {
        if (position == 0)
            mActivity.generalInformation.setGenderType(GenderType.MALE);
        else
            mActivity.generalInformation.setGenderType(GenderType.FEMALE);
    }

    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 800) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        switch (v.getId()) {
            case R.id.linearLayoutAge:
                bottomSheetFragment.setType(GeneralInfoType.AGE);
                bottomSheetFragment.show(mActivity.getSupportFragmentManager(), null);
                return;
            case R.id.linearLayoutHeight:
                bottomSheetFragment.setType(GeneralInfoType.HEIGHT);
                bottomSheetFragment.show(mActivity.getSupportFragmentManager(), null);
                return;
            case R.id.linearLayoutWeight:
                bottomSheetFragment.setType(GeneralInfoType.WEIGHT);
                bottomSheetFragment.show(mActivity.getSupportFragmentManager(), null);
                return;
            case R.id.linearLayoutWrist:
                bottomSheetFragment.setType(GeneralInfoType.WRIST);
                bottomSheetFragment.show(mActivity.getSupportFragmentManager(), null);
                return;
            case R.id.linearLayoutExperience:
                bottomSheetFragment.setType(GeneralInfoType.EXPERIENCE);
                bottomSheetFragment.show(mActivity.getSupportFragmentManager(), null);
                return;
            case R.id.linearLayoutActivity:
                bottomSheetFragment.setType(GeneralInfoType.ACTIVITY);
                bottomSheetFragment.show(mActivity.getSupportFragmentManager(), null);
        }
    }

    @Override
    public void setOnSubmitListener(GiModel giModel) {
        switch (giModel.getGeneralInfoType()) {
            case AGE:
                int age = giModel.getAge();
                mActivity.generalInformation.setAge(age);
                textViewAgeInput.setText(String.valueOf(age));
                textViewAgeInput.setAlpha(1f);
                bottomSheetFragment.dismiss();
                checkData();
                return;
            case HEIGHT:
                int height = giModel.getHeight();
                mActivity.generalInformation.setHeight(height);
                textViewHeightInput.setText(String.valueOf(height));
                textViewHeightInput.setAlpha(1f);
                bottomSheetFragment.dismiss();
                checkData();
                return;
            case WEIGHT:
                int weightKg = giModel.getWeightKg();
                String weightG = giModel.getWeightG();
                float weight = weightKg + (Float.valueOf(weightG) / 1000);
                mActivity.generalInformation.setWeightKg(weightKg);
                mActivity.generalInformation.setWeightG(weightG);
                textViewWeightInput.setText(String.valueOf(weight));
                textViewWeightInput.setAlpha(1f);
                bottomSheetFragment.dismiss();
                checkData();
                return;
            case WRIST:
                int wristCm = giModel.getWristCm();
                String wristMm = giModel.getWristMm();
                float wrist = wristCm + (Float.valueOf(wristMm) / 100);
                mActivity.generalInformation.setWristCm(wristCm);
                mActivity.generalInformation.setWristMm(wristMm);
                textViewWirstInput.setText(String.valueOf(wrist));
                textViewWirstInput.setAlpha(1f);
                bottomSheetFragment.dismiss();
                checkData();
                return;
            case ACTIVITY:
                int activity = giModel.getActivityType();
                mActivity.generalInformation.setActivityType(activity);
                textViewActivityInput.setText(bottomSheetFragment.mGiModelCaster
                        .getActivityTitle(activity));
                textViewActivityInput.setAlpha(1f);
                bottomSheetFragment.dismiss();
                checkData();
                return;
            case EXPERIENCE:
                int experience = giModel.getExperience();
                mActivity.generalInformation.setExperience(experience);
                textViewExperienceInput.setText(bottomSheetFragment.mGiModelCaster
                        .getExperienceTitle(experience));
                textViewExperienceInput.setAlpha(1f);
                bottomSheetFragment.dismiss();
                checkData();
        }
    }

    public void checkData() {
        if (mActivity.generalInformation.getAge() != -1 &&
                mActivity.generalInformation.getWeightKg() != -1 &&
                mActivity.generalInformation.getHeight() != -1 &&
                mActivity.generalInformation.getWristCm() != -1 &&
                mActivity.generalInformation.getExperience() != -1 &&
                mActivity.generalInformation.getActivityType() != -1 &&
                mActivity.generalInformation.getGenderType() != -1
        ) {
            mActivity.setFabForwardClick(true);
        }
    }
}
