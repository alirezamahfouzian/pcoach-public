package com.e.pcoach.ui.helper

enum class MealType {
    None,BREAKFAST, LUNCH, DINNER, SNACK
}
