package com.e.pcoach.ui.firsttime.signup;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityOptionsCompat;
import androidx.lifecycle.ViewModelProvider;

import com.e.pcoach.ui.helper.FragmentType;
import com.e.pcoach.R;
import com.e.pcoach.db.entities.UserEntity;
import com.e.pcoach.ui.firsttime.UserViewModel;
import com.e.pcoach.ui.firsttime.FirstTimeActivity;
import com.e.pcoach.ui.helper.ActivityHelper;
import com.e.pcoach.ui.helper.AddEditTextFocusEffect;
import com.e.pcoach.ui.helper.FragmentListener;
import com.e.pcoach.ui.main.MainActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;

public class SignUpFragment extends DaggerFragment implements View.OnClickListener {

    private static final String TAG = "SignUpFragment";
    @Inject
    ActivityHelper.Helper helper;

    @BindView(R.id.textViewFamilyNameHint)
    TextView textViewFamilyNameHint;
    @BindView(R.id.editTextFamilyName)
    EditText editTextFamilyName;
    @BindView(R.id.textViewNameHint)
    TextView textViewNameHint;
    @BindView(R.id.editTextName)
    EditText editTextName;
    @BindView(R.id.textViewPhoneNumberHint)
    TextView textViewPhoneNumberHint;
    @BindView(R.id.editTextPhoneNumber)
    EditText editTextPhoneNumber;
    @BindView(R.id.buttonSubmitSignUp)
    Button buttonSubmitSignUp;
    @BindView(R.id.textViewGoToLogin)
    TextView textViewGoToLogin;
    @BindView(R.id.textViewPasswordHint)
    TextView textViewPasswordHint;
    @BindView(R.id.editTextPassword)
    EditText editTextPassword;
    @BindView(R.id.textViewSignUp)
    TextView textViewSignUp;
    @BindView(R.id.imageViewBackLogin)
    ImageView imageViewBackLogin;

    private View mView;
    private FirstTimeActivity mActivity;
    private UserViewModel userViewModel;
    private FragmentListener mFragmentListener;
    private Bundle mSavedInstance;

    /**
     * it gets instance of the editTexts
     * texts when page changes or app ui stops
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSavedInstance = new Bundle();
        mSavedInstance = getArguments();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mActivity = (FirstTimeActivity) getActivity();
        userViewModel = new ViewModelProvider(this).get(UserViewModel.class);
        mFragmentListener = mActivity;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_signup, container, false);
        ButterKnife.bind(this, mView);
        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        build();
    }

    @Override
    public void onStop() {
        super.onStop();
        // saves current state of fragment
        saveInstance();
    }

    private void build() {
        textViewGoToLogin.setOnClickListener(this);
        imageViewBackLogin.setOnClickListener(this);
        buttonSubmitSignUp.setOnClickListener(this);
        restoreFragment();
        addFocusEffect();
    }

    /**
     * calls the {@link AddEditTextFocusEffect} and sets
     * the textViews change effect
     */
    private void addFocusEffect() {
        new AddEditTextFocusEffect(editTextName, textViewNameHint, editTextFamilyName,
                textViewFamilyNameHint, editTextPhoneNumber, textViewPhoneNumberHint,
                editTextPassword, textViewPasswordHint, buttonSubmitSignUp)
                .execute(FragmentType.SIGNUP_FRAGMENT);
    }

    private void restoreFragment() {
        if (mSavedInstance != null) {
            String name = mSavedInstance.getString(mFragmentListener.NAME_KEY);
            String familyName = mSavedInstance.getString(mFragmentListener.FAMILY_NAME_KEY);
            String number = mSavedInstance.getString(mFragmentListener.NUMBER_KEY);
            String password = mSavedInstance.getString(mFragmentListener.PASSWORD_KEY);
            if (name != null) {
                editTextName.setText(name);
            }
            if (familyName != null) {
                editTextFamilyName.setText(familyName);
            }
            if (number != null) {
                editTextPhoneNumber.setText(number);
            }
            if (password != null) {
                editTextPassword.setText(password);
            }
        }
    }

    /**
     * uses stored data in {@link SignUpFragment} whenever {@link SignUpFragment} starts again
     * after an onBackPressed
     */
    private void saveInstance() {
        mSavedInstance = new Bundle();
        mSavedInstance.putString(mFragmentListener.NAME_KEY, editTextName.getText().toString());
        mSavedInstance.putString(mFragmentListener.FAMILY_NAME_KEY, editTextFamilyName.getText().toString());
        mSavedInstance.putString(mFragmentListener.NUMBER_KEY, editTextPhoneNumber.getText().toString());
        mSavedInstance.putString(mFragmentListener.PASSWORD_KEY, editTextPassword.getText().toString());
        mFragmentListener.onBackPressedInstance(mSavedInstance);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.textViewGoToLogin:
                mFragmentListener.openLoginFragment();
                return;
            case R.id.imageViewBackLogin:
                mActivity.onBackPressed();
                return;
            case R.id.buttonSubmitSignUp:
                saveUser();
        }
    }

    /**
     * todo : should really do this
     * todo after getting the response from server and then add user to the data base
     */
    private void saveUser() {
        UserEntity entity = new UserEntity();
        entity.setFirstName(editTextName.getText().toString());
        entity.setLastName(editTextFamilyName.getText().toString());
        entity.setPhoneNumber(editTextPhoneNumber.getText().toString());
        userViewModel.insertUser(entity);
        Intent intent = new Intent(mActivity, MainActivity.class);
        // for setting the activity change animation
        Bundle bundle = ActivityOptionsCompat.makeCustomAnimation(mActivity,
                android.R.anim.fade_in, android.R.anim.fade_out).toBundle();
        intent.putExtra(MainActivity.INTRO_KEY, true);
        startActivity(intent, bundle);
        mActivity.finish();
    }

}
