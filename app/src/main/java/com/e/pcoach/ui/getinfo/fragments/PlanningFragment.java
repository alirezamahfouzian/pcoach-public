package com.e.pcoach.ui.getinfo.fragments;


import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.e.pcoach.R;
import com.e.pcoach.ui.getinfo.GetInformationActivity;
import com.e.pcoach.ui.getinfo.fragments.bottomsheet.GeneralInfoType;
import com.e.pcoach.ui.getinfo.fragments.bottomsheet.GiModel;
import com.e.pcoach.ui.getinfo.fragments.bottomsheet.InfoBottomSheetFragment;
import com.google.android.material.chip.Chip;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;

public class PlanningFragment extends DaggerFragment implements InfoBottomSheetFragment.SubmitListener
        , View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private static final String TAG = "PlanningFragment";
    @Inject
    InfoBottomSheetFragment bottomSheetFragment;
    @BindView(R.id.textViewTrainingTimeInput)
    TextView textViewTrainingTimeInput;
    @BindView(R.id.linearLayoutTrainingTimePlaning)
    LinearLayout linearLayoutTrainingTimePlaning;
    private long mLastClickTime = 0;
    private GetInformationActivity mActivity;
    private Chip[] dayChipList = new Chip[7];
    private View mView;
    private List<Integer> planingTimes;
    private boolean isFirstTime = true;

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible && isResumed() && isFirstTime) {
            isFirstTime = false;
            build();
            checkData();

        } else if (visible && !isFirstTime){
            checkData();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_planning, container, false);
        ButterKnife.bind(this, mView);
        mActivity = (GetInformationActivity) getActivity();
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void build() {
        init();
    }

    private void init() {
        initChips();
        initDayFirstTime();
        bottomSheetFragment.setSubmitListener(this);
        linearLayoutTrainingTimePlaning.setOnClickListener(this);
    }

    private void initDayFirstTime() {
        planingTimes = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            planingTimes.add(0);
        }
    }

    private void initChips() {
        int j = 1;
        for (int i = 0; i < dayChipList.length; i++) {
            int id = mActivity.getResources().getIdentifier("chipD" + j, "id" ,
                    mActivity.getPackageName());
            dayChipList[i] = mView.findViewById(id);
            dayChipList[i].setOnCheckedChangeListener(this);
            j++;
        }
    }

    public void setStatus() {
        mActivity.planingTimeInformation.setDayStatues(planingTimes);
    }

    public void checkData() {
        int count = 0;
        for (Integer integer : planingTimes) {
            if (integer == 1) {
                count++;
            }
            if (count >= 1 && mActivity.generalInformation.getTrainingMin() != -1) {
                mActivity.setFabForwardClick(true);
                break;
            } else {
                mActivity.setFabForwardClick(false);
            }
        }
    }

    @Override
    public void setOnSubmitListener(GiModel giModel) {
        if (giModel.getGeneralInfoType() == GeneralInfoType.TRAINING_TIME) {
            int trainingTime = giModel.getTrainingMin();
            mActivity.generalInformation.setTrainingMin(trainingTime);
            textViewTrainingTimeInput.setText(String.valueOf(trainingTime));
            textViewTrainingTimeInput.setAlpha(1f);
            bottomSheetFragment.dismiss();
            checkData();
        }
    }

    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 800) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        switch (v.getId()) {
            case R.id.linearLayoutTrainingTimePlaning:
                bottomSheetFragment.setType(GeneralInfoType.TRAINING_TIME);
                bottomSheetFragment.show(mActivity.getSupportFragmentManager(), null);
                return;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        for (int i = 0; i < dayChipList.length; i++) {
            if (dayChipList[i].getId() == buttonView.getId()) {
                if (isChecked)
                    planingTimes.set(i, 1);
                else
                    planingTimes.set(i, 0);
            }
        }
        checkData();
    }
}
