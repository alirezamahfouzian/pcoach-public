package com.e.pcoach.ui.main.home.workout;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.e.pcoach.R;
import com.e.pcoach.ui.main.model.WorkoutModel;

import java.util.List;

import javax.inject.Inject;

public class HomeWorkoutRecAdapter extends RecyclerView.Adapter<HomeWorkoutRecAdapter.ViewHolder> {

    private static final String TAG = "HomeWorkoutRecAdapter";
    private List<WorkoutModel> mWorkoutList;
    private Context mContext;

    @Inject
    public HomeWorkoutRecAdapter(Context context) {
        mContext = context;
    }

    public void setData(List<WorkoutModel> workoutList) {
        this.mWorkoutList = workoutList;
    }

    public void updateData(List<WorkoutModel> workoutModels) {
        mWorkoutList = workoutModels;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext)
                .inflate(R.layout.row_home_workout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (mWorkoutList == null || mWorkoutList.size() == 0) {
            return;
        }
        holder.mTextViewTitle.setText(mWorkoutList.get(position).getWorkoutTitle());
        holder.mTextViewAmount.setText(mWorkoutList.get(position).getWorkoutSet()
                + "x " + mWorkoutList.get(position).getWorkoutRep());
    }

    @Override
    public int getItemCount() {

        if (mWorkoutList == null) {
            return 3;
        } else if (mWorkoutList.size() >= 3)
            return 3;
        else
            return mWorkoutList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView mTextViewTitle;
        TextView mTextViewAmount;

        private ViewHolder(@NonNull View itemView) {
            super(itemView);
            init();
        }

        private void init() {
            mTextViewTitle = itemView.findViewById(R.id.textViewName);
            mTextViewAmount = itemView.findViewById(R.id.textViewAmount);
        }
    }
}
