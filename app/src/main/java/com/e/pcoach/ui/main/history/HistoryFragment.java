package com.e.pcoach.ui.main.history;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.e.pcoach.R;
import com.e.pcoach.ui.helper.ActivityHelper;
import com.e.pcoach.ui.main.MainActivity;
import com.e.pcoach.ui.main.history.details.DetailsHistoryFragment;
import com.e.pcoach.ui.main.history.select.HistoryRecAdapter;
import com.e.pcoach.ui.main.programs.ProgramViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;
import io.reactivex.disposables.CompositeDisposable;

public class HistoryFragment extends DaggerFragment implements HistoryRecAdapter.RecyclerViewClick {

    @Inject
    ActivityHelper.Helper helper;

    DetailsHistoryFragment detailsHistoryFragment;
    private static final String TAG = "HistoryFragment";

    @BindView(R.id.imageViewBack)
    ImageView imageViewBack;
    @BindView(R.id.recyclerViewHistory)
    RecyclerView recyclerViewHistory;

    private HistoryRecAdapter historyRecAdapter;
    private List<HistoryModel> historyModels = new ArrayList<>();
    private ProgramViewModel mProgramViewModel;
    private MainActivity mActivity;
    private CompositeDisposable disposable;
    int[] years = new int[]{99,99,99,98};
    private boolean isFirstTime = true;

    @Override
    public void onResume() {
        super.onResume();
        mActivity.setViewPagerVisibility(false);
        mActivity.setNavViewVisibility(false);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        mProgramViewModel = new ViewModelProvider(this).get(ProgramViewModel.class);
        mActivity = (MainActivity) getActivity();
        View root = inflater.inflate(R.layout.fragment_history, container, false);
        ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        build();
    }

    private void build() {
        isFirstTime = false;
        init();
        setUpHistoryRecyclerView();
        setUpHistoryItems();
    }

    private void init() {
        disposable = new CompositeDisposable();
        imageViewBack.setOnClickListener(v -> {
            mActivity.getSupportFragmentManager().popBackStack();
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        disposable.clear();
        historyModels.clear();
    }

    private void setUpHistoryRecyclerView() {
        historyRecAdapter = new HistoryRecAdapter(mActivity);
        historyRecAdapter.setData(historyModels, this);
        helper.setUpRecyclerView(mActivity, recyclerViewHistory, historyRecAdapter,
                false);
    }

    private void setUpHistoryItems() {
        mProgramViewModel.getHistoryPrograms().observe(getViewLifecycleOwner(), new Observer<List<HistoryModel>>() {
            @Override
            public void onChanged(List<HistoryModel> historyModels) {
                historyRecAdapter.updateData(historyModels);
            }
        });
    }

    @Override
    public void recOncClickListener(HistoryModel historyModel) {
        mActivity.isMain = false;
        detailsHistoryFragment = new DetailsHistoryFragment();
        detailsHistoryFragment.setProgramId(historyModel.getProgramId());
        detailsHistoryFragment.setDayTitle(historyModel.getTitle());
        helper.transitToFragment(mActivity,detailsHistoryFragment, true);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mActivity.setViewPagerVisibility(true);
        mActivity.setNavViewVisibility(true);
    }
}
