package com.e.pcoach.ui.main.home.datepicker;

import android.util.Log;
import android.util.SparseIntArray;

import java.util.HashMap;

public class DateConstants {

    private static final String TAG = "DateConstants";
    private HashMap<String, Integer> mDaysPosition = new HashMap<>();
    private HashMap<Integer, String> mDaysName = new HashMap<>();
    private SparseIntArray mMonthDayCount = new SparseIntArray();

    public DateConstants() {
        setMonthDayCount();
        setDaysPosition();
        setDaysName();
    }

    public HashMap<Integer, String> getDaysName() {
        return mDaysName;
    }

    public HashMap<String, Integer> getDaysPosition() {
        return mDaysPosition;
    }

    public SparseIntArray getMonthDayCount() {
        return mMonthDayCount;
    }

    /**
     * we use it for getting position when
     * we have the weekDayName
     */
    public void setDaysPosition() {
        mDaysPosition.put("شنبه", 0);
        mDaysPosition.put("یک‌شنبه", 1);
        mDaysPosition.put("دوشنبه", 2);
        mDaysPosition.put("سه‌شنبه", 3);
        mDaysPosition.put("چهارشنبه", 4);
        mDaysPosition.put("پنج\u200Cشنبه", 5);
        mDaysPosition.put("جمعه", 6);
    }

    /**
     * we use it for getting weekDayName
     * when we have position
     */
    public void setDaysName() {
        mDaysName.put(0, "شنبه");
        mDaysName.put(1, "یک‌شنبه");
        mDaysName.put(2, "دوشنبه");
        mDaysName.put(3, "سه‌شنبه");
        mDaysName.put(4, "چهارشنبه");
        mDaysName.put(5, "پنج\u200Cشنبه");
        mDaysName.put(6, "جمعه");
    }

    /**
     * we use it for getting monthDayCount
     * when we have the month position in year
     */
    public void setMonthDayCount() {
        mMonthDayCount.put(0, 31);
        mMonthDayCount.put(1, 31);
        mMonthDayCount.put(2, 31);
        mMonthDayCount.put(3, 31);
        mMonthDayCount.put(4, 31);
        mMonthDayCount.put(5, 31);
        mMonthDayCount.put(6, 30);
        mMonthDayCount.put(7, 30);
        mMonthDayCount.put(8, 30);
        mMonthDayCount.put(9, 30);
        mMonthDayCount.put(10, 30);
        mMonthDayCount.put(11, 29);
    }

    public int[] getTowDaysAhead(int day, int month, int year, boolean isYearLeap) {
        int[] date = new int[3];
        Log.d(TAG, "getTowDaysAhead: " + isYearLeap);
        if (month != 12 && day == getMonthDayCount().get(month - 1) - 1) {
            // example: month 11 and it days are 30 -1 = 29 so we add one to month and set day to 1
            date[0] = year;
            date[1] = month + 1;
            date[2] = 1;

        } else if (!isYearLeap && month == 12 && day > 27) {
            // example: month 12 days are 29 and if 28 days be 1 and ...
            if (day == 28) {
                date[0] = year + 1;
                date[1] = 1;
                date[2] = 1;
            } else {
                date[0] = year + 1;
                date[1] = 1;
                date[2] = 2;
            }
        } else if (isYearLeap && month == 12 && day > 28) {
            Log.d(TAG, "getTowDaysAhead: " + month + "  " + day);
            if (day == 29) {
                date[0] = year + 1;
                date[1] = 1;
                date[2] = 1;
            } else {
                date[0] = year + 1;
                date[1] = 1;
                date[2] = 2;
            }
        } else {
            date[0] = year;
            date[1] = month;
            date[2] = day + 2;
        }
        return date;
    }
}
