package com.e.pcoach.ui.main.home.datepicker;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.e.pcoach.R;
import com.e.pcoach.libraries.persiandatepicker.PersianDatePicker;
import com.e.pcoach.libraries.persiandatepicker.util.PersianCalendar;
import com.e.pcoach.ui.main.MainActivity;
import com.e.pcoach.ui.main.home.HomeFragment;
import com.e.pcoach.ui.main.model.WorkoutDate;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BottomSheetFragment extends BottomSheetDialogFragment {

    @BindView(R.id.submitButton)
    TextView submitButton;
    @BindView(R.id.persianDatePicker)
    PersianDatePicker persianDatePicker;
    private PersianCalendar mLeapCalender;
    private WorkoutDate mDate;
    private View mView;
    private MainActivity activity;
    private DateConstants dateConstants;

    /**
     * get called when submitButton get clicked
     */
    private DateSubmitListener listener;

    @Inject
    public BottomSheetFragment(MainActivity activity,
                               PersianCalendar leapCalender, WorkoutDate date) {
        this.activity = activity;
        this.mLeapCalender = leapCalender;
        this.mDate = date;
        dateConstants = new DateConstants();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        listener = (DateSubmitListener) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * for adding the style to bottom sheet
     */
    @Override
    public int getTheme() {
        return R.style.BottomSheetDialogTheme;
    }

    /**
     * for setting the rounded corner theme and making it fully extend
     * when it opens up
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(activity, getTheme());
        bottomSheetDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dia) {
                BottomSheetDialog dialog = (BottomSheetDialog) dia;
                FrameLayout bottomSheet = dialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                // for making it fully open not half
                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
                BottomSheetBehavior.from(bottomSheet).setSkipCollapsed(true);
                BottomSheetBehavior.from(bottomSheet).setHideable(true);
            }
        });
        return bottomSheetDialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.row_bottom_sheet_date_picker, container, false);
        ButterKnife.bind(this, mView);
        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        handleDataTransformation();
    }

    /**
     * setting the submit button onClickListener it getting
     * selected date from date picker and setting it to {@link WorkoutDate}
     * so we can pass it to listener and we receive
     * it from {@link com.e.pcoach.ui.main.MainActivity}
     * and it send it to {@link HomeFragment}
     */
    private void handleDataTransformation() {
        submitButton.setOnClickListener(v -> {
            PersianCalendar calender = persianDatePicker.getDisplayPersianDate();
             /* we have to calculate the lastYear from the selected year so we create
             a new PersianCalender so we handle that */
            mLeapCalender.setPersianDate(calender.getPersianYear(), 1, 1);
            int[] towDaysAhead = dateConstants.getTowDaysAhead(calender.getPersianDay(), calender.getPersianMonth(),
                    calender.getPersianYear(), mLeapCalender.isPersianLeapYear());
            mDate.setDay(towDaysAhead[2]);
            int dayNamePosition = dateConstants.getDaysPosition().get(calender.getPersianWeekDayName());
            if (dayNamePosition < 5) {
                dayNamePosition = dayNamePosition + 2;
            } else {
                if (dayNamePosition == 5)
                    dayNamePosition = 0;
                else
                    dayNamePosition = 1;
            }
            mLeapCalender.setPersianDate(towDaysAhead[0] - 1, 1, 1);
            mDate.setYearLeap(mLeapCalender.isPersianLeapYear());
            mDate.setDayName(dateConstants.getDaysName().get(dayNamePosition));
            mDate.setMonth(towDaysAhead[1]);
            mDate.setYear(towDaysAhead[0]);
            listener.setOnSubmitListener(mDate);
        });
    }

    /**
     * for handling data transferring between bottomSheet and Fragment or activity
     */
    public interface DateSubmitListener {
        void setOnSubmitListener(WorkoutDate date);
    }
}