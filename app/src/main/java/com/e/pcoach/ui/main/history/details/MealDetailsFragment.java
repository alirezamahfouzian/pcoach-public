package com.e.pcoach.ui.main.history.details;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.e.pcoach.R;
import com.e.pcoach.db.entities.ProgramsEntity;
import com.e.pcoach.ui.helper.ActivityHelper;
import com.e.pcoach.ui.helper.MealTypeCaster;
import com.e.pcoach.ui.helper.ProgramType;
import com.e.pcoach.ui.main.MainActivity;
import com.e.pcoach.ui.main.model.MealModel;
import com.e.pcoach.ui.main.programs.ProgramViewModel;
import com.e.pcoach.ui.main.programs.adapter.MealCustomView;
import com.e.pcoach.ui.main.programs.adapter.ProgramDayRec;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class MealDetailsFragment extends DaggerFragment implements ProgramDayRec.DietDayChangeListener {

    @Inject
    ProgramDayRec programDayRec;
    @Inject
    ActivityHelper.Helper helper;
    @Inject
    MealTypeCaster caster;
    @Inject
    CompositeDisposable disposable;
    @BindView(R.id.recyclerViewMealProgramDate)
    RecyclerView recyclerViewMealProgramDate;
    @BindView(R.id.detailsSnackCustomView)
    MealCustomView snackCustomView;
    @BindView(R.id.detailsDinnerCustomView)
    MealCustomView dinnerCustomView;
    @BindView(R.id.detailsLunchCustomView)
    MealCustomView lunchCustomView;
    @BindView(R.id.detailsBreakFastCustomView)
    MealCustomView breakFastCustomView;
    // get in use days
    private List<Integer> mInUsedDays;
    private final int lastDay = -1;
    private final int indexCount = 0;
    private MainActivity mActivity;
    private View mViewNoProgram;
    private View mView;
    private ProgramViewModel mProgramViewModel;
    private final List<MealModel> mealBreakFastList = new ArrayList<>();
    private final List<MealModel> mealLunchList = new ArrayList<>();
    private final List<MealModel> mealDinnerList = new ArrayList<>();
    private final List<MealModel> mealSnackList = new ArrayList<>();
    private int mProgramId = 0;

    @Override
    public void onResume() {
        super.onResume();
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        mActivity = (MainActivity) getActivity();
        mView = inflater.inflate(R.layout.fragment_details_meal, container, false);
        ButterKnife.bind(this, mView);
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        build();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        disposable.dispose();
    }

    private void build() {
        if (mProgramViewModel == null) {
            mProgramViewModel = new ViewModelProvider(this).get(ProgramViewModel.class);
        }
        setUpMealTypeViews();
        setUpDayRecyclerView();
        setUpDay(ProgramType.MEAL);
        dinnerCustomView.getId();
    }

    void setDependency(ProgramViewModel programViewModel, int programId) {
        mProgramViewModel = programViewModel;
        mProgramId = programId;
    }

    private void setUpDay(ProgramType programType) {
        mInUsedDays = new ArrayList<>();
        mProgramViewModel.getEnabledPrograms().observe(getViewLifecycleOwner(), new Observer<ProgramsEntity>() {
            @Override
            public void onChanged(ProgramsEntity programsEntity) {
                Observable
                        .create(new ObservableOnSubscribe<Object>() {
                            @Override
                            public void subscribe(ObservableEmitter<Object> emitter) throws Exception {
                                if (!disposable.isDisposed()) {
                                    char[] chars = programsEntity.getProgramDietDays().toCharArray();
                                    int i = 0;
                                    for (char aChar : chars) {
                                        mInUsedDays.add(Integer.valueOf(String.valueOf(aChar)));
                                    }
                                    emitter.onComplete();
                                }
                            }
                        })
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new io.reactivex.Observer<Object>() {
                            @Override
                            public void onSubscribe(Disposable d) {
                                disposable.add(d);
                            }

                            @Override
                            public void onNext(Object o) {

                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onComplete() {
                                programDayRec.setData(mInUsedDays);
                            }
                        });
            }
        });
    }

    private void setUpMealTypeViews() {
        breakFastCustomView.build(null, helper);
        lunchCustomView.build(null, helper);
        dinnerCustomView.build(null, helper);
        snackCustomView.build(null, helper);
    }

    private void setUpMeals(int dayPosition) {
        mProgramViewModel.getDayMealsByTypeByProgramId(dayPosition, mProgramId)
                .observe(this, new Observer<List<MealModel>>() {
                    @Override
                    public void onChanged(List<MealModel> mealModels) {
                        try {
                            mealBreakFastList.clear();
                            mealLunchList.clear();
                            mealDinnerList.clear();
                            mealSnackList.clear();
                            Observable.fromIterable(mealModels)
                                    .map(new Function<MealModel, Boolean>() {
                                        @Override
                                        public Boolean apply(MealModel mealModel) throws Exception {
                                            try {
                                                if (!disposable.isDisposed()) {
                                                    switch (mealModel.getMealType()) {
                                                        case "0":
                                                            mealBreakFastList.add(mealModel);
                                                            break;
                                                        case "1":
                                                            mealLunchList.add(mealModel);
                                                            break;
                                                        case "2":
                                                            mealDinnerList.add(mealModel);
                                                            break;
                                                        case "3":
                                                            mealSnackList.add(mealModel);
                                                            break;
                                                    }
                                                }
                                            } catch (Exception e) {
                                            }
                                            return true;
                                        }
                                    })
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(new io.reactivex.Observer<Boolean>() {
                                        @Override
                                        public void onSubscribe(Disposable d) {
                                            disposable.add(d);
                                        }

                                        @Override
                                        public void onNext(Boolean b) {

                                        }

                                        @Override
                                        public void onError(Throwable e) {

                                        }

                                        @Override
                                        public void onComplete() {
                                            try {
                                                breakFastCustomView.updateData(mealBreakFastList);
                                                lunchCustomView.updateData(mealLunchList);
                                                dinnerCustomView.updateData(mealDinnerList);
                                                snackCustomView.updateData(mealSnackList);
                                            } catch (Exception e) {
                                            }
                                        }
                                    });
                        } catch (Exception e) {
                            throw e;
                        }
                    }
                });
    }

    private void setUpDayRecyclerView() {
        programDayRec.build(ProgramType.MEAL, this);
        helper.setUpRecyclerView(mActivity, recyclerViewMealProgramDate, programDayRec,
                true);
        recyclerViewMealProgramDate.smoothScrollToPosition(0);
    }

    @Override
    public void onDietDayChangeListener(int position) {
        setUpMeals(position);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        disposable.clear();
    }
}