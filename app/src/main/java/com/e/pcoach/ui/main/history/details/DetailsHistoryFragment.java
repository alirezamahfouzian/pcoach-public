package com.e.pcoach.ui.main.history.details;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.e.pcoach.R;
import com.e.pcoach.db.entities.ProgramsEntity;
import com.e.pcoach.ui.helper.ActivityHelper;
import com.e.pcoach.ui.main.MainActivity;
import com.e.pcoach.ui.main.adapter.bottomnavigation.NoSwipePager;
import com.e.pcoach.ui.main.adapter.bottomnavigation.PagerSaveStateHelpAdapter;
import com.e.pcoach.ui.main.adapter.customtabchanger.CustomTabChanger;
import com.e.pcoach.ui.main.programs.ProgramViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class DetailsHistoryFragment extends DaggerFragment
        implements CustomTabChanger.MealTabChangeListener, View.OnClickListener {

    @Inject
    PagerSaveStateHelpAdapter pagerAdapter;
    @Inject
    WorkoutDetailsFragment programWorkoutFragment;
    @Inject
    MealDetailsFragment programMealFragment;
    @Inject
    ActivityHelper.Helper helper;
    @Inject
    CustomTabChanger customTabChanger;

    @BindView(R.id.detailsViewPager)
    NoSwipePager programsViewPager;
    @BindView(R.id.imageViewBack)
    ImageView imageViewBack;
    @BindView(R.id.textViewName)
    TextView textViewName;
    @BindView(R.id.fabSelectProgram)
    FloatingActionButton fabSelectProgram;
    @Inject
    CompositeDisposable disposable;
    private MainActivity mActivity;
    private View mViewNoProgram;
    private View mView;
    private int mProgramId;
    private String mProgramTitle = "برنامه";
    private WorkoutDetailsFragment workoutDetailsFragment;
    private MealDetailsFragment mealDetailsFragment;
    private ProgramViewModel mProgramViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        mProgramViewModel = new ViewModelProvider(this).get(ProgramViewModel.class);
        mView = inflater.inflate(R.layout.fragment_history_details, container, false);
        ButterKnife.bind(this, mView);
        mActivity = (MainActivity) getActivity();
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        build();
    }

    private void build() {
        init();
        setUpProgramTab();
        setUpProgramTabChanger();
        checkTheProgramId();
    }

    private void checkTheProgramId() {
        disposable.add(mProgramViewModel
                .getEnabledProgramsObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<ProgramsEntity>() {
                    @Override
                    public void accept(ProgramsEntity programsEntity) throws Exception {
                        if (mProgramId != programsEntity.getProgramId()) {
                            fabSelectProgram.setVisibility(View.VISIBLE);
                        }
                    }
                }));
    }

    private void init() {
        imageViewBack.setOnClickListener(this);
        fabSelectProgram.setOnClickListener(this);
        textViewName.setText(mProgramTitle);
    }

    public void setProgramId(int programId) {
        mProgramId = programId;
    }

    public void setDayTitle(String programTitle) {
        mProgramTitle = programTitle;
    }

    private void setUpProgramTab() {
        workoutDetailsFragment = new WorkoutDetailsFragment();
        workoutDetailsFragment.setDependency(mProgramViewModel, mProgramId);
        mealDetailsFragment = new MealDetailsFragment();
        mealDetailsFragment.setDependency(mProgramViewModel, mProgramId);
        programsViewPager.setOffscreenPageLimit(1);
        pagerAdapter.addFragments(workoutDetailsFragment);
        pagerAdapter.addFragments(mealDetailsFragment);
        programsViewPager.setPagingEnabled(false);
        programsViewPager.setAdapter(pagerAdapter);
    }

    /**
     * sets up ProgramTabChanger
     */
    private void setUpProgramTabChanger() {
        String[] titles = new String[]{"تمرینی", "غذایی"};
        customTabChanger.build(mActivity, mView, this, titles);
    }

    @Override
    public void onTabChangeListener(int position) {
        programsViewPager.setCurrentItem(position, false);
    }

    @SuppressLint("CheckResult")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fabSelectProgram:
                disposable.add(mProgramViewModel
                        .getEnabledProgramsObservable()
                        .subscribeOn(Schedulers.io())
                        .observeOn(Schedulers.io())
                        .subscribe(new Consumer<ProgramsEntity>() {
                            @Override
                            public void accept(ProgramsEntity programsEntity) throws Exception {
                                try {
                                    if (!disposable.isDisposed()) {
                                        programsEntity.setIsEnabled(0);
                                        mProgramViewModel.updateProgram(programsEntity);
                                        mProgramViewModel.getProgramById(mProgramId).subscribe(new Consumer<ProgramsEntity>() {
                                            @Override
                                            public void accept(ProgramsEntity programsEntity) throws Exception {
                                                programsEntity.setIsEnabled(1);
                                                mProgramViewModel.updateProgram(programsEntity);
                                            }
                                        });
                                    }
                                } catch (Exception e) { }
                            }
                        }));
                mActivity.getSupportFragmentManager().popBackStack();
                mActivity.getSupportFragmentManager().popBackStack();
                mActivity.setViewPagerVisibility(true);
                mActivity.setNavViewVisibility(true);
                return;
            case R.id.imageViewBack:
                mActivity.getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        disposable.clear();
        mActivity.isMain = false;
    }
}