package com.e.pcoach.ui.getinfo.fragments.loading;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.e.pcoach.R;
import com.e.pcoach.algorithm.HandleGetInfo;
import com.e.pcoach.algorithm.InsertProgramServer;
import com.e.pcoach.db.entities.ProgramsEntity;
import com.e.pcoach.network.DietProgramServer;
import com.e.pcoach.network.ProgramsServer;
import com.e.pcoach.network.WorkoutProgramServer;
import com.e.pcoach.ui.getinfo.GetInfoViewModel;
import com.e.pcoach.ui.getinfo.GetInformationActivity;
import com.e.pcoach.ui.getinfo.fragments.bottomsheet.GiModel;
import com.e.pcoach.ui.getinfo.fragments.bottomsheet.GiModelCaster;
import com.e.pcoach.ui.helper.ActivityHelper;
import com.e.pcoach.ui.main.programs.ProgramViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

@SuppressLint("CheckResult")
public class LoadingFragment extends DaggerFragment {

    private static final String TAG = "LoadingFragment";
    @BindView(R.id.textViewProsses)
    TextView textViewProcesses;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @Inject
    HandleGetInfo handleGetInfo;
    @Inject
    GiModelCaster giModelCaster;
    @Inject
    ActivityHelper.Helper helper;
    int i = 0;
    private GetInformationActivity mActivity;
    private GiModel giModel;
    private View mView;
    private String[] progressStatements;
    private GetInfoViewModel giViewModel;
    private ProgramViewModel programViewModel;
    private InsertProgramServer insertProgramServer;
    private CompositeDisposable mDisposable = new CompositeDisposable();
    private int id = 1;
    private List<Integer> mInUsedDays = new ArrayList<Integer>();
    private int lastDay = -1;
    private int indexCount = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mActivity = (GetInformationActivity) getActivity();
        giViewModel = new ViewModelProvider(mActivity).get(GetInfoViewModel.class);
        programViewModel = new ViewModelProvider(mActivity).get(ProgramViewModel.class);
        mView = getLayoutInflater().inflate(R.layout.fragment_loading, container, false);
        ButterKnife.bind(this, mView);
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        build();
    }

    private void build() {
        init();
        setUpProgressStatements();
        setUpTheProgressChange();
        handleGetInfo.build(mActivity, giViewModel, programViewModel);
        insertProgram();
    }

    private void init() {
        giModel = mActivity.generalInformation;
    }

    private void setUpProgressStatements() {
        progressStatements = new String[]{
                "در حال تحلیل داده ها...",
                "در حال ساخت برنامه...",
                "در حال برنامه ریزی..."
        };
    }

    private void setUpTheProgressChange() {
        CompositeDisposable disposable = new CompositeDisposable();
        Observable.timer(1900, TimeUnit.MILLISECONDS)
                .repeat(4)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Long>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable.add(d);
                    }

                    @Override
                    public void onNext(Long aLong) {
                        if (i == 3) {
                            mActivity.finish();
                            return;
                        }
                        textViewProcesses.setText(progressStatements[i]);
                        i++;
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        disposable.clear();
                    }
                });
    }


    @SuppressLint("CheckResult")
    private void insertProgram() {
        CompositeDisposable disposable = new CompositeDisposable();
        Observable.range(0, 1)
                .map(new Function<Integer, Integer>() {
                    @Override
                    public Integer apply(Integer integer) throws Exception {
                        ProgramsServer programServer = new ProgramsServer();
                        programServer.setProgramId(integer);
                        programServer.setIsEnabled(1);
                        programServer.setProgramGoal(2);
                        programServer.setProgramMinute(50);
                        programServer.setProgramCreatedTime(giModelCaster.getDateTime());
                        programServer.setProgramStartTime(helper.getNowTime());
                        programServer.setProgramEndTime(helper.getMonthForward(1));
                        programServer.setUserId(1);
                        programViewModel.getEnabledProgramsObservable().subscribe(new Consumer<ProgramsEntity>() {
                            @Override
                            public void accept(ProgramsEntity programsEntity) throws Exception {
                                if (programsEntity != null) {
                                    id = programsEntity.getProgramId() + 1;
                                    programsEntity.setIsEnabled(0);
                                    programViewModel.updateProgram(programsEntity);
                                }
                            }
                        });
                        programServer.setProgramTitle(id + " برنامه شماره ");
                        insertProgramServer = new InsertProgramServer(mDisposable, programViewModel);
                        insertProgramServer.insertServerProgram(programServer);
                        return integer;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable::add)
                .doOnComplete(disposable::clear)
                .subscribe(new Consumer<Integer>() {
                    @Override
                    public void accept(Integer integer) throws Exception {
                        insertDiet();
                        insertWorkout();
                    }
                });
    }

    /**
     * WORKOUT RULE
     * todo: day position and program order size should be same
     * todo: sets and reps and moves size too
     * todo: first we have to validate the data from server then pass it to the db
     */
    /**
     * DIET RULE
     * todo: every 4 typeOrder should be sum of it own day order
     * todo: like 5445 sum in mealTypeOrder should be equal to (r) in day order
     */
    private void insertDiet() {
        CompositeDisposable disposable = new CompositeDisposable();
        Observable.range(0, 1)
                .map(new Function<Integer, Integer>() {
                    @Override
                    public Integer apply(Integer integer) throws Exception {
                        DietProgramServer programServer = new DietProgramServer();
                        programServer.setMeals("abacadaeafagahaiajabacadaeafagahaiajabacadaeafagahaiajabacadaeafagahaiajabacadaeafagahaiajabacadaeafagahaiaj");
                        programServer.setMealAmount("EaeHIjcGFEaeHIjcGFEaeHIjcGFEaeHIjcGFEaeHIjcGFEaeHIjcGF");
                        programServer.setMealTypesOrder("544545555444");
                        programServer.setMealDayOrder("rsq");
                        programServer.setMealDayPositions("012");
                        programServer.setProgramId(id);
                        insertProgramServer = new InsertProgramServer(mDisposable, programViewModel);
                        insertProgramServer.insertServerDiet(programServer);
                        return integer;
                    }
                })
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(disposable::add)
                .doOnComplete(disposable::clear)
                .subscribe();
    }

    private void insertWorkout() {
        CompositeDisposable disposable = new CompositeDisposable();
        Observable.create(new ObservableOnSubscribe<Object>() {
            @Override
            public void subscribe(ObservableEmitter<Object> emitter) throws Exception {
                WorkoutProgramServer programServer = new WorkoutProgramServer();
                programServer.setMoves("aaabacadaeafagaaabacadaeafagaaabacadaeafagaaabacadaeafagaaabacadaeafagaaabacadaeafag");
                programServer.setWorkoutProgramOrders("mno");
                programServer.setWorkoutDayPositions("135");
                programServer.setWorkoutSets("343343334433343343343434434334333443333334");
                programServer.setWorkoutReps("jhhjhjhjhjhjhjhjhjhjhhhhhhhhjhhjhjhjhjhjhj");
                programServer.setWorkoutPercentages("dcdddcdcdcdcddccdccdcdcadcdadcdddcdcdcdcdd");
                programServer.setProgramId(id);
                InsertProgramServer insertProgramServer = new InsertProgramServer(mDisposable, programViewModel);
                insertProgramServer.insertServerWorkout(programServer);
            }
        }).subscribeOn(Schedulers.io())
                .doOnSubscribe(disposable::add)
                .doOnComplete(disposable::clear)
                .subscribe();
    }
}
