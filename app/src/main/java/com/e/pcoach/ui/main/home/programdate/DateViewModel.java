package com.e.pcoach.ui.main.home.programdate;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.e.pcoach.db.entities.WorkoutDateEntity;
import com.e.pcoach.repository.DateRepository;

import io.reactivex.Maybe;

public class DateViewModel extends AndroidViewModel {

    private DateRepository mDateRepository;

    public DateViewModel(@NonNull Application application) {
        super(application);
        mDateRepository = new DateRepository(application);
    }

    public Maybe<Integer> getWorkoutByDayInMonth(int positionInMonth) {
       return mDateRepository.getWorkoutByDayInMonth(positionInMonth);
    }

}
