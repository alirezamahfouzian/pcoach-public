package com.e.pcoach.ui.main.home;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.core.app.ActivityOptionsCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.e.pcoach.R;
import com.e.pcoach.db.entities.ProgramsEntity;
import com.e.pcoach.libraries.persiandatepicker.util.PersianCalendar;
import com.e.pcoach.network.ServerDateConverter;
import com.e.pcoach.ui.getinfo.GetInformationActivity;
import com.e.pcoach.ui.helper.ActivityHelper;
import com.e.pcoach.ui.helper.DarkModePrefManager;
import com.e.pcoach.ui.helper.DataSource;
import com.e.pcoach.ui.helper.MealTypeCaster;
import com.e.pcoach.ui.helper.OnSwipeTouchListener;
import com.e.pcoach.ui.helper.TabChangeListener;
import com.e.pcoach.ui.helper.UserState;
import com.e.pcoach.ui.main.MainActivity;
import com.e.pcoach.ui.main.adapter.customtabchanger.CustomTabChanger;
import com.e.pcoach.ui.main.home.datepicker.BottomSheetFragment;
import com.e.pcoach.ui.main.home.datepicker.DateAdapter;
import com.e.pcoach.ui.main.home.datepicker.DateConstants;
import com.e.pcoach.ui.main.home.datepicker.HomeDatePickerAdapter;
import com.e.pcoach.ui.main.home.mealviewpager.HomeMealPagerAdapter;
import com.e.pcoach.ui.main.home.mealviewpager.MealRecAdapter;
import com.e.pcoach.ui.main.home.programdate.DateHandler;
import com.e.pcoach.ui.main.home.programdate.DateViewModel;
import com.e.pcoach.ui.main.home.programtoday.TodayProgramsFragment;
import com.e.pcoach.ui.main.home.workout.HomeWorkoutRecAdapter;
import com.e.pcoach.ui.main.model.MealModel;
import com.e.pcoach.ui.main.model.WorkoutDate;
import com.e.pcoach.ui.main.model.WorkoutDaysModel;
import com.e.pcoach.ui.main.model.WorkoutModel;
import com.e.pcoach.ui.main.profile.ProfileFragment;
import com.e.pcoach.ui.main.programs.ProgramViewModel;
import com.e.pcoach.ui.main.question.QuestionFragment;
import com.e.pcoach.ui.main.subscription.SubscriptionFragment;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;
import io.reactivex.MaybeObserver;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;

@SuppressLint("CheckResult")
public class HomeFragment extends DaggerFragment implements TabChangeListener,
        CustomTabChanger.MealTabChangeListener, View.OnClickListener {

    private static final String TAG = "HomeFragment";
    private final int MIN_DISTANCE = 150;

    @Inject
    ActivityHelper.Helper helper;
    @Inject
    BottomSheetFragment bottomSheetFragment;
    @Inject
    PersianCalendar persianCalendar;
    @Inject
    WorkoutDate mWorkoutDate;
    @Inject
    CustomTabChanger customTabChanger;
    @Inject
    Bundle bundle;
    @Inject
    WorkoutDaysModel mWorkoutDaysModel;
    @Inject
    HomeWorkoutRecAdapter homeWorkoutRecAdapter;
    @Inject
    MealTypeCaster mMealTypeCaster;
    @Inject
    ServerDateConverter dateConverter;
    @Inject
    CompositeDisposable disposable;
    @Inject
    DateHandler dateHandler;
    @BindView(R.id.textViewTodayTitle)
    TextView textViewTodayTitle;
    @BindView(R.id.linearLayoutDisableWorkout)
    LinearLayout linearLayoutDisableWorkout;
    @BindView(R.id.linearLayoutDisableMeals)
    LinearLayout linearLayoutDisableMeals;
    @BindView(R.id.linearLayoutDisableCharts)
    LinearLayout linearLayoutDisableCharts;
    @BindView(R.id.textViewValueProtein)
    TextView textViewValueProtein;
    @BindView(R.id.textViewValueFat)
    TextView textViewValueFat;
    @BindView(R.id.textViewRestDay)
    TextView textViewRestDay;
    @BindView(R.id.textViewDotsLeft)
    TextView textViewDotsLeft;
    @BindView(R.id.textViewDotsRight)
    TextView textViewDotsRight;
    @BindView(R.id.textViewValueCarb)
    TextView textViewValueCarb;
    @BindView(R.id.barProtein)
    View barProtein;
    @BindView(R.id.barFat)
    View barFat;
    @BindView(R.id.barCarb)
    View barCarb;
    @BindView(R.id.progressbarColeries)
    ProgressBar progressbarColeries;
    @BindView(R.id.background_progressbar)
    ProgressBar progressbarBack;
    @BindView(R.id.constraintLayoutNoProgram)
    ConstraintLayout constraintLayoutNoProgram;
    @BindView(R.id.constraintLayoutDiet)
    ConstraintLayout constraintLayoutDiet;
    @BindView(R.id.textViewGetProgram)
    TextView textViewGetProgram;
    @BindView(R.id.textView0)
    TextView textView0;
    @BindView(R.id.textView1)
    TextView textView1;
    @BindView(R.id.textView2)
    TextView textView2;
    @BindView(R.id.textView3)
    TextView textView3;
    @BindView(R.id.textViewNoProgram)
    TextView textViewNoProgram;
    @BindView(R.id.statsBg)
    ConstraintLayout constraintStatsBg;
    @BindView(R.id.guidelineFat)
    Guideline guidelineFat;
    @BindView(R.id.guidelineProtein)
    Guideline guidelineProtein;
    @BindView(R.id.guidelineCarb)
    Guideline guidelineCarb;
    @BindView(R.id.imageViewCalender)
    ImageView imageViewCalender;
    @BindView(R.id.firstDayName)
    TextView firstDayName;
    @BindView(R.id.firstDayDate)
    TextView firstDayDate;
    @BindView(R.id.firstDaySelector)
    LinearLayout firstDaySelector;
    @BindView(R.id.recyclerViewWorkout)
    RecyclerView recyclerViewWorkout;
    @BindView(R.id.recyclerViewMeals)
    RecyclerView recyclerViewMeals;
    @BindView(R.id.buttonMoreWorkout)
    TextView buttonMoreWorkout;
    @BindView(R.id.buttonMoreMeal)
    TextView buttonMoreMeal;
    @BindView(R.id.imageViewProfile)
    ImageView imageViewProfile;
    @BindView(R.id.imageSubscribe)
    ImageView imageSubscribe;
    @BindView(R.id.imageQuestion)
    ImageView imageQuestion;

    private int mWorkoutSelectedDay = -1;
    private TodayProgramsFragment todayProgramsFragment;
    private PersianCalendar mLeapCalender;
    private UserState mUserState;
    private View mView;
    private MainActivity mActivity;
    private int mCalsBurned = 2230;
    private int mCalsConsumed = 3320;
    private DateAdapter mDateAdapter;
    private HomeDatePickerAdapter mDatePickerAdapter;
    private long mLastClickTime = 0;
    private Resources mResources;
    private int mOffColor;
    private int mOnColor;
    private ProgramViewModel mProgramViewModel;
    private DateViewModel mDateViewModel;
    private HomeMealPagerAdapter mHomeMealPagerAdapter;
    private float x1, x2;
    private MealRecAdapter mHomeMealRecAdapter;
    private int mealCurrentPage = 0;
    private String isRestDayText;
    private String isNoProgramDay;
    private Observable<Integer> positionInMonth;
    private List<Integer> mInUsedDays = new ArrayList<Integer>();
    private int lastDay = -1;
    private int indexCount = 0;
    private int mDietSelectedDay;
    private DateConstants mDateConstants;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        mDateViewModel = new ViewModelProvider(this).get(DateViewModel.class);
        mView = inflater.inflate(R.layout.fragment_home, container, false);
        mActivity = ((MainActivity) getActivity());
        ButterKnife.bind(this, mView);
        init();
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        build();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void setViewModel(ProgramViewModel programViewModel) {
        mProgramViewModel = programViewModel;
    }

    private void init() {
        mResources = mActivity.getResources();
        mOffColor = mResources.getColor(R.color.gray_program_day);
        mOnColor = mResources.getColor(R.color.colorAccent);
        isRestDayText = getResources().getString(R.string.isRestDay);
        isNoProgramDay = getResources().getString(R.string.isNoProgramDay);
        mDateConstants = new DateConstants();
        mDatePickerAdapter = new HomeDatePickerAdapter(mActivity, mView, this);
        imageViewCalender.setOnClickListener(this);
        buttonMoreMeal.setOnClickListener(this);
        buttonMoreWorkout.setOnClickListener(this);
        imageViewProfile.setOnClickListener(this);
        imageQuestion.setOnClickListener(this);
        imageSubscribe.setOnClickListener(this);
    }

    private void build() {
        setTodayToDatePicker();
        checkUserState();
        updateCircleChart();
        setUpTodayMeal();
        setUpMealTabChanger();
        setUpTodayWorkout();
    }

    private void checkUserState() {
        if (mProgramViewModel == null) {
            mProgramViewModel = new ViewModelProvider(this).get(ProgramViewModel.class);
        }
        mProgramViewModel.getEnabledPrograms().observe(getViewLifecycleOwner(), new Observer<ProgramsEntity>() {
            @Override
            public void onChanged(ProgramsEntity programsEntity) {
                if (programsEntity != null) {
                    mUserState = UserState.ENABLED_PROGRAM;
                    mDatePickerAdapter.selectView(2, 4, 5);
//                    customTabChanger.selectTab(0);
                    setState(mUserState);
                } else {
                    mProgramViewModel.getAllProgramsLimited().subscribe(new MaybeObserver<List<ProgramsEntity>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            disposable.add(d);
                        }

                        @Override
                        public void onSuccess(List<ProgramsEntity> programsEntities) {
                            try {
                                if (programsEntities.size() == 0) {
                                    mUserState = UserState.DISABLED_PROGRAM;
                                    setState(mUserState);
                                    setUpMoves(0);
                                    checkWorkoutRestDay(0);
                                    checkDietRestDay(0);
                                    homeWorkoutRecAdapter.updateData(DataSource.getWorkoutByDay(0));
                                } else {
                                    mUserState = UserState.DISABLED_PROGRAM;
                                    setState(mUserState);
                                }
                            } catch (Exception e) {
                            }
                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onComplete() {
                        }
                    });
                }

            }
        });
    }

    private void setUpGetProgram() {
        textViewGetProgram.setOnClickListener(view -> {
            Intent intent = new Intent(mActivity, GetInformationActivity.class);
            Bundle bundle = ActivityOptionsCompat.makeCustomAnimation(mActivity,
                    android.R.anim.fade_in, android.R.anim.fade_out).toBundle();
            mActivity.startActivity(intent, bundle);
        });
    }

    /**
     * gets value from data base
     * and updates view
     */
    private void updateUpBarChart() {
//        ChartAnimation animFat = new ChartAnimation(guidelineFat, 1f, 0.35f);
//        animFat.setDuration(2000);
//        ChartAnimation animCarb = new ChartAnimation(guidelineCarb, 1f, 0.453f);
//        animCarb.setDuration(2000);
//        ChartAnimation animProtein = new ChartAnimation(guidelineProtein, 1f, 0.2f);
//        animProtein.setDuration(2000);
//        guidelineCarb.startAnimation(animCarb);
//        guidelineFat.startAnimation(animFat);
//        guidelineProtein.startAnimation(animProtein);
        setChartHeight(guidelineFat, 0.35f);
        setChartHeight(guidelineCarb, 0.453f);
        setChartHeight(guidelineProtein, 0.2f);
    }

    /**
     * sets bar chart height
     */
    private void setChartHeight(Guideline guideLine, Float height) {
        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) guideLine.getLayoutParams();
        params.guidePercent = height; // 45% // range: 0.6 min <-> 0.04 max
        guideLine.setLayoutParams(params);
    }

    /**
     * gets value from data base
     * and updates the circle view
     */
    private void updateCircleChart() {
        double d = (double) mCalsBurned / (double) mCalsConsumed;
        int progress = (int) (d * 100);
        progressbarColeries.setProgress(progress);
    }

    /**
     * gets called at {@link BottomSheetFragment }
     * when user touch submit
     * and has a date value converted with dates
     * in an array with six days before and set
     * the converted data to the DatePicker in home
     */
    public void dismissDialog(WorkoutDate date) {
        Log.d(TAG, "day: " + +date.getDay() + "month: " + date.getMonth() + "year: " + date.getYear());
        bottomSheetFragment.dismiss();
        mDateAdapter = new DateAdapter(date);
        mWorkoutDaysModel = mDateAdapter.build();
        mDatePickerAdapter.setData(mWorkoutDaysModel);
        mDatePickerAdapter.setIndicatorToMiddle();
    }

    /**
     * gets today's date and sets it to
     * homeFragment datePicker
     */
    private void setTodayToDatePicker() {
        mLeapCalender = new PersianCalendar();
        mLeapCalender.setPersianDate(persianCalendar.getPersianYear(), 1, 1);
        int[] towDaysAhead = mDateConstants.getTowDaysAhead(persianCalendar.getPersianDay(), persianCalendar.getPersianMonth(),
                persianCalendar.getPersianYear(), mLeapCalender.isPersianLeapYear());
        int dayNamePosition = mDateConstants.getDaysPosition().get(persianCalendar.getPersianWeekDayName());
        if (dayNamePosition < 5) {
            dayNamePosition = dayNamePosition + 2;
        } else {
            if (dayNamePosition == 5)
                dayNamePosition = 0;
            else
                dayNamePosition = 1;
        }
        int persianDay = towDaysAhead[2];
        int persianMonth = towDaysAhead[1];
        int persianYear = towDaysAhead[0];
        mWorkoutDate.setDayName(mDateConstants.getDaysName().get(dayNamePosition));
        mWorkoutDate.setDay(persianDay);
        mWorkoutDate.setMonth(persianMonth);
        mWorkoutDate.setYear(persianYear);
        mLeapCalender = new PersianCalendar();
        mLeapCalender.setPersianDate(towDaysAhead[0] - 1, 1, 1);
        mWorkoutDate.setYearLeap(mLeapCalender.isPersianLeapYear());
        mDateAdapter = new DateAdapter(mWorkoutDate);
        mWorkoutDaysModel = mDateAdapter.build();
        mDatePickerAdapter.setData(mWorkoutDaysModel);
    }

    /**
     * sets up mealTabChanger
     */
    private void setUpMealTabChanger() {
        String[] titles = new String[]{"صبحانه", "ناهار", "شام", "میان وعده"};
        customTabChanger.build(mActivity, mView, this, titles);
    }

    /**
     * gets data from data base and sets it
     * into the the today workout segment
     */
    private void setUpTodayWorkout() {
        homeWorkoutRecAdapter.setData(null);
        recyclerViewWorkout.setNestedScrollingEnabled(false);
        helper.setUpRecyclerView(mActivity, recyclerViewWorkout, homeWorkoutRecAdapter, false);
    }

    private void setUpMoves(int position) {
        if (mUserState == UserState.ENABLED_PROGRAM) {
            mProgramViewModel.getEnabledPrograms().observe(getViewLifecycleOwner(), new Observer<ProgramsEntity>() {
                @Override
                public void onChanged(ProgramsEntity programsEntity) {
                    if (programsEntity == null)
                        return;
                    int[] startOfProgram = dateConverter.convertDateToPersianDate(programsEntity.getProgramStartTime());
                    positionInMonth = dateHandler
                            .calculateDateDifference(mWorkoutDaysModel.getMonth()[position],
                                    mWorkoutDaysModel.getDate()[position], startOfProgram);
                    positionInMonth.subscribe(new Consumer<Integer>() {
                        @Override
                        public void accept(Integer dateDiff) throws Exception {
                            try {
                                // program is enabled
                                if (0 <= dateDiff && dateDiff <= 30) {
                                    Integer selectedDayPosition = mDateConstants.getDaysPosition().get(mWorkoutDaysModel.getDayName()[position]);
                                    char[] programDayPosition = programsEntity.getProgramWorkoutDays().toCharArray();
                                    for (char c : programDayPosition) {
                                        if (String.valueOf(c).equals(selectedDayPosition.toString())) {
                                            mDateViewModel.getWorkoutByDayInMonth(selectedDayPosition)
                                                    .map(new Function<Integer, Integer>() {
                                                        @Override
                                                        public Integer apply(Integer integer) throws Exception {
                                                            mProgramViewModel
                                                                    .getDayMovesObservable(integer)
                                                                    .observeOn(AndroidSchedulers.mainThread())
                                                                    .subscribe(new MaybeObserver<List<WorkoutModel>>() {
                                                                        @Override
                                                                        public void onSubscribe(Disposable d) {

                                                                        }

                                                                        @Override
                                                                        public void onSuccess(List<WorkoutModel> workoutModels) {
                                                                            try {
                                                                                if (workoutModels.size() != 0) {
                                                                                    checkWorkoutRestDay(0);
                                                                                    homeWorkoutRecAdapter.updateData(workoutModels);
                                                                                    mWorkoutSelectedDay = integer;
                                                                                }
                                                                            } catch (Exception e) {
                                                                            }
                                                                        }

                                                                        @Override
                                                                        public void onError(Throwable e) {

                                                                        }

                                                                        @Override
                                                                        public void onComplete() {
                                                                        }
                                                                    });
                                                            return integer;
                                                        }
                                                    })
                                                    .subscribe();
                                            break;
                                        } else {
                                            checkWorkoutRestDay(1);
                                            mWorkoutSelectedDay = -1;
                                        }
                                    }
                                } else { // program is disabled
                                    checkWorkoutRestDay(-1);
                                }
                            } catch (Exception e) {

                            }
                        }
                    });
                }
            });
        }
    }

    /**
     * sets up TodayMeal and make it rtl and
     * gives the view pager change animation
     */
    private void setUpTodayMeal() {
        mHomeMealRecAdapter = new MealRecAdapter(mActivity, null);
        recyclerViewMeals.setNestedScrollingEnabled(false);
        helper.setUpRecyclerView(mActivity, recyclerViewMeals, mHomeMealRecAdapter, false);
        recyclerViewMeals.setOnTouchListener(new OnSwipeTouchListener(mActivity) {
            public void onSwipeRight() {
                if (mealCurrentPage != 3) {
                    mealCurrentPage++;
                    customTabChanger.selectTab(mealCurrentPage);
                }
            }

            public void onSwipeLeft() {
                if (mealCurrentPage != 0) {
                    mealCurrentPage--;
                    customTabChanger.selectTab(mealCurrentPage);
                }
            }
        });
    }

    private void setUpMealDateChange(int position) {
        mProgramViewModel.getEnabledPrograms().observe(getViewLifecycleOwner(), new Observer<ProgramsEntity>() {
            @Override
            public void onChanged(ProgramsEntity programsEntity) {
                int[] startOfProgram = dateConverter.convertDateToPersianDate(programsEntity.getProgramStartTime());
                positionInMonth = dateHandler
                        .calculateDateDifference(mWorkoutDaysModel.getMonth()[position],
                                mWorkoutDaysModel.getDate()[position], startOfProgram);
                positionInMonth
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Consumer<Integer>() {
                            @Override
                            public void accept(Integer dateDiff) throws Exception {
                                Log.d(TAG, "dateDiff: " + dateDiff);
                                if (0 <= dateDiff && dateDiff <= 30) {
                                    mDietSelectedDay = dateDiff % programsEntity.getProgramDietDays().length();
                                    setTodayMealData(mDietSelectedDay, 0);
                                    customTabChanger.selectTab(0);
                                    checkDietRestDay(0);
                                } else { // program is disabled
                                    checkDietRestDay(-1);
                                }
                            }
                        });
            }
        });
    }

    private void setTodayMealData(int dayPosition, int mealType) {
        if (mUserState == UserState.ENABLED_PROGRAM) {
            mProgramViewModel.getDayMealsByType(dayPosition, mealType).observe(getViewLifecycleOwner(),
                    new Observer<List<MealModel>>() {
                        @Override
                        public void onChanged(List<MealModel> mealModels) {
                            try {
                                mHomeMealRecAdapter.updateData(mealModels);
                            } catch (Exception e) {
                            }
                        }
                    });
        } else {
            mHomeMealRecAdapter.updateData(DataSource.getMealByType(dayPosition, String.valueOf(mealType)));
        }
    }

    private void setState(UserState userState) {
        if (userState == UserState.DISABLED_PROGRAM) {
            customTabChanger.setTabChangerSelectorColor(mActivity, true);
            linearLayoutDisableWorkout.setVisibility(View.VISIBLE);
            linearLayoutDisableMeals.setVisibility(View.VISIBLE);
            linearLayoutDisableCharts.setVisibility(View.VISIBLE);
            constraintLayoutNoProgram.setVisibility(View.VISIBLE);
            buttonMoreWorkout.setEnabled(false);
            buttonMoreMeal.setEnabled(false);
            textViewTodayTitle.setTextColor(mOffColor);
            buttonMoreWorkout.setBackgroundResource(R.drawable.more_button_style_off);
            buttonMoreMeal.setBackgroundResource(R.drawable.more_button_style_off);
            textViewValueCarb.setTextColor(mOffColor);
            textViewValueFat.setTextColor(mOffColor);
            textViewValueProtein.setTextColor(mOffColor);
            barCarb.setBackgroundResource(R.drawable.column_style_off);
            barFat.setBackgroundResource(R.drawable.column_style_off);
            barProtein.setBackgroundResource(R.drawable.column_style_off);
            progressbarColeries.getProgressDrawable().setColorFilter(
                    mActivity.getResources().getColor(R.color.gray_program_day), PorterDuff.Mode.SRC_IN);
            setUpGetProgram();
        } else {
            customTabChanger.setTabChangerSelectorColor(mActivity, false);
            linearLayoutDisableWorkout.setVisibility(View.GONE);
            linearLayoutDisableMeals.setVisibility(View.GONE);
            linearLayoutDisableCharts.setVisibility(View.GONE);
            constraintLayoutNoProgram.setVisibility(View.GONE);
            buttonMoreWorkout.setEnabled(true);
            buttonMoreMeal.setEnabled(true);
            textViewTodayTitle.setTextColor(mOnColor);
            buttonMoreWorkout.setBackgroundResource(R.drawable.more_button_style);
            buttonMoreMeal.setBackgroundResource(R.drawable.more_button_style);
            textViewValueCarb.setTextColor(mResources.getColor(R.color.culomn_carb));
            textViewValueFat.setTextColor(mResources.getColor(R.color.column_fat));
            textViewValueProtein.setTextColor(mResources.getColor(R.color.culomn_protein));
            barCarb.setBackgroundResource(R.drawable.column_carb_style);
            barFat.setBackgroundResource(R.drawable.column_fat_style);
            barProtein.setBackgroundResource(R.drawable.column_protein_style);
//            ProgressBarAnimation anim = new ProgressBarAnimation(progressbarColeries, 0, 60);
//            anim.setDuration(1000);
//            progressbarColeries.startAnimation(anim);
            progressbarColeries.getProgressDrawable().setColorFilter(
                    mActivity.getResources().getColor(R.color.chart_indicator), PorterDuff.Mode.SRC_IN);
        }
    }

    private void checkWorkoutRestDay(int isRestDay) {
        Observable
                .create(new ObservableOnSubscribe<Object>() {
                    @Override
                    public void subscribe(ObservableEmitter<Object> emitter) throws Exception {
                        if (isRestDay == 1) {
                            textViewRestDay.setText(isRestDayText);
                            textViewRestDay.setVisibility(View.VISIBLE);
                            textViewDotsLeft.setVisibility(View.INVISIBLE);
                            textViewDotsRight.setVisibility(View.INVISIBLE);
                            recyclerViewWorkout.setVisibility(View.INVISIBLE);
                            buttonMoreWorkout.setVisibility(View.INVISIBLE);
                            textViewTodayTitle.setVisibility(View.GONE);
                        } else if (isRestDay == 0) {
                            textViewRestDay.setVisibility(View.GONE);
                            textViewDotsLeft.setVisibility(View.VISIBLE);
                            textViewDotsRight.setVisibility(View.VISIBLE);
                            recyclerViewWorkout.setVisibility(View.VISIBLE);
                            buttonMoreWorkout.setVisibility(View.VISIBLE);
                            textViewTodayTitle.setVisibility(View.VISIBLE);
                        } else {
                            textViewRestDay.setText(isNoProgramDay);
                            textViewRestDay.setVisibility(View.VISIBLE);
                            textViewDotsLeft.setVisibility(View.INVISIBLE);
                            textViewDotsRight.setVisibility(View.INVISIBLE);
                            recyclerViewWorkout.setVisibility(View.INVISIBLE);
                            buttonMoreWorkout.setVisibility(View.INVISIBLE);
                            textViewTodayTitle.setVisibility(View.GONE);
                        }
                    }
                })
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
    }

    private void checkDietRestDay(int isRestDay) {
        Observable
                .create(new ObservableOnSubscribe<Object>() {
                    @Override
                    public void subscribe(ObservableEmitter<Object> emitter) throws Exception {
                        if (isRestDay == 1) {

                        } else if (isRestDay == 0) {
                            textViewNoProgram.setVisibility(View.GONE);
                            textView0.setVisibility(View.VISIBLE);
                            textView1.setVisibility(View.VISIBLE);
                            textView2.setVisibility(View.VISIBLE);
                            textView3.setVisibility(View.VISIBLE);
                            recyclerViewMeals.setVisibility(View.VISIBLE);
                            constraintStatsBg.setVisibility(View.VISIBLE);
                            progressbarBack.setVisibility(View.VISIBLE);
                            buttonMoreMeal.setVisibility(View.VISIBLE);
                        } else {
                            textViewNoProgram.setVisibility(View.VISIBLE);
                            textView0.setVisibility(View.GONE);
                            textView1.setVisibility(View.GONE);
                            textView2.setVisibility(View.GONE);
                            textView3.setVisibility(View.GONE);
                            constraintStatsBg.setVisibility(View.GONE);
                            recyclerViewMeals.setVisibility(View.INVISIBLE);
                            buttonMoreMeal.setVisibility(View.INVISIBLE);
                        }
                    }
                })
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
    }

    @SuppressLint("CheckResult")
    @Override
    public void onTabChangeListener(int position) {
        mealCurrentPage = position;
        setTodayMealData(mDietSelectedDay, position);
    }

    @Override
    public void onDateChangeListener(int position) {
        if (mUserState == UserState.ENABLED_PROGRAM) {
            setUpMoves(position);
            setUpMealDateChange(position);
        }
    }

    @Override
    public void onClick(View v) {
        // prevents double click
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        switch (v.getId()) {
            case R.id.buttonMoreMeal:
                mActivity.isMain = false;
                todayProgramsFragment = new TodayProgramsFragment();
                todayProgramsFragment.programType = 1;
                todayProgramsFragment.setDay(mWorkoutSelectedDay, mDietSelectedDay);
                helper.transitToFragment(mActivity, todayProgramsFragment, true);
                mActivity.setViewPagerVisibility(false);
                mActivity.setNavViewVisibility(false);
                return;
            case R.id.buttonMoreWorkout:
                mActivity.isMain = false;
                todayProgramsFragment = new TodayProgramsFragment();
                todayProgramsFragment.setDay(mWorkoutSelectedDay, mDietSelectedDay);
                todayProgramsFragment.programType = 0;
                helper.transitToFragment(mActivity, todayProgramsFragment, true);
                mActivity.setViewPagerVisibility(false);
                mActivity.setNavViewVisibility(false);

                return;
            case R.id.imageViewCalender:
                bottomSheetFragment.show(mActivity.getSupportFragmentManager(), null);
                return;
            case R.id.imageViewProfile:
                mActivity.isMain = false;
                helper.transitToFragment(mActivity, new ProfileFragment(), true);
                return;
            case R.id.imageQuestion:
                mActivity.isMain = false;
                helper.transitToFragment(mActivity, new QuestionFragment(), true);
                return;
            case R.id.imageSubscribe:
                mActivity.isMain = false;
                helper.transitToFragment(mActivity, new SubscriptionFragment(), true);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        disposable.clear();
    }
}