package com.e.pcoach.ui.helper

enum class ProgramType {
    MEAL, WORKOUT
}
