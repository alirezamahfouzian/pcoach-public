package com.e.pcoach.ui.main.tools;

import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import com.e.pcoach.R;
import com.e.pcoach.ui.helper.ActivityHelper;
import com.e.pcoach.ui.main.MainActivity;
import com.e.pcoach.ui.main.tools.rm.InputFilterMinMax;
import com.e.pcoach.ui.main.tools.rm.RmModel;
import com.e.pcoach.ui.main.tools.rm.RmRecAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class ToolsFragment extends DaggerFragment implements View.OnFocusChangeListener {
    private static final String TAG = "ToolsFragment";
    @Inject
    CompositeDisposable disposable;
    @BindView(R.id.editTextReps)
    EditText editTextReps;
    @BindView(R.id.editTextWeight)
    EditText editTextWeight;
    @Inject
    ActivityHelper.Helper helper;
    @BindView(R.id.recyclerViewRm)
    RecyclerView recyclerViewRm;
    private ToolsViewModel stateViewModel;
    private MainActivity mActivity;
    private float mRm;
    private int mLastPercentage = 125;
    private RmRecAdapter rmRecAdapter;
    private long mLastClickTime = 0;
    private boolean isFirstTime = true;

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible && isResumed() && isFirstTime) {
            isFirstTime = false;
            build();
        }
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        stateViewModel =
                ViewModelProviders.of(this).get(ToolsViewModel.class);
        mActivity = (MainActivity) getActivity();
        View root = inflater.inflate(R.layout.fragment_tools, container, false);
        ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void build() {
        init();
        setUpRmRec();
        setTextChangeLister();
    }

    private void init() {
        editTextWeight.setFilters(new InputFilter[]{new InputFilterMinMax(1, 500)});
        editTextReps.setFilters(new InputFilter[]{new InputFilterMinMax(1, 20)});
        rmRecAdapter = new RmRecAdapter(mActivity, null);
    }

    private List<RmModel> getRmList(int weight, int reps) {
        List<RmModel> modelList = new ArrayList<>();
        mLastPercentage = 125;
        getRm(weight, reps);
        Observable.range(0, 25)
                .map(new Function<Integer, Integer>() {
                    @Override
                    public Integer apply(Integer integer) throws Exception {
                        if (!disposable.isDisposed()) {
                            RmModel model = new RmModel();
                            model.setPercentage(mLastPercentage);
                            model.setRm(getRmPercentage());
                            modelList.add(model);
                            mLastPercentage = mLastPercentage - 5;
                        }
                        return 0;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable1 -> {
                    disposable.add(disposable1);
                })
                .subscribe();
        return modelList;
    }

    /**
     * calculates the 1RM based
     * on it formula
     *
     * @return
     */
    private float getRm(int weight, int reps) {
        float rm = weight / (1.0278f - (reps * 0.0278f));
        mRm = rm;
        return mRm;
    }

    /**
     * calculates the 1RM by the Last
     * Percentage that the #setUpPercentages created
     *
     * @return
     */
    private int getRmPercentage() {
        float rmPercentage = mRm * mLastPercentage / 100;
        return Math.round(rmPercentage);
    }

    private void setUpRmRec() {
        rmRecAdapter.setData(getRmList(20, 6));
        helper.setUpRecyclerView(mActivity, recyclerViewRm, rmRecAdapter, false);
    }

    private void setTextChangeLister() {
        editTextWeight.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString();
                if (editTextReps.getText().length() != 0 && text.length() != 0) {
                    String weight = editTextWeight.getText().toString().replace(" kg", "");
                    rmRecAdapter.setData(getRmList(Integer.valueOf(weight),
                            Integer.valueOf(editTextReps.getText().toString())));
                }
            }
        });
        editTextWeight.setOnFocusChangeListener(this);
        editTextReps.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString();
                if (editTextWeight.getText().length() != 0 && text.length() != 0) {
                    String weight = editTextWeight.getText().toString().replace(" kg", "");
                    rmRecAdapter.setData(getRmList(Integer.valueOf(weight),
                            Integer.valueOf(editTextReps.getText().toString())));
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        disposable.dispose();
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus) {
            if (editTextWeight.getText().toString().contains(" kg")) {
                String noKg = editTextWeight.getText().toString().replace(" kg", "");
                editTextWeight.setText(noKg);
            }
            return;
        }
        if (editTextWeight.getText().length() != 0) {
            if (editTextWeight.getText().toString().contains(" kg"))
                return;
            String text = editTextWeight.getText() + " kg";
            editTextWeight.setText(text);
        }
    }
}
