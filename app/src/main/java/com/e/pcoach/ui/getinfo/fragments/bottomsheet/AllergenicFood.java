package com.e.pcoach.ui.getinfo.fragments.bottomsheet;

import java.util.List;

public class AllergenicFood {
    List<Integer> allergenicFoods;

    public List<Integer> getAllergenicFoods() {
        return allergenicFoods;
    }

    public void setAllergenicFoods(List<Integer> allergenicFoods) {
        this.allergenicFoods = allergenicFoods;
    }
}
