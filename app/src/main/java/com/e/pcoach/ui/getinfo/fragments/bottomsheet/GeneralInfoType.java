package com.e.pcoach.ui.getinfo.fragments.bottomsheet;

public enum GeneralInfoType {
    AGE, HEIGHT, EXPERIENCE, WEIGHT, WRIST, ACTIVITY, TRAINING_TIME
}
