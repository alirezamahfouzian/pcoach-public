package com.e.pcoach.ui.getinfo.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.e.pcoach.R;
import com.e.pcoach.ui.getinfo.GetInformationActivity;
import com.e.pcoach.ui.getinfo.fragments.bottomsheet.GenderType;
import com.google.android.material.chip.Chip;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;

public class BodyTypeFragment extends DaggerFragment implements
        CompoundButton.OnCheckedChangeListener {

    @BindView(R.id.constraintLayoutMan)
    ConstraintLayout constraintLayoutMan;
    @BindView(R.id.constraintLayoutWoman)
    ConstraintLayout constraintLayoutWoman;

    private GetInformationActivity mActivity;
    private long mLastClickTime = 0;
    private Chip[] menChipList = new Chip[20];
    private Chip[] womenChipList = new Chip[20];
    private View mView;
    private List<Integer> maleBodyTypeList;
    private List<Integer> femaleBodyTypeList;
    private int mGenderType;
    private boolean isFirstTime = true;

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible && isResumed() && isFirstTime) {
            isFirstTime = false;
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mActivity = (GetInformationActivity) getActivity();
        mView = inflater.inflate(R.layout.fragment_body_type, container, false);
        ButterKnife.bind(this, mView);
        return mView;

    }

    public void refreshGender() {
        mGenderType = mActivity.generalInformation.getGenderType();
        switch (mGenderType) {
            case GenderType.MALE:
                mActivity.setFabForwardClick(false);
                constraintLayoutMan.setVisibility(View.VISIBLE);
                constraintLayoutWoman.setVisibility(View.GONE);
                if (maleBodyTypeList == null) {
                    initManBodyTypeLists();
                    initMenChips();
                }
                return;
            case  GenderType.FEMALE:
                mActivity.setFabForwardClick(false);
                constraintLayoutWoman.setVisibility(View.VISIBLE);
                constraintLayoutMan.setVisibility(View.GONE);
                if (femaleBodyTypeList == null) {
                    initWomanBodyTypeLists();
                    initWomenChips();
                }
        }
    }

    private void initManBodyTypeLists() {
        maleBodyTypeList = new ArrayList<>();
        for (int i = 0; i < menChipList.length; i++) {
            maleBodyTypeList.add(0);
        }
    }

    private void initWomanBodyTypeLists() {
        femaleBodyTypeList = new ArrayList<>();
        for (int i = 0; i < menChipList.length; i++) {
            femaleBodyTypeList.add(0);
        }
    }

    private void initMenChips() {
        for (int i = 0; i < menChipList.length; i++) {
            int id = mActivity.getResources().getIdentifier("chipM" + i, "id",
                    mActivity.getPackageName());
            menChipList[i] = mView.findViewById(id);
            menChipList[i].setOnCheckedChangeListener(this);
        }
    }

    private void initWomenChips() {
        for (int i = 0; i < womenChipList.length; i++) {
            int id = mActivity.getResources().getIdentifier("chipW" + i, "id",
                    mActivity.getPackageName());
            womenChipList[i] = mView.findViewById(id);
            womenChipList[i].setOnCheckedChangeListener(this);
        }
    }

    public void setBodyTypes() {
        if (mGenderType == GenderType.MALE)
            mActivity.bodyTypeInformation.setBodyTypeQuestions(maleBodyTypeList);
        else
            mActivity.bodyTypeInformation.setBodyTypeQuestions(femaleBodyTypeList);
    }

    public void checkData() {
        int count = 0;
        if (mGenderType == GenderType.MALE) {
            for (Integer integer : maleBodyTypeList) {
                if (integer == 1) {
                    count++;
                }
                if (count >= 1) {
                    mActivity.setFabForwardClick(true);
                    break;
                } else {
                    mActivity.setFabForwardClick(false);
                }
            }
        } else {
            for (Integer integer : femaleBodyTypeList) {
                if (integer == 1) {
                    count++;
                }
                if (count >= 1) {
                    mActivity.setFabForwardClick(true);
                    break;
                } else {
                    mActivity.setFabForwardClick(false);
                }
            }
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (mGenderType == GenderType.MALE) {
            for (int i = 0; i < menChipList.length; i++) {
                if (menChipList[i].getId() == buttonView.getId()) {
                    if (isChecked)
                        maleBodyTypeList.set(i, 1);
                    else
                        maleBodyTypeList.set(i, 0);
                }
            }
        } else {
            for (int i = 0; i < womenChipList.length; i++) {
                if (womenChipList[i].getId() == buttonView.getId()) {
                    if (isChecked)
                        femaleBodyTypeList.set(i, 1);
                    else
                        femaleBodyTypeList.set(i, 0);
                }
            }
        }
        checkData();
    }

}
