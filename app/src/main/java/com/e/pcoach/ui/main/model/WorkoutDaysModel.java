package com.e.pcoach.ui.main.model;

public class WorkoutDaysModel {
    private String[] dayName;
    private int[] date;
    private int[] month;
    private int[] year;

    public int[] getMonth() {
        return month;
    }

    public void setMonth(int[] month) {
        this.month = month;
    }

    public int[] getYear() {
        return year;
    }

    public void setYear(int[] year) {
        this.year = year;
    }

    public String[] getDayName() {
        return dayName;
    }

    public void setDayName(String[] dayName) {
        this.dayName = dayName;
    }

    public int[] getDate() {
        return date;
    }

    public void setDate(int[] date) {
        this.date = date;
    }
}
