package com.e.pcoach.ui.main.programs.adapter;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.e.pcoach.R;
import com.e.pcoach.ui.helper.ActivityHelper;
import com.e.pcoach.ui.helper.MealTypeCaster;
import com.e.pcoach.ui.main.home.mealviewpager.MealRecAdapter;
import com.e.pcoach.ui.main.model.MealModel;

import java.util.List;

public class MealCustomView extends ConstraintLayout {

    private RecyclerView mRecyclerViewProgram;
    private ProgramMealRecAdapter mHomeMealRecAdapter;
    private ActivityHelper.Helper mHelper;

    public void updateData(List<MealModel> mealModels) {
        mHomeMealRecAdapter.updateData(mealModels);
    }

    public MealCustomView(Context context) {
        super(context);
        getlistproduct();
    }

    public MealCustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        getlistproduct();
    }

    public MealCustomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        getlistproduct();
    }

    private void getlistproduct() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.row_custom_view_meals
                , this, true);
        mRecyclerViewProgram = view.findViewById(R.id.recyclerViewProgram);
    }

    /**
     * TODO: give a fully fetched mealModel to the build so we dont have data slowing down problem
     *
     * @param mealModels
     */
    public void build(List<MealModel> mealModels, ActivityHelper.Helper helper) {
        mHelper = helper;
        mHomeMealRecAdapter = new ProgramMealRecAdapter(getContext(), mealModels);
        mRecyclerViewProgram.setNestedScrollingEnabled(false);
        mRecyclerViewProgram.setHasFixedSize(true);
        mHelper.setUpRecyclerView(getContext(), mRecyclerViewProgram, mHomeMealRecAdapter, false);
    }

}
