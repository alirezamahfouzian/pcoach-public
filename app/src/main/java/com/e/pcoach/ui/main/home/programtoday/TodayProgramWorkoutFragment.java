package com.e.pcoach.ui.main.home.programtoday;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.e.pcoach.R;
import com.e.pcoach.ui.helper.ActivityHelper;
import com.e.pcoach.ui.main.model.WorkoutModel;
import com.e.pcoach.ui.main.programs.ProgramViewModel;
import com.e.pcoach.ui.main.programs.adapter.WorkoutRecAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;

public class TodayProgramWorkoutFragment extends DaggerFragment {

    public int today;
    @Inject
    ActivityHelper.Helper helper;
    @Inject
    WorkoutRecAdapter workoutRecAdapter;
    @BindView(R.id.todayRecyclerViewWorkoutProgram)
    RecyclerView recyclerViewWorkoutProgram;
    private Activity mActivity;
    private View mView;
    private ProgramViewModel mProgramViewModel;
    private int mDayPosition = 0;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        mActivity = getActivity();
        mView = inflater.inflate(R.layout.fragment_program_workout_today, container, false);
        ButterKnife.bind(this, mView);
        build();
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    private void build() {
        setUpWorkout();
        setUpMoves();
    }

    private void setUpWorkout() {
        List<WorkoutModel> workoutModels = new ArrayList<>();
        workoutRecAdapter.setData(workoutModels);
        helper.setUpRecyclerView(mActivity, recyclerViewWorkoutProgram,
                workoutRecAdapter, false);
    }

    void setDependency(ProgramViewModel programViewModel, int dayPosition) {
        mProgramViewModel = programViewModel;
        mDayPosition = dayPosition;
    }

    private void setUpMoves() {
        mProgramViewModel.getDayMoves(mDayPosition).observe(getViewLifecycleOwner(), new Observer<List<WorkoutModel>>() {
            @Override
            public void onChanged(List<WorkoutModel> workoutModels) {
                try {
                    workoutRecAdapter.updateData(workoutModels);
                } catch (Exception ignored) {

                }
            }
        });
    }

}