package com.e.pcoach.ui.helper;

public enum UserState {
    ENABLED_PROGRAM, DISABLED_PROGRAM, NO_PROGRAM
}
