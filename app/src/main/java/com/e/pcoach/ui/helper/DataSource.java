package com.e.pcoach.ui.helper;

import android.content.Context;
import android.util.SparseIntArray;

import com.e.pcoach.db.entities.MovesEntity;
import com.e.pcoach.ui.main.model.MealModel;
import com.e.pcoach.ui.main.model.WorkoutModel;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class DataSource {


    private static final String TAG = "DataSource";
    public static boolean isProgramOn = false;
    public static UserState userState = UserState.DISABLED_PROGRAM;
    static MovesEntity[] movesEntities = new MovesEntity[]{
            new MovesEntity("aa", "جلوبازو چکشی", "https://fdjflkds"),
            new MovesEntity("ab", "جلوبازو تمرکزی", "https://fdjflkds"),
            new MovesEntity("ac", "جلوبازو هالر", "https://fdjflkds"),
            new MovesEntity("ad", "جلوبازو تک دست", "https://fdjflkds"),
            new MovesEntity("ae", "جلوبازو خم میز شیبدار", "https://fdjflkds"),
            new MovesEntity("af", "جلوبازو ez", "https://fdjflkds"),
            new MovesEntity("ag", "جلوبازو دنبل متناوب", "https://fdjflkds"),
            new MovesEntity("ah", "جلوبازو دست باز", "https://fdjflkds")
    };
    static List<Integer> WorkoutSelectedDays = new ArrayList<>();
    static List<Integer> dietSelectedDays = new ArrayList<>();
    private static SparseIntArray mMonthDayCount = new SparseIntArray();
    // workout
    private static int[] mWorkoutPositionInWeek = new int[]{
            0, 0, 0, 0, 0, 0, 0,
            2, 2, 2, 2, 2, 2, 2,
            5, 5, 5, 5, 5, 5, 5
    };
    private static String[] mWorkoutName = new String[]{
            "سرشانه هالتر", "شانه آرنولدی", "نشر از بقل", "نشر از جلو", "سرشانه سیم کش بقل", "سرشانه سیم کش از جلو", "سرشانه خم", "جلو بازو هالتر",
            "سرشانه خم", "نشر از بقل", "جلو بازو هالتر", "سرشانه سیم کش از جلو", "شانه آرنولدی", "نشر از جلو", "سرشانه سیم کش بقل", "سرشانه هالتر",
            "نشر از بقل", "نشر از جلو", "سرشانه خم", "سرشانه سیم کش از جلو", "جلو بازو هالتر", "سرشانه سیم کش بقل", "شانه آرنولدی", "سرشانه هالتر"
    };
    private static int[] mWorkoutReps = new int[]{
            10, 12, 13, 10, 10, 12, 13,
            12, 10, 13, 10, 13, 10, 12,
            13, 10, 12, 10, 13, 10, 12
    };
    private static int[] mWorkoutSets = new int[]{
            3, 4, 3, 3, 3, 3, 4,
            3, 3, 3, 3, 4, 3, 4,
            3, 3, 4, 3, 3, 3, 4
    };
    private static int[] mWorkoutPercentage = new int[]{
            75, 60, 55, 75, 80, 90, 86,
            60, 75, 75, 86, 55, 80, 90,
            60, 75, 75, 86, 80, 55, 90

    };
    private static String[] mWorkoutPic = new String[]{
            "i00001", "i00002", "i00001", "i00002",
            "i00001", "i00002", "i00002",
            "i00001", "i00002", "i00001", "i00002",
            "i00001", "i00002", "i00002",
            "i00001", "i00002", "i00001", "i00002",
            "i00001", "i00002", "i00002",
    };
    // meal
    private static int[] mMealPositionInWeek = new int[]{
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
            2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    };
    private static String[] mMealName = new String[]{
            "پنیر لیقوان", "شیر", "نان سنگک", "جوجه", "سبزی", "ماست", "پلو", "تخم مرغ", "ماست", "سالاد",
            "ماست", "تخم مرغ", "نان سنگک",
            "پنیر لیقوان", "شیر", "نان سنگک", "جوجه", "ماست", "پلو", "تخم مرغ", "ماست", "سالاد",
            "ماست", "تخم مرغ", "نان سنگک",
            "پنیر لیقوان", "شیر", "نان سنگک", "جوجه", "ماست", "پلو", "تخم مرغ", "ماست", "سالاد",
            "ماست", "تخم مرغ", "نان سنگک",
            "پنیر لیقوان", "شیر", "نان سنگک", "جوجه", "ماست", "پلو", "تخم مرغ", "ماست", "سالاد",
            "ماست", "تخم مرغ", "نان سنگک",
            "پنیر لیقوان", "شیر", "نان سنگک", "جوجه", "ماست", "پلو", "تخم مرغ", "ماست", "سالاد",
            "ماست", "تخم مرغ", "نان سنگک",
            "پنیر لیقوان", "شیر", "نان سنگک", "جوجه", "ماست", "پلو", "تخم مرغ", "ماست", "سالاد",
            "ماست", "تخم مرغ", "نان سنگک",
            "پنیر لیقوان", "شیر", "نان سنگک", "جوجه", "ماست", "پلو", "تخم مرغ", "ماست", "سالاد",
            "ماست", "تخم مرغ", "نان سنگک"
    };
    private static String[] mMealType = new String[]{
            "0", "0", "0", "1", "1", "1", "1", "2", "2", "2", "3", "3", "3",
            "0", "0", "0", "1", "1", "1", "1", "2", "2", "2", "3", "3", "3",
            "0", "0", "0", "1", "1", "1", "1", "2", "2", "2", "3", "3", "3"
    };
    private static String[] mMealAmount = new String[]{
            "50", "1", "5", "200", "175", "250", "10", "3", "150", "100", "200", "4", "10",
            "50", "1", "5", "200", "250", "10", "3", "150", "100", "200", "4", "10",
            "50", "1", "5", "200", "250", "10", "3", "150", "100", "200", "4", "10",
            "50", "1", "5", "200", "250", "10", "3", "150", "100", "200", "4", "10",
            "50", "1", "5", "200", "250", "10", "3", "150", "100", "200", "4", "10",
            "50", "1", "5", "200", "250", "10", "3", "150", "100", "200", "4", "10",
            "50", "1", "5", "200", "250", "10", "3", "150", "100", "200", "4", "10",
    };
    private static String[] mMealAmountType = new String[]{
            "گرم", "لیوان", "کف دست", "گرم", "گرم", "گرم", "قاشق", "عدد", "گرم", "گرم", "گرم", "عدد", "کف دست",
            "گرم", "لیوان", "کف دست", "گرم", "گرم", "گرم", "قاشق", "عدد", "گرم", "گرم", "گرم", "عدد", "کف دست",
            "گرم", "لیوان", "کف دست", "گرم", "گرم", "گرم", "قاشق", "عدد", "گرم", "گرم", "گرم", "عدد", "کف دست"
    };
    private String[] mMealNote = new String[]{};

    public DataSource() {
        setMonthDayCount();
    }

    public static List<Integer> getWorkoutSelectedDays() {
        WorkoutSelectedDays.clear();
        WorkoutSelectedDays.add(0);
        WorkoutSelectedDays.add(2);
        WorkoutSelectedDays.add(5);
        return WorkoutSelectedDays;
    }

    public static List<Integer> getDietSelectedDays() {
        dietSelectedDays.clear();
        dietSelectedDays.add(0);
        dietSelectedDays.add(1);
        dietSelectedDays.add(2);
        return dietSelectedDays;
    }

    public static List<WorkoutModel> getWorkoutByDay(int selectedDay) {
        List<WorkoutModel> list = new ArrayList<>();
        CompositeDisposable disposable = new CompositeDisposable();
        Observable.range(0, mWorkoutPositionInWeek.length)
                .map(new Function<Integer, Integer>() {
                    @Override
                    public Integer apply(Integer i) throws Exception {
                        if (mWorkoutPositionInWeek[i] == selectedDay) {
                            WorkoutModel workoutModel = new WorkoutModel();
                            workoutModel.setPositionInWeek(mWorkoutPositionInWeek[i]);
                            workoutModel.setWorkoutTitle(mWorkoutName[i]);
                            workoutModel.setWorkoutSet(mWorkoutSets[i]);
                            workoutModel.setWorkoutRep(mWorkoutReps[i]);
                            workoutModel.setWorkoutPic(mWorkoutPic[i]);
                            workoutModel.setWorkoutPercentage(mWorkoutPercentage[i]);
                            list.add(workoutModel);
                        }
                        return 0;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable::add)
                .doOnComplete(disposable::clear)
                .subscribe();
        return list;
    }

//    public static List<MealModel> getMeal(int selectedDay) {
//        List<MealModel> list = new ArrayList<>();
//        for (int i = 0; i < mMealPositionInWeek.length; i++) {
//        if (mMealPositionInWeek[i] == selectedDay) {
//            MealModel mealModel = new MealModel();
//            mealModel.setPositionInWeek(mMealPositionInWeek[i]);
//            mealModel.setMealName(mMealName[i]);
//            mealModel.setMealType(mMealType[i]);
//            mealModel.setMealAmount(mMealAmount[i]);
//            mealModel.setMealAmountType(mMealAmountType[i]);
//            list.add(mealModel);
//        }
//    }
//        return list;
//}

    public static List<MealModel> getMealByType(int selectedDay, String mealType) {
        List<MealModel> list = new ArrayList<>();
        CompositeDisposable disposable = new CompositeDisposable();
        Observable.range(0, mMealPositionInWeek.length)
                .map(new Function<Integer, Integer>() {
                    @Override
                    public Integer apply(Integer i) throws Exception {
                        if (mMealPositionInWeek[i] == selectedDay && mMealType[i].equals(mealType)) {
                            MealModel mealModel = new MealModel();
                            mealModel.setPositionInWeek(mMealPositionInWeek[i]);
                            mealModel.setMealName(mMealName[i]);
                            mealModel.setMealType(mMealType[i]);
                            mealModel.setMealAmount(mMealAmount[i]);
                            mealModel.setMealAmountType(mMealAmountType[i]);
                            list.add(mealModel);
                        }
                        return 0;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable::add)
                .doOnComplete(disposable::clear)
                .subscribe();
        return list;
    }

    public static List<String> getQuestions(Context context) {
        CompositeDisposable disposable = new CompositeDisposable();
        List<String> questions = new ArrayList<>();
        Observable
                .create(new ObservableOnSubscribe<Object>() {
                    @Override
                    public void subscribe(ObservableEmitter<Object> emitter) throws Exception {

                        int id;
                        String item;
                        for (int i = 0; i < 7; i++) {
                            id = context.getResources().getIdentifier("q_position_" + i,
                                    "string", context.getPackageName());
                            item = context.getResources().getString(id);
                            questions.add(item);
                        }
                    }
                })
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(disposable::add)
                .doOnComplete(disposable::clear)
                .subscribe();
        return questions;
    }

    public static String getAnswers(Context context, int position) {
        int id = context.getResources().getIdentifier("q_position_" + position + "_answer",
                "string", context.getPackageName());
        return context.getResources().getString(id);
    }

    public static MovesEntity[] getMovesEntities() {
        return movesEntities;
    }

    public static void setMonthDayCount() {
        CompositeDisposable disposable = new CompositeDisposable();
        Observable
                .create(new ObservableOnSubscribe<Object>() {
                    @Override
                    public void subscribe(ObservableEmitter<Object> emitter) throws Exception {
                        mMonthDayCount.put(1, 31);
                        mMonthDayCount.put(2, 31);
                        mMonthDayCount.put(3, 31);
                        mMonthDayCount.put(4, 31);
                        mMonthDayCount.put(5, 31);
                        mMonthDayCount.put(6, 31);
                        mMonthDayCount.put(7, 30);
                        mMonthDayCount.put(8, 30);
                        mMonthDayCount.put(9, 30);
                        mMonthDayCount.put(10, 30);
                        mMonthDayCount.put(11, 30);
                        mMonthDayCount.put(12, 29);
                    }
                })
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(disposable::add)
                .doOnComplete(disposable::clear)
                .subscribe();
    }

    public static int getMonthDayCount(int month) {
        return mMonthDayCount.get(month);
    }
}
