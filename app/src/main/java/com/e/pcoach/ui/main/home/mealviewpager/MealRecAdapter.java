package com.e.pcoach.ui.main.home.mealviewpager;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.e.pcoach.R;
import com.e.pcoach.ui.main.model.MealModel;

import java.util.List;


public class MealRecAdapter extends RecyclerView.Adapter<MealRecAdapter.ViewHolder> {
    private Context mContext;
    private List<MealModel> mMealModelList;

    public MealRecAdapter(Context context, List<MealModel> workoutModels) {
        mContext = context;
        mMealModelList = workoutModels;
    }

    public void updateData(List<MealModel> mealModel) {
        mMealModelList = mealModel;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.row_home_workout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (mMealModelList == null)
            return;

        if (position == 3) {
            holder.mTextViewName.setText("...");
            holder.mTextViewAmount.setText("...");
            return;
        }

        if (mMealModelList.size() <= position)
            return;

        holder.mTextViewName.setText(mMealModelList.get(position).getMealName());
        holder.mTextViewName.setText(mMealModelList.get(position).getMealName());
        holder.mTextViewAmount.setText(mMealModelList.get(position)
                .getMealAmountType() + " " +
                mMealModelList.get(position).getMealAmount()
        );
    }

    @Override
    public int getItemCount() {
        return 4;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView mTextViewName;
        TextView mTextViewAmount;

        private ViewHolder(@NonNull View itemView) {
            super(itemView);
            init();
        }

        private void init() {
            mTextViewName = itemView.findViewById(R.id.textViewName);
            mTextViewAmount = itemView.findViewById(R.id.textViewAmount);
        }
    }
}
