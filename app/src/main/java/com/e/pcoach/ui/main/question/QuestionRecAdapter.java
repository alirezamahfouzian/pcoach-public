package com.e.pcoach.ui.main.question;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.e.pcoach.R;
import com.e.pcoach.ui.main.tools.rm.RmModel;

import java.util.List;

import javax.inject.Inject;

public class QuestionRecAdapter extends RecyclerView.Adapter<QuestionRecAdapter.ViewHolder> {

    private static final String TAG = "HomeWorkoutRecAdapterr";
    private Context mContext;
    private List<String> mQuestions;
    private QuestionClickListener clickListener;
    @Inject
    public QuestionRecAdapter(Context context) {
        mContext = context;
    }

    public void setData(List<String> questions, QuestionClickListener listener) {
        mQuestions = questions;
        clickListener = listener;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext)
                .inflate(R.layout.row_question, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.mTextViewQuestion.setText(mQuestions.get(position));
        holder.itemView.setOnClickListener(v -> {
            clickListener.onRecyclerViewItemClickListener(position);
        });
    }

    @Override
    public int getItemCount() {
        return mQuestions.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView mTextViewQuestion;

        private ViewHolder(@NonNull View itemView) {
            super(itemView);
            init();
        }

        private void init() {
            mTextViewQuestion = itemView.findViewById(R.id.textViewQuestion);
        }
    }

    interface QuestionClickListener {
        void onRecyclerViewItemClickListener(int position);
    }
}