package com.e.pcoach.ui.getinfo.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import androidx.annotation.Nullable;

import com.e.pcoach.R;
import com.e.pcoach.ui.getinfo.GetInformationActivity;
import com.google.android.material.chip.Chip;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;

public class AllergenicFragment extends DaggerFragment implements
        CompoundButton.OnCheckedChangeListener {

    private long mLastClickTime = 0;
    private GetInformationActivity mActivity;
    private Chip[] dayChipList = new Chip[7];
    private View mView;
    private List<Integer> allergenicFoods;
    private boolean isFirstTime = true;

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible && isResumed() && isFirstTime) {
            isFirstTime = false;
            build();
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = getLayoutInflater().inflate(R.layout.fragment_allergenic, container, false);
        ButterKnife.bind(this, mView);
        mActivity = (GetInformationActivity) getActivity();
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void build() {
        init();
    }

    private void init() {
        initChips();
        initAllergenicFoodFirstTime();
    }

    private void initAllergenicFoodFirstTime() {
        allergenicFoods = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            allergenicFoods.add(0);
        }
    }

    private void initChips() {
        int j = 1;
        for (int i = 0; i < dayChipList.length; i++) {
            int id = mActivity.getResources().getIdentifier("chipA" + j, "id",
                    mActivity.getPackageName());
            dayChipList[i] = mView.findViewById(id);
            dayChipList[i].setOnCheckedChangeListener(this);
            j++;
        }
    }

    public void setStatus() {
        mActivity.allergenicFragmentInformation.setAllergenicFoods(allergenicFoods);
    }

    public void checkData() {
        mActivity.setFabForwardClick(true);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        for (int i = 0; i < dayChipList.length; i++) {
            if (dayChipList[i].getId() == buttonView.getId()) {
                if (isChecked)
                    allergenicFoods.set(i, 1);
                else
                    allergenicFoods.set(i, 0);
            }
        }

        checkData();
    }

}
