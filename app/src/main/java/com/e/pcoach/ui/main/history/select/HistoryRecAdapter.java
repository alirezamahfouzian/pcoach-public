package com.e.pcoach.ui.main.history.select;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.e.pcoach.R;
import com.e.pcoach.libraries.persiandatepicker.util.PersianCalendar;
import com.e.pcoach.network.ServerDateConverter;
import com.e.pcoach.ui.main.history.HistoryModel;

import java.util.List;

public class HistoryRecAdapter extends RecyclerView.Adapter<HistoryRecAdapter.ViewHolder> {

    private Context mContext;
    private List<HistoryModel> mModelList;
    private RecyclerViewClick mRecyclerViewClick;
    private ServerDateConverter mDateConverter;
    private PersianCalendar mPersianCalendar;
    int[] mDate;

    public HistoryRecAdapter(Context context) {
        this.mContext = context;
    }

    public void setData(List<HistoryModel> modelList , RecyclerViewClick recyclerViewClick) {
        mModelList = modelList;
        mRecyclerViewClick = recyclerViewClick;
        mDateConverter = new ServerDateConverter();
        mPersianCalendar = new PersianCalendar();
    }

    public void updateData(List<HistoryModel> modelList) {
        mModelList = modelList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext)
                .inflate(R.layout.row_history_select, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        mDate = mDateConverter
                .convertDateToPersianDate(mModelList.get(position).getCreatedDate());
        holder.mTextViewName.setText(mModelList.get(position).getTitle());
        holder.mTextViewMonthName.setText(getMonthName(position));
        holder.mTextViewYear.setText(String.valueOf(mDate[0]));
        holder.mTextViewShow.setOnClickListener(v -> {
            mRecyclerViewClick.recOncClickListener(mModelList.get(position));
        });
    }

    @Override
    public int getItemCount() {
        return mModelList.size();
    }

    private String getMonthName(int position) {
        mPersianCalendar.setPersianDate(mDate[0], mDate[1], mDate[2]);
        return mPersianCalendar.getPersianMonthName();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView mTextViewName;
        TextView mTextViewMonthName;
        TextView mTextViewYear;
        TextView mTextViewShow;

        private ViewHolder(@NonNull View itemView) {
            super(itemView);
            init();
        }

        private void init() {
            mTextViewName = itemView.findViewById(R.id.textViewName);
            mTextViewMonthName = itemView.findViewById(R.id.textViewMonthName);
            mTextViewYear = itemView.findViewById(R.id.textViewYear);
            mTextViewShow = itemView.findViewById(R.id.textViewShow);
        }
    }

    public interface RecyclerViewClick {
        void recOncClickListener(HistoryModel historyModel);
    }
}