package com.e.pcoach.ui.getinfo.fragments.bottomsheet;

public class GiModel {

    private GeneralInfoType generalInfoType;
    private String weightG = null, wristMm = null;
    private int age = -1, height = -1, weightKg = -1, wristCm = -1, experience = -1,
            activityType = -1, genderType = -1, trainingMin = -1;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public int getWeightKg() {
        return weightKg;
    }

    public void setWeightKg(int weightKg) {
        this.weightKg = weightKg;
    }

    public String getWeightG() {
        return weightG;
    }

    public void setWeightG(String weightG) {
        this.weightG = weightG;
    }

    public int getWristCm() {
        return wristCm;
    }

    public void setWristCm(int wristCm) {
        this.wristCm = wristCm;
    }

    public String getWristMm() {
        return wristMm;
    }

    public void setWristMm(String wristMm) {
        this.wristMm = wristMm;
    }

    public int getActivityType() {
        return activityType;
    }

    public void setActivityType(int activityType) {
        this.activityType = activityType;
    }

    public GeneralInfoType getGeneralInfoType() {
        return generalInfoType;
    }

    public void setGeneralInfoType(GeneralInfoType generalInfoType) {
        this.generalInfoType = generalInfoType;
    }

    public int getGenderType() {
        return genderType;
    }

    public void setGenderType(int genderType) {
        this.genderType = genderType;
    }

    public int getTrainingMin() {
        return trainingMin;
    }

    public void setTrainingMin(int trainingMin) {
        this.trainingMin = trainingMin;
    }
}
