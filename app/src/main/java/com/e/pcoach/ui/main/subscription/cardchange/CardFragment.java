package com.e.pcoach.ui.main.subscription.cardchange;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.e.pcoach.R;

public class CardFragment extends Fragment {

    private static final String POSITION_KEY = "POSITION_KEY";
    private View mView;
    private LinearLayout linearLayoutHeader;
    private LinearLayout buttonSubmit;
    private TextView textViewTitle;
    private TextView textViewPrice;
    private TextView textViewDescOne;
    private TextView textViewDescTwo;
    private CardView cardView;
    private Activity mActivity;
    private Resources mResources;
    private int mPosition;

    public static Fragment getInstance(int position) {
        CardFragment f = new CardFragment();
        Bundle args = new Bundle();
        args.putInt(POSITION_KEY, position);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPosition = getArguments().getInt(POSITION_KEY);
        }
    }

    @SuppressLint("DefaultLocale")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mActivity = getActivity();
        mResources = mActivity.getResources();
        mView = inflater.inflate(R.layout.row_subscrioption, container, false);
        init();
        cardView.setMaxCardElevation(cardView.getCardElevation() * CardAdapter.MAX_ELEVATION_FACTOR);
        setUpDesign();
        return mView;
    }

    private void init() {
        textViewTitle = mView.findViewById(R.id.textViewTitle);
        textViewDescOne = mView.findViewById(R.id.textViewDescOne);
        textViewDescTwo = mView.findViewById(R.id.textViewDescTwo);
        linearLayoutHeader = mView.findViewById(R.id.linearLayoutHeader);
        textViewPrice = mView.findViewById(R.id.textViewPrice);
        buttonSubmit = mView.findViewById(R.id.buttonSubmit);
        cardView = mView.findViewById(R.id.cardView);
    }


    private void setUpDesign() {
        switch (mPosition) {
            case 0:
                setUpLayout("8,490", "یک ماهه",
                        mResources.getColor(R.color.sub_text_green),
                        R.drawable.sub_but_color_green,
                        mResources.getString(R.string.sub_text_desc_one_one),
                        mResources.getString(R.string.sub_text_desc_one_two)
                );
                return;
            case 1:
                setUpLayout("7,490", "دو ماهه",
                        mResources.getColor(R.color.sub_text_orange),
                        R.drawable.sub_but_color_orange,
                        mResources.getString(R.string.sub_text_desc_two_one),
                        mResources.getString(R.string.sub_text_desc_two_two)
                );
                return;
            case 2:
                setUpLayout("6,999", "سه ماهه",
                        mResources.getColor(R.color.sub_text_purple),
                        R.drawable.sub_but_color_purple,
                        mResources.getString(R.string.sub_text_desc_three_one),
                        mResources.getString(R.string.sub_text_desc_three_two));
                return;
            case 3:
                setUpLayout("5,999", "شش ماهه",
                        mResources.getColor(R.color.sub_text_blue),
                        R.drawable.sub_but_color_blue,
                        mResources.getString(R.string.sub_text_desc_four_one),
                        mResources.getString(R.string.sub_text_desc_four_two));
        }
    }

    private void setUpLayout(String price, String subMonth, int mainColor,
                             int submitButtonColor, String descOne, String descTwo) {
        textViewTitle.setText(subMonth);
        textViewPrice.setTextColor(mainColor);
        textViewPrice.setText(price);
        textViewDescOne.setText(descOne);
        textViewDescTwo.setText(descTwo);
        linearLayoutHeader.setBackgroundColor(mainColor);
        buttonSubmit.setBackgroundResource(submitButtonColor);
    }

    public CardView getCardView() {
        return cardView;
    }
}