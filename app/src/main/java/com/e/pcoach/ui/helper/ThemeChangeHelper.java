package com.e.pcoach.ui.helper;

import android.os.Build;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.e.pcoach.R;

import javax.inject.Inject;

public class ThemeChangeHelper {

    private AppCompatActivity mContext;
    private DarkModePrefManager mSharedPref;

    @Inject
    public ThemeChangeHelper(AppCompatActivity context, DarkModePrefManager sharedPref) {
        this.mContext = context;
        this.mSharedPref = sharedPref;
    }

    public void setDarkMode(boolean isMain) {
        if (mSharedPref.isNightMode()) {
            mContext.setTheme(R.style.darkTheme);
            if (isMain) {
                changeStatusBar(0);
            }
        } else {
            mContext.setTheme(R.style.AppTheme);
            if (isMain)
                changeStatusBar(1);
        }
    }

    public void changeStatusBar(int mode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (mode == 1) {
                mContext.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            } else {
                View decorView = mContext.getWindow().getDecorView();
                int systemUiVisibilityFlags = decorView.getSystemUiVisibility();
                systemUiVisibilityFlags &= ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
                decorView.setSystemUiVisibility(systemUiVisibilityFlags);
            }
        }
    }
}
