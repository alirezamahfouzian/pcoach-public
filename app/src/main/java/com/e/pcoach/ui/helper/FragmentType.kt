package com.e.pcoach.ui.helper

enum class FragmentType {
    LOGIN_FRAGMENT, SIGNUP_FRAGMENT
}