package com.e.pcoach.ui.main.home.programtoday;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;

import com.e.pcoach.R;
import com.e.pcoach.ui.helper.ActivityHelper;
import com.e.pcoach.ui.helper.MealTypeCaster;
import com.e.pcoach.ui.main.model.MealModel;
import com.e.pcoach.ui.main.programs.ProgramViewModel;
import com.e.pcoach.ui.main.programs.adapter.MealCustomView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class TodayProgramMealFragment extends DaggerFragment {

    public int today;
    @Inject
    ActivityHelper.Helper helper;
    @Inject
    MealTypeCaster caster;
    @BindView(R.id.todaySnackCustomView)
    MealCustomView snackCustomView;
    @BindView(R.id.todayDinnerCustomView)
    MealCustomView dinnerCustomView;
    @BindView(R.id.todayLunchCustomView)
    MealCustomView lunchCustomView;
    @BindView(R.id.todayBreakFastCustomView)
    MealCustomView breakFastCustomView;
    private Activity mActivity;
    private View mView;
    private int mDayPosition = 0;
    private ProgramViewModel mProgramViewModel;
    private List<MealModel> mealBreakFastList = new ArrayList<>();
    private List<MealModel> mealLunchList = new ArrayList<>();
    private List<MealModel> mealDinnerList = new ArrayList<>();
    private List<MealModel> mealSnackList = new ArrayList<>();
    private CompositeDisposable disposable = new CompositeDisposable();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        mActivity = getActivity();
        mView = inflater.inflate(R.layout.fragment_program_meal_today, container, false);
        ButterKnife.bind(this, mView);
        build();
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        disposable.dispose();
    }

    private void build() {
        Log.d("", "build: " + mDayPosition);
        setUpMeals(mDayPosition);
        setUpMealTypeViews();
    }

    void setDependency(ProgramViewModel programViewModel, int dayPosition) {
        mProgramViewModel = programViewModel;
        mDayPosition = dayPosition;
    }

    private void setUpMealTypeViews() {
        breakFastCustomView.build(null, helper);
        lunchCustomView.build(null, helper);
        dinnerCustomView.build(null, helper);
        snackCustomView.build(null, helper);
    }

    private void setUpMeals(int dayPosition) {
        mProgramViewModel.getDayMeals(dayPosition).observe(getViewLifecycleOwner(), new Observer<List<MealModel>>() {
            @Override
            public void onChanged(List<MealModel> mealModels) {
                try {
                    mealBreakFastList.clear();
                    mealLunchList.clear();
                    mealDinnerList.clear();
                    mealSnackList.clear();
                    Observable.fromIterable(mealModels)
                            .map(new Function<MealModel, Boolean>() {
                                @Override
                                public Boolean apply(MealModel mealModel) throws Exception {
                                    switch (mealModel.getMealType()) {
                                        case "0":
                                            mealBreakFastList.add(mealModel);
                                            break;
                                        case "1":
                                            mealLunchList.add(mealModel);
                                            break;
                                        case "2":
                                            mealDinnerList.add(mealModel);
                                            break;
                                        case "3":
                                            mealSnackList.add(mealModel);
                                            break;
                                    }
                                    return true;
                                }
                            })
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new io.reactivex.Observer<Boolean>() {
                                @Override
                                public void onSubscribe(Disposable d) {
                                    disposable.add(d);
                                }

                                @Override
                                public void onNext(Boolean b) {

                                }

                                @Override
                                public void onError(Throwable e) {

                                }

                                @Override
                                public void onComplete() {
                                    breakFastCustomView.updateData(mealBreakFastList);
                                    lunchCustomView.updateData(mealLunchList);
                                    dinnerCustomView.updateData(mealDinnerList);
                                    snackCustomView.updateData(mealSnackList);
                                }
                            });
                } catch (Exception e) {
                    throw e;
                }
            }
        });
    }

}