package com.e.pcoach.ui.helper;

import android.content.Context;
import android.content.SharedPreferences;

public class DarkModePrefManager {
    // Shared preferences file name
    public static final String PREF_NAME = "darkMode";
    private static final String IS_NIGHT_MODE = "isNightMode";
    private static final String IS_THEME_CHANGED = "isThemeChanged";

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context mContext;
    // shared pref mode
    int PRIVATE_MODE = 0;

    public DarkModePrefManager(Context context) {
        this.mContext = context;
        pref = mContext.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
    }

    public void setDarkMode(boolean isDarkMode) {
        editor = pref.edit();
        editor.putBoolean(IS_NIGHT_MODE, isDarkMode);
        editor.apply();
    }

    public void setThemeChanged(boolean isThemeChanged) {
        editor = pref.edit();
        editor.putBoolean(IS_THEME_CHANGED, isThemeChanged);
        editor.apply();
    }

    public boolean isNightMode() {
        return pref.getBoolean(IS_NIGHT_MODE, false);
    }

    public boolean isThemeChanged() {
        return pref.getBoolean(IS_THEME_CHANGED, false);
    }

}