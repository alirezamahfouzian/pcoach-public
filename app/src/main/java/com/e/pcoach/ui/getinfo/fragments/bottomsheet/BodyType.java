package com.e.pcoach.ui.getinfo.fragments.bottomsheet;

import java.util.List;

public class BodyType {
    List<Integer> bodyTypeQuestions;

    public List<Integer> getBodyTypeQuestions() {
        return bodyTypeQuestions;
    }

    public void setBodyTypeQuestions(List<Integer> bodyTypeQuestions) {
        this.bodyTypeQuestions = bodyTypeQuestions;
    }
}
