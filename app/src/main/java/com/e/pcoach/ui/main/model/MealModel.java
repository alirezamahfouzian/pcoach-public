package com.e.pcoach.ui.main.model;

import androidx.room.ColumnInfo;

public class MealModel {

    @ColumnInfo(name = "meal_day_position")
    private int positionInWeek;
    @ColumnInfo(name = "meal_type")
    private String mealType;
    @ColumnInfo(name = "meal_title")
    private String mealName;
    @ColumnInfo(name = "meal_amount")
    private String mealAmount;
    @ColumnInfo(name = "meal_amount_type")
    private String mealAmountType;

    public int getPositionInWeek() {
        return positionInWeek;
    }

    public void setPositionInWeek(int positionInWeek) {
        this.positionInWeek = positionInWeek;
    }

    public String getMealType() {
        return mealType;
    }

    public void setMealType(String mealType) {
        this.mealType = mealType;
    }

    public String getMealName() {
        return mealName;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    public String getMealAmount() {
        return mealAmount;
    }

    public void setMealAmount(String mealAmount) {
        this.mealAmount = mealAmount;
    }

    public String getMealAmountType() {
        return mealAmountType;
    }

    public void setMealAmountType(String mealAmountType) {
        this.mealAmountType = mealAmountType;
    }

}
