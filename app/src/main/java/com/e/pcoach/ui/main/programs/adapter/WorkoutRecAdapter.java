package com.e.pcoach.ui.main.programs.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.e.pcoach.R;
import com.e.pcoach.ui.main.model.WorkoutModel;

import java.util.List;


public class WorkoutRecAdapter extends RecyclerView.Adapter<WorkoutRecAdapter.ViewHolder> {
    private Context mContext;
    private List<WorkoutModel> mWorkoutList;

    public WorkoutRecAdapter(Context context) {
        mContext = context;
    }

    public void setData(List<WorkoutModel> workoutModels) {
        mWorkoutList = workoutModels;
    }

    public void updateData(List<WorkoutModel> workoutModels) {
        mWorkoutList = workoutModels;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.row_program_workout, parent, false);
        return new ViewHolder(view);
    }

    /**
     * TODO: make the image ids real
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (mWorkoutList != null) {
            holder.mTextViewTitle.setText(mWorkoutList.get(position).getWorkoutTitle());
            String title = mWorkoutList.get(position).getWorkoutSet()
                    + "x " + mWorkoutList.get(position).getWorkoutRep();
            holder.mTextViewCount.setText(title);
            holder.mTextViewPercentage.setText(mWorkoutList.get(position).getWorkoutPercentage() + "%");
            int drawable = mContext.getResources().getIdentifier(mWorkoutList.get(position).getWorkoutPic(), "drawable", mContext.getPackageName());
            holder.mImageViewPic.setImageResource(drawable);
        }
    }

    @Override
    public int getItemCount() {
        if (mWorkoutList != null) {
            return mWorkoutList.size();
        } else
            return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mImageViewPic;
        TextView mTextViewTitle;
        TextView mTextViewCount;
        TextView mTextViewPercentage;

        private ViewHolder(@NonNull View itemView) {
            super(itemView);
            init();
        }

        private void init() {
            mTextViewTitle = itemView.findViewById(R.id.textViewWorkoutTitle);
            mImageViewPic = itemView.findViewById(R.id.imageViewWorkoutPic);
            mTextViewCount = itemView.findViewById(R.id.textViewRm);
            mTextViewPercentage = itemView.findViewById(R.id.textViewWorkoutPercentage);
        }
    }
}
