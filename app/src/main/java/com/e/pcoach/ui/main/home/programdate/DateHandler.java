package com.e.pcoach.ui.main.home.programdate;

import com.e.pcoach.libraries.persiandatepicker.util.PersianCalendar;
import com.e.pcoach.ui.helper.DataSource;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class DateHandler {
    private static final String TAG = "HomeFragment";
    private PersianCalendar mCalendar;
    private int dayDiff = 0;

    @Inject
    public DateHandler(PersianCalendar calendar) {
        mCalendar = calendar;
        DataSource.setMonthDayCount();
    }

    /**
     * calculates the difference between
     * today and end of the program
     * @param month
     * @param day
     * @return
     */
    public Observable<Integer> calculateDateDifference(int month, int day, int[] startOfProgram) {
        CompositeDisposable disposable = new CompositeDisposable();
        return Observable
                .create(new ObservableOnSubscribe<Integer>() {
                    @Override
                    public void subscribe(ObservableEmitter<Integer> emitter) throws Exception {
                        int monthDays = 0;
                        int inMonthDiff = 0;
                        dayDiff = 0;
                        if (month == startOfProgram[1]) {
                            dayDiff = day - startOfProgram[2];
                        } else if (month == startOfProgram[1] + 1){
                            monthDays = DataSource.getMonthDayCount(startOfProgram[1]);
                            inMonthDiff = monthDays - startOfProgram[2];
                            dayDiff = inMonthDiff + day;
                        } else {
                            dayDiff = 31;
                        }
                        emitter.onNext(dayDiff);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .doOnSubscribe(disposable::add)
                .doOnComplete(disposable::clear);
    }
}
