package com.e.pcoach.ui.getinfo.fragments.bottomsheet;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.NumberPicker;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.e.pcoach.R;
import com.e.pcoach.ui.getinfo.GetInformationActivity;
import com.e.pcoach.ui.main.MainActivity;
import com.e.pcoach.ui.main.home.HomeFragment;
import com.e.pcoach.ui.main.model.WorkoutDate;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.text.DecimalFormat;

import javax.inject.Inject;

public class InfoBottomSheetFragment extends BottomSheetDialogFragment {

    public GiModelCaster mGiModelCaster = new GiModelCaster();
    private TextView submitButton;
    private View mView;
    private GetInformationActivity mActivity;
    private GeneralInfoType mInfoType;
    // arrays for puting into numberPicker
    private String[] gramDisplayedValues;
    private String[] wristDisplayedValues;
    private String[] minuteDisplayedValues;
    private String[] activityDisplayedValues;
    private String[] experienceDisplayedValues;

    // number pickers for different layouts
    private NumberPicker numberPickerAge;
    private NumberPicker numberPickerWeightKg;
    private NumberPicker numberPickerWeightG;
    private NumberPicker numberPickerHeight;
    private NumberPicker numberPickerWristCm;
    private NumberPicker numberPickerWristMm;
    private NumberPicker numberPickerExperience;
    private NumberPicker numberPickerActivity;
    private NumberPicker numberPickerMinute;

    /**
     * get called when submitButton get clicked
     */
    private SubmitListener mListener;
    private DecimalFormat numberFormat = new DecimalFormat("#.0");

    @Inject
    public InfoBottomSheetFragment(GetInformationActivity activity) {
        this.mActivity = activity;
    }

    public void setType(GeneralInfoType infoType) {
        mInfoType = infoType;
    }

    public void setSubmitListener(SubmitListener listener) {
        mListener = listener;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

    }

    /**
     * for adding the style to bottom sheet
     */
    @Override
    public int getTheme() {
        return R.style.BottomSheetDialogTheme;
    }

    /**
     * for setting the rounded corner theme and making it fully extend
     * when it opens up
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(mActivity, getTheme());
        bottomSheetDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dia) {
                BottomSheetDialog dialog = (BottomSheetDialog) dia;
                FrameLayout bottomSheet = dialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                // for making it fully open not half
                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
                BottomSheetBehavior.from(bottomSheet).setSkipCollapsed(true);
                BottomSheetBehavior.from(bottomSheet).setHideable(true);
            }
        });
        return bottomSheetDialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        switch (mInfoType) {
            case AGE:
                mView = inflater.inflate(R.layout.row_bottom_sheet_age, container, false);
                return mView;
            case HEIGHT:
                mView = inflater.inflate(R.layout.row_bottom_sheet_height, container, false);
                return mView;
            case WEIGHT:
                mView = inflater.inflate(R.layout.row_bottom_sheet_weight, container, false);
                return mView;
            case WRIST:
                mView = inflater.inflate(R.layout.row_bottom_sheet_wrist, container, false);
                return mView;
            case ACTIVITY:
                mView = inflater.inflate(R.layout.row_bottom_sheet_activity, container, false);
                return mView;
            case TRAINING_TIME:
                mView = inflater.inflate(R.layout.row_bottom_sheet_training_time, container, false);
                return mView;
            default:
                mView = inflater.inflate(R.layout.row_bottom_sheet_experience, container, false);
                return mView;
        }


    }

    @Override
    public void onResume() {
        super.onResume();
        build();
        handleDataTransfermation();
    }

    public void build() {
        submitButton = mView.findViewById(R.id.submitButton);
        switch (mInfoType) {
            case AGE:
                buildAge();
                return;
            case HEIGHT:
                buildHeight();
                return;
            case WEIGHT:
                buildWeight();
                return;
            case WRIST:
                buildWrist();
                return;
            case ACTIVITY:
                buildActivity();
                return;
            case EXPERIENCE:
                buildExperience();
                return;
            case TRAINING_TIME:
                buildTrainingTime();
        }
    }

    private void buildAge() {
        numberPickerAge = mView.findViewById(R.id.numberPickerAge);
        numberPickerAge.setMaxValue(120);
        numberPickerAge.setMinValue(10);
        if (mActivity.generalInformation.getAge() != -1) {
            numberPickerAge.setValue(mActivity.generalInformation.getAge());
            return;
        }
        numberPickerAge.setValue(25);
    }

    private void buildHeight() {
        numberPickerHeight = mView.findViewById(R.id.numberPickerHeight);
        numberPickerHeight.setMaxValue(220);
        numberPickerHeight.setMinValue(130);
        if (mActivity.generalInformation.getHeight() != -1) {
            numberPickerHeight.setValue(mActivity.generalInformation.getHeight());
            return;
        }
        numberPickerHeight.setValue(173);
    }

    private void buildWeight() {
        numberPickerWeightKg = mView.findViewById(R.id.numberPickerWeightKg);
        numberPickerWeightG = mView.findViewById(R.id.numberPickerWeightG);
        setUpGramValues();
        numberPickerWeightKg.setMaxValue(200);
        numberPickerWeightKg.setMinValue(40);
        numberPickerWeightG.setMinValue(0);
        numberPickerWeightG.setMaxValue(gramDisplayedValues.length - 1);
        numberPickerWeightG.setDisplayedValues(gramDisplayedValues);
        if (mActivity.generalInformation.getWeightKg() != -1 && mActivity.generalInformation.getWeightG() != null) {
            numberPickerWeightKg.setValue(mActivity.generalInformation.getWeightKg());
            numberPickerWeightG.setValue(mGiModelCaster.castWeight(mActivity.generalInformation
                    .getWeightG(), gramDisplayedValues));
            return;
        }
        numberPickerWeightKg.setValue(82);
        numberPickerWeightG.setValue(0);
    }

    private void buildWrist() {
        setUpWristSizeValues();
        numberPickerWristCm = mView.findViewById(R.id.numberPickerWristCm);
        numberPickerWristMm = mView.findViewById(R.id.numberPickerWristMm);
        numberPickerWristCm.setMinValue(10);
        numberPickerWristCm.setMaxValue(30);
        numberPickerWristMm.setMinValue(0);
        numberPickerWristMm.setMaxValue(wristDisplayedValues.length - 1);
        numberPickerWristMm.setDisplayedValues(wristDisplayedValues);
        if (mActivity.generalInformation.getWristCm() != -1 && mActivity.generalInformation.getWristMm() != null) {
            numberPickerWristCm.setValue(mActivity.generalInformation.getWristCm());
            numberPickerWristMm.setValue(mGiModelCaster.castWrist(mActivity.generalInformation.getWristMm(), wristDisplayedValues));
            return;
        }
        numberPickerWristCm.setValue(17);
    }

    private void buildExperience() {
        numberPickerExperience = mView.findViewById(R.id.numberPickerExperience);
        experienceDisplayedValues = new String[]{"مبتدی (کمتر از 6 ماه)",
                "نیمه حرفه ای (کمتر از 1 سال)",
                "حرفه ای (کمتر از 2 سال)",
                "فوق حرفه ای (بیشتر از 2 سال)"
        };
        numberPickerExperience.setMinValue(0);
        numberPickerExperience.setMaxValue(experienceDisplayedValues.length - 1);
        numberPickerExperience.setWrapSelectorWheel(false);
        numberPickerExperience.setDisplayedValues(experienceDisplayedValues);
        if (mActivity.generalInformation.getExperience() != ExperienceType.NONE) {
            numberPickerExperience
                    .setValue(mActivity.generalInformation.getExperience());
            return;
        }
        numberPickerExperience.setValue(1);
    }

    private void buildActivity() {
        numberPickerActivity = mView.findViewById(R.id.numberPickerActivity);
        activityDisplayedValues = new String[]{"کم (فعالیت روزانه)",
                "متوسط (روزانه نیم ساعت ورزش سبک)",
                "زیاد (روزی یک ساعت ورزش سنگین)", "بسیار زیاد (ورزشکار حرفه ای)"
        };
        numberPickerActivity.setMinValue(0);
        numberPickerActivity.setMaxValue(activityDisplayedValues.length - 1);
        numberPickerActivity.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        numberPickerActivity.setWrapSelectorWheel(false);
        numberPickerActivity.setDisplayedValues(activityDisplayedValues);
        if (mActivity.generalInformation.getActivityType() != ActivityType.NONE) {
            numberPickerActivity
                    .setValue(mActivity.generalInformation.getActivityType());
            return;
        }
        numberPickerActivity.setValue(1);
    }

    private void buildTrainingTime() {
        numberPickerMinute = mView.findViewById(R.id.numberPickerMinute);
        setUpMinuteValues();
        numberPickerMinute.setMinValue(0);
        numberPickerMinute.setMaxValue(minuteDisplayedValues.length - 1);
        numberPickerMinute.setDisplayedValues(minuteDisplayedValues);
        if (mActivity.generalInformation.getTrainingMin() != -1) {
            numberPickerMinute.setValue(mGiModelCaster.castMinute(String.valueOf(mActivity
                    .generalInformation.getTrainingMin()), minuteDisplayedValues));
            return;
        }
        numberPickerMinute.setValue(5);
    }

    /**
     * makes numbers multiplied by
     * 100 for gram number picker values
     */
    private void setUpGramValues() {
        int NUMBER_OF_VALUES = 10; //num of values in the picker
        int PICKER_RANGE = 100;
        gramDisplayedValues = new String[NUMBER_OF_VALUES];
        for (int i = 0; i < NUMBER_OF_VALUES; i++) {
            if (i == 0)
                gramDisplayedValues[i] = String.valueOf(0);
            else
                gramDisplayedValues[i] = String.valueOf(PICKER_RANGE * (i));
        }
    }

    /**
     * makes numbers multiplied by
     * 100 for gram number picker values
     */
    private void setUpWristSizeValues() {
        int NUMBER_OF_VALUES = 10; //num of values in the picker
        int PICKER_RANGE = 10;
        wristDisplayedValues = new String[NUMBER_OF_VALUES];
        for (int i = 0; i < NUMBER_OF_VALUES; i++) {
            if (i == 0)
                wristDisplayedValues[i] = String.valueOf(0);
            else {
                wristDisplayedValues[i] = String.valueOf(PICKER_RANGE * (i));
            }
        }
    }

    /**
     * makes numbers multiplied by
     * 100 for gram number picker values
     */
    private void setUpMinuteValues() {
        int NUMBER_OF_VALUES = 17; //num of values in the picker
        int PICKER_RANGE = 5;
        int j = 4;
        minuteDisplayedValues = new String[NUMBER_OF_VALUES];
        for (int i = 0; i < NUMBER_OF_VALUES; i++) {
            minuteDisplayedValues[i] = String.valueOf(PICKER_RANGE * (j));
            j++;
        }
    }

    /**
     * setting the submit button onClicklistener we getting
     * selected date from date picker and setting it to {@link WorkoutDate}
     * so we can pass it to listener and we receive
     * it from {@link MainActivity}
     * and it send it to {@link HomeFragment}
     */
    private void handleDataTransfermation() {
        GiModel giModel = new GiModel();
        submitButton.setOnClickListener(v -> {
            switch (mInfoType) {
                case AGE:
                    giModel.setGeneralInfoType(GeneralInfoType.AGE);
                    giModel.setAge(numberPickerAge.getValue());
                    mListener.setOnSubmitListener(giModel);
                    return;
                case HEIGHT:
                    giModel.setGeneralInfoType(GeneralInfoType.HEIGHT);
                    giModel.setHeight(numberPickerHeight.getValue());
                    mListener.setOnSubmitListener(giModel);
                    return;
                case WEIGHT:
                    giModel.setGeneralInfoType(GeneralInfoType.WEIGHT);
                    giModel.setWeightKg(numberPickerWeightKg.getValue());
                    giModel.setWeightG(gramDisplayedValues[numberPickerWeightG.getValue()]);
                    mListener.setOnSubmitListener(giModel);
                    return;
                case WRIST:
                    giModel.setGeneralInfoType(GeneralInfoType.WRIST);
                    giModel.setWristCm(numberPickerWristCm.getValue());
                    giModel.setWristMm(wristDisplayedValues[numberPickerWristMm.getValue()]);
                    mListener.setOnSubmitListener(giModel);
                    return;
                case ACTIVITY:
                    giModel.setGeneralInfoType(GeneralInfoType.ACTIVITY);
                    giModel.setActivityType(numberPickerActivity.getValue());
                    mListener.setOnSubmitListener(giModel);
                    return;
                case EXPERIENCE:
                    giModel.setGeneralInfoType(GeneralInfoType.EXPERIENCE);
                    giModel.setExperience(numberPickerExperience.getValue());
                    mListener.setOnSubmitListener(giModel);
                    return;
                case TRAINING_TIME:
                    giModel.setGeneralInfoType(GeneralInfoType.TRAINING_TIME);
                    giModel.setTrainingMin(Integer.parseInt(minuteDisplayedValues[numberPickerMinute.getValue()]));
                    mListener.setOnSubmitListener(giModel);
            }
        });
    }

    /**
     * for handling data transferring between bottomSheet and Fragment or mActivity
     */
    public interface SubmitListener {
        void setOnSubmitListener(GiModel giModel);
    }
}