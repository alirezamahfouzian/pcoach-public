package com.e.pcoach.ui.main.history;

import androidx.room.ColumnInfo;

public class HistoryModel {

    @ColumnInfo(name = "program_id")
    private int programId;
    @ColumnInfo(name = "program_title")
    private String title;
    @ColumnInfo(name = "program_created_time")
    private String createdDate;

    public HistoryModel(int programId, String title, String createdDate) {
        this.programId = programId;
        this.title = title;
        this.createdDate = createdDate;
    }

    public int getProgramId() {
        return programId;
    }

    public String getTitle() {
        return title;
    }

    public String getCreatedDate() {
        return createdDate;
    }
}
