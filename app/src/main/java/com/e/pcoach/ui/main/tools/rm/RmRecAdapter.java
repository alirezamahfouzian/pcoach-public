package com.e.pcoach.ui.main.tools.rm;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.e.pcoach.R;

import java.util.List;

import javax.inject.Inject;

public class RmRecAdapter extends RecyclerView.Adapter<RmRecAdapter.ViewHolder> {

    private Context mContext;
    private List<RmModel> mModelList;

    @Inject
    public RmRecAdapter(Context context, List<RmModel> modelList) {
        mModelList = modelList;
        this.mContext = context;
    }

    public void setData(List<RmModel> modelList) {
        mModelList = modelList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext)
                .inflate(R.layout.row_tools_rm, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.mTextViewRm.setText("" + mModelList.get(position).getRm());
        holder.mTextViewPercentage.setText("" + mModelList.get(position).getPercentage());
    }

    @Override
    public int getItemCount() {
        return 25;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView mTextViewRm;
        TextView mTextViewPercentage;

        private ViewHolder(@NonNull View itemView) {
            super(itemView);
            init();
        }

        private void init() {
            mTextViewRm = itemView.findViewById(R.id.textViewRm);
            mTextViewPercentage = itemView.findViewById(R.id.textViewQuestion);
        }
    }
}