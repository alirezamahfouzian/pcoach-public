package com.e.pcoach.ui.getinfo.fragments.bottomsheet;

import com.e.pcoach.libraries.persiandatepicker.util.PersianCalendar;
import com.e.pcoach.libraries.persiandatepicker.util.PersianDateParser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class GiModelCaster {


    public int castWeight(String item, String[] gramDisplayedValues) {
        int position = 0;
        for (int i = 0; i < gramDisplayedValues.length; i++) {
            if (gramDisplayedValues[i].equals(item)) {
                position = i;
                break;
            }
        }
        return position;
    }
    
    public float combineWeight(int kg, String gram) {
        return kg + (Float.valueOf(gram) / 1000);
    }

    public float combineWrist(int cm, String mm) {
        return cm + (Float.valueOf(mm) / 100);
    }

    public int castMinute(String item, String[] minuteDisplayedValues) {
        int position = 0;
        for (int i = 0; i < minuteDisplayedValues.length; i++) {
            if (minuteDisplayedValues[i].equals(item)) {
                position = i;
                break;
            }
        }
        return position;
    }

    public int castWrist(String item, String[] wristDisplayedValues) {
        int position = 0;
        for (int i = 0; i < wristDisplayedValues.length; i++) {
            if (wristDisplayedValues[i].equals(item)) {
                position = i;
                break;
            }
        }
        return position;
    }
    
    public String getActivityTitle(int activityType) {
        switch (activityType) {
            case 0:
                return "کم";
            case 1:
                return "متوسط";
            case 2:
                return "زیاد";
            default:
                return "بسیار زیاد";
        }
    }

    public String getExperienceTitle(int experienceType) {
        switch (experienceType) {
            case 0:
                return "مبتدی";
            case 1:
                return "نیمه حرفه ای";
            case 2:
                return "حرفه ای";
            default:
                return "فوق حرفه ای";
        }
    }

    public String castIntegerListToString(List<Integer> integers) {
        StringBuilder strings = new StringBuilder();
        for (Integer integer : integers) {
            strings.append(integer);
        }
        return strings.toString();
    }

    public String extractSelectedDays(List<Integer> integers) {
        StringBuilder strings = new StringBuilder();
        for (int i = 0; i < integers.size(); i++) {
            if (integers.get(i) == 1) {
                strings.append(i);
            }
        }
        return strings.toString();
    }

    public String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }
}
