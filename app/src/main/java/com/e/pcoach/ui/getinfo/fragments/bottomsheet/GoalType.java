package com.e.pcoach.ui.getinfo.fragments.bottomsheet;

public class GoalType {

    public static final int NONE = -1;
    public static final int GAIN = 0;
    public static final int LOSE = 1;
    public static final int BOTH = 2;
    public static final int HEALTH = 3;
}

