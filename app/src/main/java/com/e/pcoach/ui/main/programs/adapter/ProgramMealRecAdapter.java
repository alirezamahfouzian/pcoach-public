package com.e.pcoach.ui.main.programs.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.e.pcoach.R;
import com.e.pcoach.ui.helper.MealTypeCaster;
import com.e.pcoach.ui.main.model.MealModel;

import java.util.List;


public class ProgramMealRecAdapter extends RecyclerView.Adapter<ProgramMealRecAdapter.ViewHolder> {
    private Context mContext;
    private List<MealModel> mMealModelList;


    public ProgramMealRecAdapter(Context context, List<MealModel> workoutModels) {
        mContext = context;
        mMealModelList = workoutModels;

    }

    public void updateData(List<MealModel> mealModelList) {
        mMealModelList = mealModelList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.row_home_workout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (mMealModelList != null) {
            holder.mTextViewName.setText(mMealModelList.get(position).getMealName());
            String amount = mMealModelList.get(position).getMealAmountType() + " " +
                    mMealModelList.get(position).getMealAmount();
            holder.mTextViewAmount.setText(amount);
        }
    }

    @Override
    public int getItemCount() {
        if (mMealModelList == null)
            return 0;
        return mMealModelList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView mTextViewName;
        TextView mTextViewAmount;

        private ViewHolder(@NonNull View itemView) {
            super(itemView);
            init();
        }

        private void init() {
            mTextViewName = itemView.findViewById(R.id.textViewName);
            mTextViewAmount = itemView.findViewById(R.id.textViewAmount);
        }
    }
}
