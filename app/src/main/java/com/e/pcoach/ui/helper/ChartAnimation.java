package com.e.pcoach.ui.helper;

import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ProgressBar;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;

public class ChartAnimation extends Animation {
    private Guideline progressBar;
    private float from;
    private float  to;
    private ConstraintLayout.LayoutParams params;

    public ChartAnimation(Guideline progressBar, float from, float to) {
        super();
        this.progressBar = progressBar;
        this.from = from;
        this.to = to;
        params = (ConstraintLayout.LayoutParams) this.progressBar.getLayoutParams();
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        super.applyTransformation(interpolatedTime, t);
        float value = from + (to - from) * interpolatedTime;
        params.guidePercent = value; // 45% // range: 0.6 min <-> 0.04 max
        progressBar.setLayoutParams(params);
    }
}
