package com.e.pcoach.ui.main.home.programtoday;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.e.pcoach.R;
import com.e.pcoach.ui.helper.ActivityHelper;
import com.e.pcoach.ui.main.MainActivity;
import com.e.pcoach.ui.main.adapter.bottomnavigation.NoSwipePager;
import com.e.pcoach.ui.main.adapter.bottomnavigation.TodayPagerSaveStateHelpAdapter;
import com.e.pcoach.ui.main.adapter.customtabchanger.CustomTabChanger;
import com.e.pcoach.ui.main.programs.ProgramViewModel;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;

public class TodayProgramsFragment extends DaggerFragment
        implements CustomTabChanger.MealTabChangeListener, View.OnClickListener {

    private static final String TAG = "ProgramsFragment";
    public int programType;
    TodayProgramWorkoutFragment todayProgramWorkoutFragment;
    TodayProgramMealFragment todayProgramMealFragment;
    @Inject
    ActivityHelper.Helper helper;
    @Inject
    CustomTabChanger customTabChanger;
    @Inject
    TodayPagerSaveStateHelpAdapter pagerAdapter;
    @BindView(R.id.todayProgramsViewPager)
    NoSwipePager programsViewPager;
    @BindView(R.id.imageViewBack)
    ImageView imageViewBack;
    @BindView(R.id.textView0)
    TextView textView0;
    @BindView(R.id.textView1)
    TextView textView1;
    private MainActivity mActivity;
    private View mView;
    private int mWorkoutSelectedDay = 0;
    private int mDietSelectedDay;
    private ProgramViewModel mProgramViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        mActivity = (MainActivity) getActivity();
        mView = inflater.inflate(R.layout.fragment_program_today, container, false);
        ButterKnife.bind(this, mView);
        mProgramViewModel = new ViewModelProvider(this).get(ProgramViewModel.class);
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        build();
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.setViewPagerVisibility(false);
        mActivity.setNavViewVisibility(false);
//        Toast.makeText(mActivity, "todayProgram resume", Toast.LENGTH_SHORT).show();
    }

    private void build() {
        Log.d(TAG, "build: " + programType);
        init();
        setUpProgramTab();
        setUpProgramTabChanger();
        selectTab();
    }

    public void setDay(int selectedDay, int dietSelectedDay) {
        mWorkoutSelectedDay = selectedDay;
        mDietSelectedDay = dietSelectedDay;
    }

    private void init() {
        imageViewBack.setOnClickListener(this);
        pagerAdapter = new TodayPagerSaveStateHelpAdapter(getChildFragmentManager());
    }

    private void selectTab() {
        if (programType == 0) {
            customTabChanger.selectTab(0);
        } else {
            customTabChanger.selectTab(1);
        }
    }

    private void setUpProgramTab() {
        //optimisation
        todayProgramWorkoutFragment = new TodayProgramWorkoutFragment();
        todayProgramWorkoutFragment.setDependency(mProgramViewModel, mWorkoutSelectedDay);
        todayProgramMealFragment = new TodayProgramMealFragment();
        todayProgramMealFragment.setDependency(mProgramViewModel, mDietSelectedDay);
        pagerAdapter.addFragments(todayProgramWorkoutFragment);
        pagerAdapter.addFragments(todayProgramMealFragment);
        programsViewPager.setOffscreenPageLimit(1);
        programsViewPager.setPagingEnabled(false);
        programsViewPager.setAdapter(pagerAdapter);
    }

    /**
     * sets up ProgramTabChanger
     */
    private void setUpProgramTabChanger() {
        if (mWorkoutSelectedDay == -1) {
            textView0.setVisibility(View.GONE);
            textView1.setClickable(false);
            Log.d(TAG, "setUpProgramTabChanger: ");
        }
        String[] titles = new String[]{"تمرینی", "غذایی"};
        customTabChanger.build(mActivity, mView, this, titles);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mActivity.setNavViewVisibility(true);
        mActivity.setViewPagerVisibility(true);
//        Toast.makeText(mActivity, "todayProgram destroy", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTabChangeListener(int position) {
        programsViewPager.setCurrentItem(position, false);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageViewBack:
                helper.removeFragment(mActivity, this);
                return;
        }
    }
}