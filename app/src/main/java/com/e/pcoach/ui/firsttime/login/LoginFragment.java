package com.e.pcoach.ui.firsttime.login;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;

import com.e.pcoach.ui.helper.FragmentType;
import com.e.pcoach.R;
import com.e.pcoach.network.retrofit.auth.AuthApi;
import com.e.pcoach.network.retrofit.auth.User;
import com.e.pcoach.ui.firsttime.FirstTimeActivity;
import com.e.pcoach.ui.helper.AddEditTextFocusEffect;
import com.e.pcoach.ui.helper.FragmentListener;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginFragment extends DaggerFragment implements View.OnClickListener {

    private static final String TAG = "LoginFragment";

    @BindView(R.id.textViewPhoneNumberHint)
    TextView textViewPhoneNumberHint;
    @BindView(R.id.editTextPhoneNumber)
    EditText editTextPhoneNumber;
    @BindView(R.id.buttonSubmitLogin)
    Button buttonSubmitLogin;
    @BindView(R.id.textViewGoToSignUp)
    TextView textViewGoToSignUp;
    @BindView(R.id.textViewPasswordHint)
    TextView textViewPasswordHint;
    @BindView(R.id.editTextPassword)
    EditText editTextPassword;

    @Inject
    AuthApi authApi;

    private View mView;
    private FirstTimeActivity mActivity;
    private LoginViewModel mViewModel;
    private FragmentListener mFragmentListener;
    private Bundle mSavedInstance;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }


    /**
     * it gets instance of the editTexts
     * texts when page changes or app ui stops
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSavedInstance = new Bundle();
        mSavedInstance = getArguments();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mActivity = (FirstTimeActivity) getActivity();
        mViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        mFragmentListener = mActivity;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, mView);
        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();

        build();
    }

    @Override
    public void onStop() {
        super.onStop();

        saveInstance();
    }

    private void build() {
        buttonSubmitLogin.setOnClickListener(this);
        restoreFragment();
        addFocusEffect();
        onClickListener();
    }

    private void onClickListener() {
        // opens sign up fragment
        textViewGoToSignUp.setOnClickListener(this);
    }

    /**
     * calls the {@link AddEditTextFocusEffect} and sets
     * the text view change effect
     */
    private void addFocusEffect() {
        new AddEditTextFocusEffect(null, null, null,
                null, editTextPhoneNumber, textViewPhoneNumberHint,
                editTextPassword, textViewPasswordHint, buttonSubmitLogin)
                .execute(FragmentType.LOGIN_FRAGMENT);
    }

    private void saveInstance() {
        mSavedInstance = new Bundle();
        mSavedInstance.putString(mFragmentListener.NUMBER_KEY, editTextPhoneNumber.getText().toString());
        mSavedInstance.putString(mFragmentListener.PASSWORD_KEY, editTextPassword.getText().toString());
        mFragmentListener.onBackPressedInstance(mSavedInstance);
    }

    /**
     * uses stored data in loginFragment whenever loginFragment starts again
     * after an onBackPressed
     */
    private void restoreFragment() {
        if (mSavedInstance != null) {
            String number = mSavedInstance.getString(mFragmentListener.NUMBER_KEY);
            String password = mSavedInstance.getString(mFragmentListener.PASSWORD_KEY);
            if (number != null) {
                editTextPhoneNumber.setText(number);
            }
            if (password != null) {
                editTextPassword.setText(password);
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonSubmitLogin:
                User userEntity = new User("alireza", "mahfouzian", "alirezamahfouzian@", "afjdlkfds", "09907211141");
                Log.d(TAG, "onClick: " + authApi);
                authApi.addUser(userEntity).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Log.d(TAG, "onResponse: " + response);
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
                return;
            case R.id.textViewGoToSignUp:
                mFragmentListener.openSignUpFragment();

        }
    }
}
