package com.e.pcoach.ui.main.home.datepicker;

import android.util.Log;

import com.e.pcoach.ui.main.model.WorkoutDate;
import com.e.pcoach.ui.main.model.WorkoutDaysModel;

public class DateAdapter {

    private static final String TAG = "DateAdapter";
    int mYear;
    private int mDayDate;
    private String mDayName;
    private int mMonthNumber;
    private boolean mIsYearLeap;
    private DateConstants mDateConstants = new DateConstants();
    private WorkoutDaysModel mWorkoutDaysModel = new WorkoutDaysModel();

    public DateAdapter(WorkoutDate workoutDate) {
        mDayDate = workoutDate.getDay();
        mDayName = workoutDate.getDayName();
        mMonthNumber = workoutDate.getMonth() - 1;
        mYear = workoutDate.getYear();
        mIsYearLeap = workoutDate.isYearLeap();
    }

    /**
     * it uses the given weekDayName(like "جمعه") {@link #mDayDate} and
     * converts it to an array that contains a list of 5 previous day before the given day
     * (like ..."جمعه" , "پنجشنبه", "چهارشنبه")
     */
    private void extractName() {
        String[] daysName = new String[5];
        Integer mDaysExtracted = mDateConstants.getDaysPosition().get(mDayName);
        for (int i = 0; i < 5; i++) {
            daysName[i] = getDayName(mDaysExtracted);
            if (mDaysExtracted == 0) mDaysExtracted = 6;
            else mDaysExtracted--;
        }
        mWorkoutDaysModel.setDayName(daysName);
    }

    /**
     * it gets a position and returns the day name by it position in week
     */
    private String getDayName(int dayPosition) {
        return mDateConstants.getDaysName().get(dayPosition);
    }

    /**
     * it uses the given date(like "28") {@link #mDayDate} and calculates
     * six date before from it
     */
    private void extractDate() {
        int[] daysDate = new int[5];
        int[] months = new int[5];
        int[] years = new int[5];
        Integer mMonthDays = mDateConstants.getMonthDayCount().get(mMonthNumber - 1);
        for (int i = 0; i < 5; i++) {
            daysDate[i] = mDayDate;
            months[i] = mMonthNumber + 1;
            years[i] = mYear;
            if (mDayDate == 1 && mMonthNumber != 0) {
                mDayDate = mMonthDays;
                mMonthNumber = mMonthNumber - 1;
            } else if (mDayDate == 1 && mMonthNumber == 0 && mIsYearLeap) {
                mDayDate = 30;
                mMonthNumber = 11;
                mYear = mYear - 1;
            } else if (mDayDate == 1 && mMonthNumber == 0 && !mIsYearLeap) {
                mDayDate = 29;
                mMonthNumber = 11;
                mYear = mYear - 1;
            } else mDayDate = mDayDate - 1;
        }
        mWorkoutDaysModel.setDate(daysDate);
        mWorkoutDaysModel.setMonth(months);
        mWorkoutDaysModel.setYear(years);
    }

    public WorkoutDaysModel build() {
        extractDate();
        extractName();
        return mWorkoutDaysModel;
    }
}
