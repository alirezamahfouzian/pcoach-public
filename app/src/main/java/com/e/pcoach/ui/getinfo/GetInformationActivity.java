package com.e.pcoach.ui.getinfo;

import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.ViewCompat;
import androidx.viewpager.widget.ViewPager;

import com.e.pcoach.R;
import com.e.pcoach.di.BaseActivity;
import com.e.pcoach.ui.getinfo.fragments.AllergenicFragment;
import com.e.pcoach.ui.getinfo.fragments.BodyTypeFragment;
import com.e.pcoach.ui.getinfo.fragments.ExerciseTargetFragment;
import com.e.pcoach.ui.getinfo.fragments.GeneralInfoFragment;
import com.e.pcoach.ui.getinfo.fragments.PlanningFragment;
import com.e.pcoach.ui.getinfo.fragments.bottomsheet.AllergenicFood;
import com.e.pcoach.ui.getinfo.fragments.bottomsheet.BodyType;
import com.e.pcoach.ui.getinfo.fragments.bottomsheet.GiModel;
import com.e.pcoach.ui.getinfo.fragments.bottomsheet.PlaningTime;
import com.e.pcoach.ui.getinfo.fragments.loading.LoadingFragment;
import com.e.pcoach.ui.helper.ActivityHelper;
import com.e.pcoach.ui.helper.DarkModePrefManager;
import com.e.pcoach.ui.helper.DataSource;
import com.e.pcoach.ui.helper.ThemeChangeHelper;
import com.e.pcoach.ui.main.adapter.bottomnavigation.NoSwipePager;
import com.e.pcoach.ui.main.adapter.bottomnavigation.PagerSaveStateHelpAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerAppCompatActivity;

public class GetInformationActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "GetInformationActivity";
    public GiModel generalInformation = new GiModel();
    public int goalType;
    public BodyType bodyTypeInformation = new BodyType();
    public AllergenicFood allergenicFragmentInformation = new AllergenicFood();
    public PlaningTime planingTimeInformation = new PlaningTime();

    @BindView(R.id.fabForward)
    public FloatingActionButton fabForward;
    @BindView(R.id.fabBackward)
    public FloatingActionButton fabBackward;
    @BindView(R.id.imageViewBack)
    ImageView imageViewBack;
    @BindView(R.id.textViewTitle)
    TextView textViewTitle;
    @BindView(R.id.viewPagerGetInfo)
    NoSwipePager viewPagerGetInfo;
    @BindView(R.id.guidelineProgress)
    Guideline guidelineProgress;
    @Inject
    PagerSaveStateHelpAdapter pagerAdapter;
    @Inject
    ActivityHelper.Helper helper;
    @Inject
    GeneralInfoFragment generalInfoFragment;
    @Inject
    BodyTypeFragment bodyTypeFragment;
    @Inject
    ExerciseTargetFragment exerciseTargetFragment;
    @Inject
    AllergenicFragment allergenicFragment;
    @Inject
    PlanningFragment planningFragment;
    @Inject
    LoadingFragment loadingFragment;
    @Inject
    DarkModePrefManager sharedPref;
    @Inject
    ThemeChangeHelper themeChangeHelper;
    @BindView(R.id.container)
    FrameLayout container;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.constraintLayoutProgressIndicator)
    ConstraintLayout constraintLayoutProgressIndicator;

    private int mPosition = 0;
    private Drawable mDrawableCheck;
    private Drawable mDrawableForward;
    private Typeface mSnackbarFont;
    private Snackbar mSnack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        themeChangeHelper.setDarkMode(true);
        helper.setRotationPortrait(this);
        setContentView(R.layout.activity_get_information);
        ButterKnife.bind(this);
        build();
    }

    private void build() {
        init();
        createSnack();
        setUpGetInfoPager();
        setUpPagingTitles();
    }

    private void init() {
        mDrawableCheck = getResources().getDrawable(R.drawable.ic_check_get_info);
        mDrawableForward = getResources().getDrawable(R.drawable.ic_backward_get_info);
        mSnackbarFont = ResourcesCompat.getFont(this, R.font.iransans_medium);
        imageViewBack.setOnClickListener(this);
        fabBackward.setOnClickListener(this);
        fabForward.setOnClickListener(this);
    }

    public void setFabForwardClick(boolean isClickable) {
        if (isClickable) {
            fabForward.setClickable(true);
            fabForward.setAlpha(1f);
            return;
        }
        fabForward.setClickable(false);
        fabForward.setAlpha(0.4f);
    }

    private void setUpGetInfoPager() {
        //optimisation
        viewPagerGetInfo.setOffscreenPageLimit(4);
        viewPagerGetInfo.setPagingEnabled(false);
        pagerAdapter.addFragments(generalInfoFragment);
        pagerAdapter.addFragments(bodyTypeFragment);
        pagerAdapter.addFragments(exerciseTargetFragment);
        pagerAdapter.addFragments(allergenicFragment);
        pagerAdapter.addFragments(planningFragment);
        viewPagerGetInfo.setAdapter(pagerAdapter);
    }

    private void handleGetInfoProgress(int position) {
        switch (position) {
            case 0:
                setProgressLength(guidelineProgress, 0.09f);
                return;
            case 1:
                setProgressLength(guidelineProgress, 0.38f);
                return;
            case 2:
                setProgressLength(guidelineProgress, 0.59f);
                return;
            case 3:
                setProgressLength(guidelineProgress, 0.78f);
                return;
            case 4:
                setProgressLength(guidelineProgress, 0.95f);

        }
    }

    private void setUpPagingTitles() {
        viewPagerGetInfo.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        textViewTitle.setText("اطلاعات کلی");
                        return;
                    case 1:
                        textViewTitle.setText("تیپ بدنی");
                        return;
                    case 2:
                        textViewTitle.setText("هدف شما");
                        return;
                    case 3:
                        textViewTitle.setText("حساسیت");
                        return;
                    case 4:
                        textViewTitle.setText("برنامه ریزی");
                        return;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    /**
     * sets bar chart height
     */
    private void setProgressLength(Guideline guideLine, Float height) {
        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) guideLine.getLayoutParams();
        params.guidePercent = height; // 45% // range: 0.6 min <-> 0.04 max
        guideLine.setLayoutParams(params);
    }

    private void createSnack() {
        mSnack = Snackbar.make(findViewById(android.R.id.content), "آیا واقعا می خوای انصراف بدی؟", 4000);
        View snackView = mSnack.getView();
        // setting the rtl direction
        ViewCompat.setLayoutDirection(snackView,ViewCompat.LAYOUT_DIRECTION_RTL);
        // setting font and color to the action and text
        TextView text = snackView.findViewById(com.google.android.material.R.id.snackbar_text);
        TextView action = snackView.findViewById(com.google.android.material.R.id.snackbar_action);
        text.setTypeface(mSnackbarFont);
        text.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.get_info_snackbar_text_size));
        action.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.get_info_snackbar_text_size));
        action.setTypeface(mSnackbarFont);
        mSnack.setBackgroundTint(getResources().getColor(R.color.red_text));
        mSnack.setActionTextColor(getResources().getColor(R.color.white));
        mSnack.setTextColor(getResources().getColor(R.color.white));
        mSnack.setAction("آره", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageViewBack:
                mSnack.show();
                return;
            case R.id.fabForward:
                setUpFabForward();
                return;
            case R.id.fabBackward:
                setUpFabBack();
        }
    }

    private void setUpFabBack() {
        if (mPosition == 1) {
            fabBackward.setVisibility(View.GONE);
            generalInfoFragment.checkData();
            setFabForwardClick(true);
        } else if (mPosition == 2) {
            bodyTypeFragment.checkData();
        } else if (mPosition == 3) {
            exerciseTargetFragment.checkData();
        } else if (mPosition == 4) {
            fabForward.setImageDrawable(mDrawableForward);
            setFabForwardClick(true);
        }
        if (mPosition != 0) {
            mPosition--;
            viewPagerGetInfo.setCurrentItem(mPosition, false);
            handleGetInfoProgress(mPosition);
        }
    }

    private void setUpFabForward() {
        if (mPosition == 0) {
            fabBackward.setVisibility(View.VISIBLE);
            bodyTypeFragment.refreshGender();
            bodyTypeFragment.checkData();
        } else if (mPosition == 1) {
            exerciseTargetFragment.checkData();
        } else if (mPosition == 2) {
            allergenicFragment.checkData();
        } else if (mPosition == 3) {
            fabForward.setImageDrawable(mDrawableCheck);
        } else if (mPosition == 4) {
            DataSource.isProgramOn = false;
            planningFragment.setStatus();
            allergenicFragment.setStatus();
            bodyTypeFragment.setBodyTypes();
            exerciseTargetFragment.setGoal();
            viewPagerGetInfo.setVisibility(View.GONE);
            toolbar.setVisibility(View.GONE);
            constraintLayoutProgressIndicator.setVisibility(View.GONE);
            fabBackward.setVisibility(View.GONE);
            fabForward.setVisibility(View.GONE);
            container.setVisibility(View.VISIBLE);
            helper.transitToFragment(this, loadingFragment, false);
        }

        if (mPosition != 5) {
            viewPagerGetInfo.setCurrentItem(mPosition + 1, false);
            mPosition++;
            handleGetInfoProgress(mPosition);
        }
    }

    @Override
    public void onBackPressed() {
        if (mPosition == 0) {
            mSnack.show();
            return;
        }
        if (mPosition == 5) {
            return;
        }
        setUpFabBack();
    }
}
