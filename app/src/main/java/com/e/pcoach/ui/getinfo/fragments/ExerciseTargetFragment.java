package com.e.pcoach.ui.getinfo.fragments;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.e.pcoach.R;
import com.e.pcoach.ui.getinfo.GetInformationActivity;
import com.e.pcoach.ui.getinfo.fragments.bottomsheet.GiModelCaster;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;


public class ExerciseTargetFragment extends DaggerFragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private static final String TAG = "ExerciseTargetFragment";
    @BindView(R.id.radioBottonGainMuscle)
    RadioButton radioButtonGainMuscle;
    @BindView(R.id.radioButtonLoseFat)
    RadioButton radioButtonLoseFat;
    @BindView(R.id.radioButtonBoth)
    RadioButton radioButtonBoth;
    @BindView(R.id.radioButtonOverallHealth)
    RadioButton radioButtonOverallHealth;
    @BindView(R.id.ConstraintLayoutGainMuscle)
    ConstraintLayout ConstraintLayoutGainMuscle;
    @BindView(R.id.constraintLayoutloseFat)
    ConstraintLayout constraintLayoutloseFat;
    @BindView(R.id.ConstraintLayoutBoth)
    ConstraintLayout ConstraintLayoutBoth;
    @BindView(R.id.ConstraintLayoutGeneralHealth)
    ConstraintLayout ConstraintLayoutGeneralHealth;

    private View mView;
    private int mGoalPosition = -1;
    private GetInformationActivity mActivity;
    private boolean isFirstTime = true;

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible && isResumed() && isFirstTime) {
            isFirstTime = false;
            build();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_exercise_target, container, false);
        ButterKnife.bind(this, mView);
        mActivity = (GetInformationActivity) getActivity();
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void build() {
        init();
    }

    private void init() {
        ConstraintLayoutGainMuscle.setOnClickListener(this);
        constraintLayoutloseFat.setOnClickListener(this);
        ConstraintLayoutBoth.setOnClickListener(this);
        ConstraintLayoutGeneralHealth.setOnClickListener(this);
    }

    private void resetRadioButtons() {
        radioButtonGainMuscle.setChecked(false);
        radioButtonGainMuscle.setOnCheckedChangeListener(this);
        radioButtonLoseFat.setChecked(false);
        radioButtonLoseFat.setOnCheckedChangeListener(this);
        radioButtonBoth.setChecked(false);
        radioButtonBoth.setOnCheckedChangeListener(this);
        radioButtonOverallHealth.setChecked(false);
        radioButtonOverallHealth.setOnCheckedChangeListener(this);
    }

    public void setGoal() {
        mActivity.goalType = mGoalPosition;
        Log.d(TAG, "setGoal: " + mActivity.goalType);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ConstraintLayoutGainMuscle:
                resetRadioButtons();
                radioButtonGainMuscle.setChecked(true);
                return;
            case R.id.constraintLayoutloseFat:
                resetRadioButtons();
                radioButtonLoseFat.setChecked(true);
                return;
            case R.id.ConstraintLayoutBoth:
                resetRadioButtons();
                radioButtonBoth.setChecked(true);
                return;
            case R.id.ConstraintLayoutGeneralHealth:
                resetRadioButtons();
                radioButtonOverallHealth.setChecked(true);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.radioBottonGainMuscle:
                if (isChecked)
                    mGoalPosition = 0;
                checkData();
                return;
            case R.id.radioButtonLoseFat:
                if (isChecked)
                    mGoalPosition = 1;
                checkData();
                return;
            case R.id.radioButtonBoth:
                if (isChecked)
                    mGoalPosition = 2;
                checkData();
                return;
            case R.id.radioButtonOverallHealth:
                if (isChecked)
                    mGoalPosition = 3;
                checkData();
        }
    }

    public void checkData() {
        if (mGoalPosition != -1)
            mActivity.setFabForwardClick(true);
        else
            mActivity.setFabForwardClick(false);
    }
}
