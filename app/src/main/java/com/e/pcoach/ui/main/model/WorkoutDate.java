package com.e.pcoach.ui.main.model;

public class WorkoutDate {

    private int day;
    private String dayName;
    private int month;
    private int year;
    private boolean isYearLeap;

    public boolean isYearLeap() {
        return isYearLeap;
    }

    public void setYearLeap(boolean yearLeap) {
        isYearLeap = yearLeap;
    }

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
