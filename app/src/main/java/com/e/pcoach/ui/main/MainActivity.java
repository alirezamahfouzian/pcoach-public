package com.e.pcoach.ui.main;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.ViewCompat;
import androidx.lifecycle.ViewModelProvider;

import com.e.pcoach.R;
import com.e.pcoach.db.entities.UserEntity;
import com.e.pcoach.di.BaseActivity;
import com.e.pcoach.ui.firsttime.UserViewModel;
import com.e.pcoach.ui.firsttime.FirstTimeActivity;
import com.e.pcoach.ui.helper.ActivityHelper;
import com.e.pcoach.ui.helper.DarkModePrefManager;
import com.e.pcoach.ui.helper.ThemeChangeHelper;
import com.e.pcoach.ui.main.adapter.bottomnavigation.NoSwipePager;
import com.e.pcoach.ui.main.adapter.bottomnavigation.PagerSaveStateHelpAdapter;
import com.e.pcoach.ui.main.home.HomeFragment;
import com.e.pcoach.ui.main.home.datepicker.BottomSheetFragment;
import com.e.pcoach.ui.main.model.WorkoutDate;
import com.e.pcoach.ui.main.programs.ProgramViewModel;
import com.e.pcoach.ui.main.programs.ProgramsFragment;
import com.e.pcoach.ui.main.splash.SplashFragment;
import com.e.pcoach.ui.main.tools.ToolsFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.MaybeObserver;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends BaseActivity implements
        BottomNavigationView.OnNavigationItemSelectedListener,
        BottomSheetFragment.DateSubmitListener {

    public static final String INTRO_KEY = "INTRO_KEY";
    private static final String TAG = "MainActivityTag";
    public boolean isAnswer = false;
    public IsAnswer isAnswerListener;
    public boolean isMain = true;
    @Inject
    DarkModePrefManager sharedPref;
    @ColorInt
    public int offColor;
    @ColorInt
    public int onColor;
    @BindView(R.id.nav_view)
    BottomNavigationView navView;
    @BindView(R.id.viewPager)
    NoSwipePager viewPager;
    @BindView(R.id.container)
    FrameLayout container;
    @Inject
    ActivityHelper.Helper helper;
    @Inject
    SplashFragment splashFragment;
    @Inject
    HomeFragment homeFragment;
    ProgramsFragment programsFragment;
    @Inject
    ToolsFragment statsFragment;
    @Inject
    CompositeDisposable disposable;
    @Inject
    PagerSaveStateHelpAdapter pagerAdapter;
    @Inject
    ThemeChangeHelper themeChangeHelper;
    private Disposable mObservable;
    private ProgramViewModel mProgramViewModel;
    private UserViewModel mUserViewModel;
    private boolean userState;
    private UserEntity mUserEntity = null;
    private boolean isFromInter = false;
    private boolean doubleBackToExitPressedOnce = false;
    private Snackbar mSnack;
    private Typeface mSnackbarFont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            isFromInter = extras.getBoolean(INTRO_KEY);
        }
        mProgramViewModel = new ViewModelProvider(this).get(ProgramViewModel.class);
        mUserViewModel = new ViewModelProvider(this).get(UserViewModel.class);
        themeChangeHelper.setDarkMode(true);
        helper.setRotationPortrait(this);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        build();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void build() {
        offColor = ResourcesCompat.getColor(getResources(), R.color.gray_background, null);
        offColor = ResourcesCompat.getColor(getResources(), R.color.colorAccent, null);
        programsFragment = new ProgramsFragment();
        if (!isFromInter && !sharedPref.isThemeChanged()) {
            openSplashFragment();
            helper.setFullscreen(this, true);
        }
        sharedPref.setThemeChanged(false);
        createSnack();
        homeFragment.setViewModel(mProgramViewModel);
        programsFragment.setViewModel(mProgramViewModel);
        navView.setOnNavigationItemSelectedListener(this);
        checkAuth();
    }

    /**
     * todo : shows the splash bog
     * checks that if the use is Authenticated or
     * not if yes starts the homeFragment if no
     * it Opens the firstTime Activity
     */
    @SuppressLint("CheckResult")
    private void checkAuth() {
        // wait 3 sec then check if user is authed or not
        mUserViewModel
                .getUser()
                .subscribe(new MaybeObserver<UserEntity>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable.add(d);
                    }

                    @Override
                    public void onSuccess(UserEntity userEntity) {
                        if (userEntity != null) {
                            disposable.add(Observable.timer(1, TimeUnit.SECONDS)
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(new Consumer<Long>() {
                                        @Override
                                        public void accept(Long aLong) throws Exception {
                                            if (!disposable.isDisposed()) {
                                                helper.removeFragment(MainActivity.this, splashFragment);
                                                helper.setFullscreen(MainActivity.this, false);
                                                setUpBottomNavigation();
                                            }
                                        }
                                    }));
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        disposable.add(Observable.timer(1, TimeUnit.SECONDS)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new Consumer<Long>() {
                                    @Override
                                    public void accept(Long aLong) throws Exception {
                                        if (!disposable.isDisposed()) {
                                            openFirstTimeActivity();
                                        }
                                    }
                                }));
                    }
                });
    }

    /**
     * sets up bottom navigation and
     * adds the fragments into it
     */
    private void setUpBottomNavigation() {
        setNavViewVisibility(true);
        //optimisation
        viewPager.setOffscreenPageLimit(2);
        viewPager.setPagingEnabled(false);
        pagerAdapter.addFragments(homeFragment);
        pagerAdapter.addFragments(programsFragment);
        pagerAdapter.addFragments(statsFragment);
        viewPager.setAdapter(pagerAdapter);
    }

    public void setNavViewVisibility(boolean isVisible) {
        if (isVisible) {
            isMain = true;
            navView.setVisibility(View.VISIBLE);
            return;
        }
        navView.setVisibility(View.GONE);
    }

    public void setViewPagerVisibility(boolean isVisible) {
        if (isVisible) {
            viewPager.setVisibility(View.VISIBLE);
            return;
        }
        viewPager.setVisibility(View.GONE);
    }

    private void openSplashFragment() {
        helper.transitToFragment(this, splashFragment,
                false);
    }

    private void openFirstTimeActivity() {
        // login sign up and intro slider
        Intent intent = new Intent(this, FirstTimeActivity.class);
        // for setting the activity change animation
        Bundle bundle = ActivityOptionsCompat.makeCustomAnimation(this,
                android.R.anim.fade_in, android.R.anim.fade_out).toBundle();
        startActivity(intent, bundle);
        finish();
    }

    /**
     * it disposes the Rxjava object
     * {@link #mObservable} in onStop
     * of SplashFragment
     */
    public void disposeObservable() {
        disposable.dispose();
    }

    private void createSnack() {
        mSnackbarFont = ResourcesCompat.getFont(this, R.font.iransans_medium);
        mSnack = Snackbar.make(findViewById(android.R.id.content), "برای خروج دوباره دکمه بازگشت رو دوباره فشار بدید", 1500);
        View snackView = mSnack.getView();
        // setting the rtl direction
        ViewCompat.setLayoutDirection(snackView, ViewCompat.LAYOUT_DIRECTION_RTL);
        // setting font and color to the action and text
        TextView text = snackView.findViewById(com.google.android.material.R.id.snackbar_text);
        text.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        text.setTypeface(mSnackbarFont);
        text.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.get_info_snackbar_text_size));
        mSnack.setBackgroundTint(getResources().getColor(R.color.red_text));
        mSnack.setActionTextColor(getResources().getColor(R.color.white));
        mSnack.setTextColor(getResources().getColor(R.color.white));
    }

    public void restartApp() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        if (isAnswer) {
            isAnswerListener.setIsAnswer(false);
            return;
        }

        if (!isMain) {
            super.onBackPressed();
            return;
        }

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            setViewPagerVisibility(true);
            setNavViewVisibility(true);
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        mSnack.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    /**
     * changes the bottomNavigation view selected
     * fragment when user changes the selected one
     */
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navigation_home:
                viewPager.setCurrentItem(0, false);
                return true;
            case R.id.navigation_program:
                viewPager.setCurrentItem(1, false);
                return true;
            case R.id.navigation_stats:
                viewPager.setCurrentItem(2, false);
                return true;

        }
        return false;
    }

    /**
     * its from the {@link BottomSheetFragment} and passes
     * the selected date from the date picker and listener
     * sends that value to {@link HomeFragment} to set into
     * the date picker
     */
    @Override
    public void setOnSubmitListener(WorkoutDate date) {
        // calender submit button tochlistener
        if (date != null) {
            homeFragment.dismissDialog(date);
        }
    }

    public interface IsAnswer {
        void setIsAnswer(boolean isAnswer);
    }
}

