package com.e.pcoach.ui.helper

enum class MealAmountType {
    KG, G, DAST, GHASHOGH, LIVAN, ADAD
}
