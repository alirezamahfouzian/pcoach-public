package com.e.pcoach.ui.main.programs;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityOptionsCompat;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleEventObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.e.pcoach.R;
import com.e.pcoach.db.entities.ProgramsEntity;
import com.e.pcoach.ui.getinfo.GetInformationActivity;
import com.e.pcoach.ui.helper.ActivityHelper;
import com.e.pcoach.ui.helper.UserState;
import com.e.pcoach.ui.main.MainActivity;
import com.e.pcoach.ui.main.adapter.bottomnavigation.NoSwipePager;
import com.e.pcoach.ui.main.adapter.bottomnavigation.PagerSaveStateHelpAdapter;
import com.e.pcoach.ui.main.adapter.customtabchanger.CustomTabChanger;
import com.e.pcoach.ui.main.history.HistoryFragment;
import com.e.pcoach.ui.main.programs.meal.ProgramMealFragment;
import com.e.pcoach.ui.main.programs.workout.ProgramWorkoutFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;
import io.reactivex.MaybeObserver;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class ProgramsFragment extends DaggerFragment
        implements CustomTabChanger.MealTabChangeListener {

    private static final String TAG = "ProgramsFragment";
    @Inject
    PagerSaveStateHelpAdapter pagerAdapter;
    @Inject
    ProgramWorkoutFragment programWorkoutFragment;

    ProgramMealFragment programMealFragment;
    @Inject
    ActivityHelper.Helper helper;
    @Inject
    CustomTabChanger customTabChanger;
    @Inject
    CompositeDisposable disposable;
    @BindView(R.id.programsViewPager)
    NoSwipePager programsViewPager;
    @BindView(R.id.imageViewHistory)
    ImageView imageViewHistory;
    @BindView(R.id.textViewProgramName)
    TextView textViewProgramName;
    @BindView(R.id.textViewTitle)
    TextView textViewTitle;
    @BindView(R.id.linearLayoutDisableProgram)
    LinearLayout linearLayoutDisableProgram;
    @BindView(R.id.fabGetProgram)
    FloatingActionButton fabGetProgram;

    private MainActivity mActivity;
    private ProgramViewModel mProgramViewModel;
    private View mView;
    private UserState mUserState;
    private boolean isFirstTime = true;


    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible && isResumed() && isFirstTime) {
            isFirstTime = false;
            build();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: ");
        mView = inflater.inflate(R.layout.fragment_program, container, false);
        ButterKnife.bind(this, mView);
        mActivity = (MainActivity) getActivity();
        return mView;
    }

    public void setViewModel(ProgramViewModel programViewModel) {
        mProgramViewModel = programViewModel;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void build() {
        programMealFragment = new ProgramMealFragment();
        if (mProgramViewModel == null) {
            mProgramViewModel = new ViewModelProvider(this).get(ProgramViewModel.class);
        }
        programMealFragment.setViewModel(mProgramViewModel);
        programWorkoutFragment.setViewModel(mProgramViewModel);
        setUpProgramTabChanger();
        setUpHistoryClick();
        setUpProgramTab();
        setUpButtonGetProgram();
        checkUserState();
    }

    private void checkUserState() {
        mProgramViewModel.getEnabledPrograms().observe(getViewLifecycleOwner(), new Observer<ProgramsEntity>() {
            @Override
            public void onChanged(ProgramsEntity programsEntity) {
                if (programsEntity != null) {
                    textViewProgramName.setText(programsEntity.getProgramTitle());
                    mUserState = UserState.ENABLED_PROGRAM;
                    setState(mUserState);
                } else {
                    mProgramViewModel.getAllPrograms().subscribe(new MaybeObserver<List<ProgramsEntity>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            disposable.add(d);
                        }

                        @Override
                        public void onSuccess(List<ProgramsEntity> programsEntities) {
                            try {
                                if (!disposable.isDisposed()) {
                                    // todo: should be NO_PROGRAM
                                    if (programsEntities.size() == 0) {
                                        Log.d(TAG, "onSuccess: no ");
                                    } else {
                                        Log.d(TAG, "onSuccess: ");
                                    }
                                    mUserState = UserState.DISABLED_PROGRAM;
                                    setState(mUserState);
                                }
                            } catch (Exception e) {
                            }
                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onComplete() {
                            Log.d(TAG, "onComplete:");
                        }
                    });
                }
            }
        });
    }

    private void setUpButtonGetProgram() {
        fabGetProgram.setOnClickListener(v -> {
            Intent intent = new Intent(mActivity, GetInformationActivity.class);
            Bundle bundle = ActivityOptionsCompat.makeCustomAnimation(mActivity,
                    android.R.anim.fade_in, android.R.anim.fade_out).toBundle();
            mActivity.startActivity(intent, bundle);
        });
    }

    private void setUpHistoryClick() {
        imageViewHistory.setOnClickListener(v -> {
            mActivity.isMain = false;
            helper.transitToFragment(mActivity, new HistoryFragment(), true);
        });
    }

    private void setUpProgramTab() {
        //optimisation
        programsViewPager.setOffscreenPageLimit(1);
        pagerAdapter.addFragments(programWorkoutFragment);
        pagerAdapter.addFragments(programMealFragment);
        programsViewPager.setPagingEnabled(false);
        programsViewPager.setAdapter(pagerAdapter);
    }

    /**
     * sets up ProgramTabChanger
     */
    private void setUpProgramTabChanger() {
        String[] titles = new String[]{"تمرینی", "غذایی"};
        customTabChanger.build(mActivity, mView, this, titles);
    }

    private void setState(UserState userState) {
        if (userState == UserState.DISABLED_PROGRAM) {
            fabGetProgram.setVisibility(View.VISIBLE);
            linearLayoutDisableProgram.setVisibility(View.VISIBLE);
            customTabChanger.setTabChangerSelectorColor(mActivity, true);
            imageViewHistory.setEnabled(false);
            textViewProgramName.setVisibility(View.GONE);
        } else {
            linearLayoutDisableProgram.setVisibility(View.GONE);
            customTabChanger.setTabChangerSelectorColor(mActivity, false);
            imageViewHistory.setEnabled(true);
            textViewProgramName.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onTabChangeListener(int position) {
        programsViewPager.setCurrentItem(position, false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        disposable.clear();
    }
}