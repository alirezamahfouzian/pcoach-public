package com.e.pcoach.ui.main.adapter.customtabchanger;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.core.content.ContextCompat;

import com.e.pcoach.R;

public class CustomTabChanger implements View.OnClickListener {

    private View mView;
    private Activity mContext;
    private int mGrayColor;
    private int mWhiteColor;
    private TextView[] mTextViews;
    private String[] mTitles;
    private Drawable mDrawableOnStyle;
    private MealTabChangeListener mListener;

    public void build(Activity activity, View view, MealTabChangeListener listener, String[] titles) {
        mContext = activity;
        mTitles = titles;
        mView = view;
        mListener = listener;
        TypedValue typedValueGray = new TypedValue();
        TypedValue typedValueWhite = new TypedValue();
        Resources.Theme theme = activity.getTheme();
        theme.resolveAttribute(R.attr.tab_changer_gray_text, typedValueGray, true);
        @ColorInt int colorGray = typedValueGray.data;
        theme.resolveAttribute(R.attr.white, typedValueWhite, true);
        @ColorInt int colorWhite = typedValueWhite.data;
        mGrayColor = colorGray;
        mWhiteColor = colorWhite;
        mDrawableOnStyle = ContextCompat
                .getDrawable(mContext, R.drawable.tab_changer_bg_selected);
        mTextViews = new TextView[mTitles.length];
        setTextViews();
    }

    public void setTabChangerSelectorColor(Context context, boolean isOff) {
        if (isOff) {
            mDrawableOnStyle = ContextCompat
                    .getDrawable(context, R.drawable.tab_changer_bg_selected_off);
            selectTab(0);
            return;
        }
        mDrawableOnStyle = ContextCompat
                .getDrawable(context, R.drawable.tab_changer_bg_selected);
        selectTab(0);
    }

    private void setTextViews() {
        int temp;
        for (int i = 0; i < mTitles.length; i++) {
            temp = mView.getResources()
                    .getIdentifier("textView" + i, "id", mContext.getPackageName());
            mTextViews[i] = mView.findViewById(temp);
            mTextViews[i].setText(mTitles[i]);
            mTextViews[i].setOnClickListener(this);
        }
    }

    private void resetViews() {
        for (TextView textViews : mTextViews) {
            textViews.setBackgroundResource(0);
            textViews.setTextColor(mGrayColor);
        }
    }

    private void selectView(int TextViewId) {
        mTextViews[TextViewId].setBackground(mDrawableOnStyle);
        mTextViews[TextViewId].setTextColor(mWhiteColor);
        mListener.onTabChangeListener(TextViewId);
    }

    public void selectTab(int position) {
        resetViews();
        selectView(position);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.textView0:
                selectTab(0);
                return;
            case R.id.textView1:
                selectTab(1);
                return;
            case R.id.textView2:
                selectTab(2);
                return;
            case R.id.textView3:
                selectTab(3);
                return;
        }
    }

    public interface MealTabChangeListener {
        void onTabChangeListener(int position);
    }

}
