package com.e.pcoach.ui.firsttime;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.e.pcoach.db.entities.UserEntity;
import com.e.pcoach.repository.UserRepository;

import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;

public class UserViewModel extends AndroidViewModel{

    UserRepository userRepository;

    public UserViewModel(@NonNull Application application) {
        super(application);
        userRepository = new UserRepository(application, new CompositeDisposable());
    }

    public void insertUser(UserEntity userEntity) {
        userRepository.insertUser(userEntity);
    }

    public void updateUser(UserEntity userEntity) {
        userRepository.updateUser(userEntity);
    }

    public Maybe<UserEntity> getUser() {
        return userRepository.getUser();
    }

}
