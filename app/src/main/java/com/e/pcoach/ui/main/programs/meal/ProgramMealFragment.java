package com.e.pcoach.ui.main.programs.meal;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.e.pcoach.ui.helper.ProgramType;
import com.e.pcoach.R;
import com.e.pcoach.db.entities.ProgramsEntity;
import com.e.pcoach.ui.helper.ActivityHelper;
import com.e.pcoach.ui.helper.DataSource;
import com.e.pcoach.ui.helper.MealTypeCaster;
import com.e.pcoach.ui.helper.UserState;
import com.e.pcoach.ui.main.MainActivity;
import com.e.pcoach.ui.main.model.MealModel;
import com.e.pcoach.ui.main.programs.ProgramViewModel;
import com.e.pcoach.ui.main.programs.adapter.MealCustomView;
import com.e.pcoach.ui.main.programs.adapter.ProgramDayRec;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;
import io.reactivex.MaybeObserver;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

@SuppressLint("CheckResult")
public class ProgramMealFragment extends DaggerFragment implements ProgramDayRec.DietDayChangeListener {

    private static final String TAG = "ProgramMealFragment";
    @Inject
    ProgramDayRec programDayRec;
    @Inject
    ActivityHelper.Helper helper;
    @Inject
    MealTypeCaster caster;
    @Inject
    CompositeDisposable disposable;

    @BindView(R.id.recyclerViewMealProgramDate)
    RecyclerView recyclerViewMealProgramDate;
    @BindView(R.id.snackCustomView)
    MealCustomView snackCustomView;
    @BindView(R.id.dinnerCustomView)
    MealCustomView dinnerCustomView;
    @BindView(R.id.lunchCustomView)
    MealCustomView lunchCustomView;
    @BindView(R.id.breakFastCustomView)
    MealCustomView breakFastCustomView;
    @BindView(R.id.textViewLunchTitle)
    TextView textViewLunchTitle;
    @BindView(R.id.textViewDinnerTitle)
    TextView textViewDinnerTitle;
    @BindView(R.id.textViewBreakFastTitle)
    TextView textViewBreakFastTitle;
    @BindView(R.id.textViewSnackTitle)
    TextView textViewSnackTitle;

    // get in use days
    private List<Integer> mInUsedDays;
    private int lastDay = -1;
    private int indexCount = 0;
    private MainActivity mActivity;
    private View mViewNoProgram;
    private View mView;
    @ColorInt
    private int offColor;
    @ColorInt
    private int onColor;
    private ProgramViewModel mProgramViewModel;
    private List<MealModel> mealBreakFastList = new ArrayList<>();
    private List<MealModel> mealLunchList = new ArrayList<>();
    private List<MealModel> mealDinnerList = new ArrayList<>();
    private List<MealModel> mealSnackList = new ArrayList<>();
    private int checkDietChange = 0;
    private UserState mUserState;

    @SuppressLint("ResourceType")
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
//      dashboardViewModel =
//              ViewModelProviders.of(this).get(ProgramsViewModel.class);
        mActivity = (MainActivity) getActivity();
        mView = inflater.inflate(R.layout.fragment_program_meal, container, false);
        ButterKnife.bind(this, mView);
        Log.d(TAG, "onCreateView: ");
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        offColor = mActivity.getResources().getColor(R.color.gray_program_day);
        onColor = mActivity.getResources().getColor(R.color.colorAccent);
        build();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        disposable.clear();
    }

    public void setViewModel(ProgramViewModel programViewModel) {
        mProgramViewModel = programViewModel;
    }

    private void build() {
        if (mProgramViewModel == null) {
            mProgramViewModel = new ViewModelProvider(this).get(ProgramViewModel.class);
        }
        setUpMealTypeViews();
        setUpDayRecyclerView();
        checkUserState();
    }

    private void setUpDay() {
        mInUsedDays = new ArrayList<>();
        mProgramViewModel.getEnabledPrograms().observe(getViewLifecycleOwner(), new Observer<ProgramsEntity>() {
            @Override
            public void onChanged(ProgramsEntity programsEntity) {
                mInUsedDays.clear();
                Observable
                        .create(new ObservableOnSubscribe<Object>() {
                            @Override
                            public void subscribe(ObservableEmitter<Object> emitter) throws Exception {
                                if (!disposable.isDisposed()) {
                                    char[] chars = programsEntity.getProgramDietDays().toCharArray();
                                    int i = 0 ;
                                    for (char aChar : chars) {
                                        mInUsedDays.add(Integer.valueOf(String.valueOf(aChar)));
                                    }
                                    emitter.onComplete();
                                }
                            }
                        })
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new io.reactivex.Observer<Object>() {
                            @Override
                            public void onSubscribe(Disposable d) {
                                disposable.add(d);
                            }

                            @Override
                            public void onNext(Object o) {

                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onComplete() {
                                programDayRec.setData(mInUsedDays);
                            }
                        });
            }
        });
    }

    private void setUpMealTypeViews() {
        breakFastCustomView.build(null, helper);
        lunchCustomView.build(null, helper);
        dinnerCustomView.build(null, helper);
        snackCustomView.build(null, helper);
    }

    private void setUpMeals(int dayPosition) {
        if (mUserState == UserState.ENABLED_PROGRAM) {
            mProgramViewModel.getDayMeals(dayPosition).observe(this, new Observer<List<MealModel>>() {
                @Override
                public void onChanged(List<MealModel> mealModels) {
                    try {
                        mealBreakFastList.clear();
                        mealLunchList.clear();
                        mealDinnerList.clear();
                        mealSnackList.clear();
                        Observable.fromIterable(mealModels)
                                .map(new Function<MealModel, Boolean>() {
                                    @Override
                                    public Boolean apply(MealModel mealModel) throws Exception {
                                        if (!disposable.isDisposed()) {
                                            switch (mealModel.getMealType()) {
                                                case "0":
                                                    mealBreakFastList.add(mealModel);
                                                    break;
                                                case "1":
                                                    mealLunchList.add(mealModel);
                                                    break;
                                                case "2":
                                                    mealDinnerList.add(mealModel);
                                                    break;
                                                case "3":
                                                    mealSnackList.add(mealModel);
                                                    break;
                                            }
                                        }
                                        return true;
                                    }
                                })
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new io.reactivex.Observer<Boolean>() {
                                    @Override
                                    public void onSubscribe(Disposable d) {
                                        disposable.add(d);
                                    }

                                    @Override
                                    public void onNext(Boolean b) {

                                    }

                                    @Override
                                    public void onError(Throwable e) {

                                    }

                                    @Override
                                    public void onComplete() {
                                        breakFastCustomView.updateData(mealBreakFastList);
                                        lunchCustomView.updateData(mealLunchList);
                                        dinnerCustomView.updateData(mealDinnerList);
                                        snackCustomView.updateData(mealSnackList);
                                    }
                                });
                    } catch (Exception e) {
                        throw e;
                    }
                }
            });
        } else {
            breakFastCustomView.updateData(DataSource.getMealByType(dayPosition, "0"));
            lunchCustomView.updateData(DataSource.getMealByType(dayPosition, "1"));
            dinnerCustomView.updateData(DataSource.getMealByType(dayPosition, "2"));
            snackCustomView.updateData(DataSource.getMealByType(dayPosition, "3"));

        }
    }

    private void checkUserState() {
        mProgramViewModel.getEnabledPrograms().observe(getViewLifecycleOwner(), new Observer<ProgramsEntity>() {
            @Override
            public void onChanged(ProgramsEntity programsEntity) {
                if (programsEntity != null) {
                    setState(UserState.ENABLED_PROGRAM);
                    setUpDay();
                } else {
                    mProgramViewModel.getAllProgramsLimited().subscribe(new MaybeObserver<List<ProgramsEntity>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            disposable.add(d);
                        }

                        @Override
                        public void onSuccess(List<ProgramsEntity> programsEntities) {
                            try {
                                if (programsEntities.size() == 0) {
                                    setUpDisabledDay();
                                    setState(UserState.DISABLED_PROGRAM);
                                } else {
                                    setUpDisabledDay();
                                    setState(UserState.DISABLED_PROGRAM);
                                }
                            } catch (Exception e) {
                            }
                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onComplete() {
                        }
                    });
                }
            }
        });
    }

    private void setUpDisabledDay() {
        programDayRec.setData(DataSource.getDietSelectedDays());
    }

    public void setState(UserState userState) {
        if (userState == UserState.DISABLED_PROGRAM) {
            textViewBreakFastTitle.setTextColor(offColor);
            textViewDinnerTitle.setTextColor(offColor);
            textViewSnackTitle.setTextColor(offColor);
            textViewLunchTitle.setTextColor(offColor);
        } else {
            textViewBreakFastTitle.setTextColor(onColor);
            textViewDinnerTitle.setTextColor(onColor);
            textViewSnackTitle.setTextColor(onColor);
            textViewLunchTitle.setTextColor(onColor);
        }
    }

    private void setUpDayRecyclerView() {
        programDayRec.build(ProgramType.MEAL, this);
        helper.setUpRecyclerView(mActivity, recyclerViewMealProgramDate, programDayRec,
                true);
        recyclerViewMealProgramDate.smoothScrollToPosition(0);
    }

    @Override
    public void onDietDayChangeListener(int position) {
        setUpMeals(position);
    }
}