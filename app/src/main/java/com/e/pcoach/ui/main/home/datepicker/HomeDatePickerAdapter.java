package com.e.pcoach.ui.main.home.datepicker;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.e.pcoach.R;
import com.e.pcoach.ui.helper.TabChangeListener;
import com.e.pcoach.ui.main.model.WorkoutDaysModel;

public class HomeDatePickerAdapter implements View.OnClickListener {

    private static final String TAG = "DatePickerAdapter";
    private AppCompatActivity mActivity;
    private View mView;
    private int mBlackColor;
    private int mWhiteColor;
    private Drawable mDrawableOffStyle;
    private Drawable mDrawableOnStyle;
    private TabChangeListener mListener;

    private String[] mTextViewIds = new String[]{"firstDayName", "firstDayDate", "secondDayName",
            "secondDayDate", "thirdDayName", "thirdDayDate", "fourthDayName", "fourthDayDate",
            "fifthDayName", "fifthDayDate"
    };

    private String[] mLinearLayoutIds = new String[]{"firstDaySelector", "secondDaySelector",
            "thirdDaySelector", "fourthDaySelector",
            "fifthDaySelector"
    };

    private TextView[] mTextViews = new TextView[10];
    private LinearLayout[] mLinearLayouts = new LinearLayout[5];


    public HomeDatePickerAdapter(AppCompatActivity activity, View view, TabChangeListener listener) {
        mActivity = activity;
        mView = view;
        mListener = listener;
        TypedValue typedValueGray = new TypedValue();
        TypedValue typedValueWhite = new TypedValue();
        Resources.Theme theme = activity.getTheme();
        theme.resolveAttribute(R.attr.homeDatePickerOff, typedValueGray, true);
        @ColorInt int colorGray = typedValueGray.data;
        theme.resolveAttribute(R.attr.white, typedValueWhite, true);
        @ColorInt int colorWhite = typedValueWhite.data;
        mBlackColor = colorGray;
        mWhiteColor = colorWhite;
        mDrawableOffStyle = ContextCompat
                .getDrawable(mActivity, R.drawable.date_changer_off_style);
        mDrawableOnStyle = ContextCompat
                .getDrawable(mActivity, R.drawable.date_changer_style);
        setTextViews();
        setLinearLayouts();
    }

    /**
     * initialize all TextViews and puts it
     * in a {@link #mTextViews}
     */
    private void setTextViews() {
        int temp;
        for (int i = 0; i < mTextViewIds.length; i++) {
            temp = mActivity.getResources().getIdentifier(mTextViewIds[i], "id", mActivity.getPackageName());
            mTextViews[i] = mView.findViewById(temp);
        }
    }

    /**
     * there is two text views in the mTextViews and
     * the even ones are the name and
     * odd ones are the date
     * @param model has given days and dates to set in date picker
     */
    public void setData(WorkoutDaysModel model) {
        int e = 0;
        int o = 0;
        for (int i = 0; i < mTextViewIds.length; i++) {
            if (i % 2 == 0) {
                mTextViews[i].setText(String.valueOf(model.getDayName()[e]));
                e++;
            } else {
                mTextViews[i].setText(String.valueOf(model.getDate()[o]));
                o++;
            }
        }
    }

    /**
     * gets linear layouts from resources and adds it
     * to an array so we use them in other places
     */
    private void setLinearLayouts() {
        int temp;
        for (int i = 0; i < mLinearLayoutIds.length; i++) {
            temp = mActivity.getResources().getIdentifier(mLinearLayoutIds[i], "id", mActivity.getPackageName());
            mLinearLayouts[i] = mView.findViewById(temp);
            mLinearLayouts[i].setOnClickListener(this);
        }
    }

    /**
     * resets all view to no
     * background and black text color
     */
    private void resetViews() {
        for (LinearLayout mLinearLayout : mLinearLayouts) {
            mLinearLayout.setBackground(mDrawableOffStyle);
        }
        for (TextView mTextView : mTextViews) {
            mTextView.setTextColor(mBlackColor);
        }
    }

    public void selectView(int LinearLayoutPosition, int TextViewName, int TextViewDate) {
        mLinearLayouts[LinearLayoutPosition].setBackground(mDrawableOnStyle);
        mTextViews[TextViewName].setTextColor(mWhiteColor);
        mTextViews[TextViewDate].setTextColor(mWhiteColor);
        mListener.onDateChangeListener(LinearLayoutPosition);
    }

    /**
     * it needs this so every time it sets new
     * date to datePicker it selects the first
     * element of the date picker
     */
    public void setIndicatorToMiddle() {
        resetViews();
        selectView(2, 4, 5);
    }

    /**
     * it adds the element changeEffect in the
     * homeFragment datePicker
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.firstDaySelector:
                resetViews();
                selectView(0, 0, 1);
                return;
            case R.id.secondDaySelector:
                resetViews();
                selectView(1, 2, 3);
                return;
            case R.id.thirdDaySelector:
                resetViews();
                selectView(2, 4, 5);
                return;
            case R.id.fourthDaySelector:
                resetViews();
                selectView(3, 6, 7);
                return;
            case R.id.fifthDaySelector:
                resetViews();
                selectView(4, 8, 9);
        }
    }
}

