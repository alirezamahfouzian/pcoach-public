package com.e.pcoach.ui.main.home.mealviewpager;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;

import com.e.pcoach.R;
import com.e.pcoach.ui.helper.ActivityHelper;
import com.e.pcoach.ui.helper.MealTypeCaster;
import com.e.pcoach.ui.main.model.MealModel;

import java.util.List;


public class HomeMealPagerAdapter extends PagerAdapter {

    private static final String TAG = "HomeMealPagerAdapter";
    private MealRecAdapter mHomeMealRecAdapter;
    private List<MealModel> mMealModelList;
    private ActivityHelper.Helper mHelper;
    private LayoutInflater inflater;
    private Context mContext;
    private RecyclerView mRecyclerView;
    private View mView;
    private boolean mIsFirstTime = true;
    private MealTypeCaster mCaster;
    private int index = 0;

    public HomeMealPagerAdapter(Context context, List<MealModel> mealModel,
                                ActivityHelper.Helper helper, MealTypeCaster caster) {
        mContext = context;
        inflater = LayoutInflater.from(context);
        mMealModelList = mealModel;
        mHelper = helper;
        mCaster = caster;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return 4;
    }

    /**
     * TODO: make this new object making
     * TODO: of mealRecAdapter get pass by constructor
     * @param view
     * @param position
     * @return
     */
    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        mView = LayoutInflater.from(mContext).inflate(R.layout.row_cat_tab_slider, null);
        Log.d(TAG, "instantiateItem: " + position);
        if (index <= position) {
            Log.d(TAG, "instantiateItem: " + position + "   " + index);
            mHomeMealRecAdapter = new MealRecAdapter(mContext, mMealModelList);
            mRecyclerView = mView.findViewById(R.id.recyclerViewMealPager);
            mRecyclerView.setRotationY(180);
            mRecyclerView.setNestedScrollingEnabled(false);
            mHelper.setUpRecyclerView(mContext, mRecyclerView, mHomeMealRecAdapter, false);
            view.addView(mView, 0);
        }
        index++;
        return mView;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

}