package com.e.pcoach.ui.main.programs.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.e.pcoach.ui.helper.ProgramType;
import com.e.pcoach.R;

import java.util.List;

import javax.inject.Inject;

public class ProgramDayRec extends RecyclerView.Adapter<ProgramDayRec.ViewHolder> {


    private static final String TAG = "HomeWorkoutRecAdapter";
    private final int mGrayColor;
    private final int mBlackColor;
    private final Drawable mDrawableSelected;
    private String[] mWorkoutDayList = new String[]{"شنبه", "یک‌شنبه", "دوشنبه",
            "سه‌شنبه", "چهارشنبه", "پنج‌شنبه", "جمعه"};

    private String[] mMealDayList = new String[]{"روز‌اول", "روزدوم", "روز‌سوم",
            "روزچهارم", "روزپنج", "روزششم", "روزهفتم"};

    private TextView[] mTextViews;

    private Context mContext;
    private ProgramType mProgramType;
    private List<Integer> mDayPositionList;
    private String[] mDaysInUse = new String[]{};
    private int mRowIndex = -1;
    private boolean mIsTheFirstTime = true;
    private WorkoutDayChangeListener mWorkoutListener;
    private DietDayChangeListener mMealListener;
    private long mLastClickTime = 0;

    @Inject
    public ProgramDayRec(Context context) {
        this.mContext = context;

        TypedValue typedValueGray = new TypedValue();
        TypedValue typedValueWhite = new TypedValue();
        Resources.Theme theme = context.getTheme();
        theme.resolveAttribute(R.attr.gray_program_day, typedValueGray, true);
        @ColorInt int colorGray = typedValueGray.data;
        theme.resolveAttribute(R.attr.programDayOn, typedValueWhite, true);
        @ColorInt int colorBlack = typedValueWhite.data;

        mGrayColor = colorGray;
        mBlackColor = colorBlack;
        mDrawableSelected = ContextCompat
                .getDrawable(mContext, R.drawable.tab_changer_bg_selected);
    }

    public void build(ProgramType programType, WorkoutDayChangeListener workoutListener) {
        mProgramType = programType;
        mWorkoutListener = workoutListener;
    }

    public void build(ProgramType programType, DietDayChangeListener mealListener) {
        mProgramType = programType;
        mMealListener = mealListener;
    }

    public void setData(List<Integer> dayPositionList) {
        mDayPositionList = dayPositionList;
        mIsTheFirstTime = true;
        mRowIndex = 0;
        getDaysName();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext)
                .inflate(R.layout.row_workout_day_rec, parent, false));
    }

    /**
     * if the position be grater than the day count it wont input that item
     * also checks the program type and after setting the texts based on
     * that it will add the change effect
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (mDaysInUse == null || mDayPositionList.size() == 0) return;
        if (position > 6) return;
        if (mProgramType == ProgramType.WORKOUT) {
            if (position > mDayPositionList.size()) return;
            holder.mTextViewProgramDate.setText(mDaysInUse[position]);
        } else {
            if (position > mDayPositionList.size()) return;
            holder.mTextViewProgramDate.setText(mDaysInUse[position]);
        }

        // clicking effect
        if (mRowIndex == position) {
            holder.mTextViewProgramDate.setTextColor(mBlackColor);
            if (mProgramType == ProgramType.WORKOUT) {
                mWorkoutListener.onWorkoutDayChangeListener(mDayPositionList.get(position));
            } else {
                mMealListener.onDietDayChangeListener(mDayPositionList.get(position));
            }
        } else {
            holder.mTextViewProgramDate.setTextColor(mGrayColor);
        }

        holder.itemView.setOnClickListener(view -> {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 250) {
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();
            mRowIndex = position;
            notifyDataSetChanged();
        });
    }

    @Override
    public int getItemCount() {
        return mDaysInUse.length;
    }

    private void getDaysName() {
        mDaysInUse = new String[mDayPositionList.size()];
        if (mProgramType == ProgramType.WORKOUT) {
            for (int i = 0; i < mDayPositionList.size(); i++) {
                mDaysInUse[i] = mWorkoutDayList[mDayPositionList.get(i)];
            }
        } else {
            for (int i = 0; i < mDayPositionList.size(); i++) {
                mDaysInUse[i] = mMealDayList[mDayPositionList.get(i)];
            }
        }
        notifyDataSetChanged();
    }

    public interface WorkoutDayChangeListener {
        void onWorkoutDayChangeListener(int position);
    }

    public interface DietDayChangeListener {
        void onDietDayChangeListener(int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView mTextViewProgramDate;
        LinearLayout mLinearLayoutDay;

        private ViewHolder(@NonNull View itemView) {
            super(itemView);
            init();
        }

        private void init() {
            mTextViewProgramDate = itemView.findViewById(R.id.textViewProgramDate);
            mLinearLayoutDay = itemView.findViewById(R.id.linearLayoutDay);
        }
    }
}
