package com.e.pcoach.ui.helper;

import com.e.pcoach.ui.main.home.mealviewpager.HomeMealPagerAdapter;

import javax.inject.Inject;

public class MealTypeCaster {

    @Inject
    public MealTypeCaster() {
    }

    /**
     * casts {@link MealAmountType} to an string
     * so we can show it into app ui
     *
     * @param amountType
     * @return
     */
    public String cast(MealAmountType amountType) {
        switch (amountType) {
            case G:
                return "گرم";
            case KG:
                return "کیلو گرم";
            case DAST:
                return "کف دست";
            case LIVAN:
                return "لیوان";
            case GHASHOGH:
                return "قاشق";
            case ADAD:
                return "عدد";
            default:
                return null;
        }
    }

    /**
     * it get used in {@link HomeMealPagerAdapter}
     * @param position
     * @return
     */
    public MealType getMealTypeByPosition(int position) {
        switch (position) {
            case 0:
                return MealType.BREAKFAST;
            case 1:
                return MealType.LUNCH;
            case 2:
                return MealType.DINNER;
            case 3:
                return MealType.SNACK;
            default:
                return MealType.None;
        }
    }
}
