package com.e.pcoach.ui.main.history.details;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.e.pcoach.ui.helper.ProgramType;
import com.e.pcoach.R;
import com.e.pcoach.db.entities.ProgramsEntity;
import com.e.pcoach.ui.helper.ActivityHelper;
import com.e.pcoach.ui.main.model.WorkoutModel;
import com.e.pcoach.ui.main.programs.ProgramViewModel;
import com.e.pcoach.ui.main.programs.adapter.ProgramDayRec;
import com.e.pcoach.ui.main.programs.adapter.WorkoutRecAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class WorkoutDetailsFragment extends DaggerFragment implements ProgramDayRec.WorkoutDayChangeListener {

    @Inject
    ProgramDayRec programDayRec;
    @Inject
    ActivityHelper.Helper helper;
    @Inject
    WorkoutRecAdapter workoutRecAdapter;
    @Inject
    CompositeDisposable disposable;


    @BindView(R.id.recyclerViewWorkoutDetailsDate)
    RecyclerView recyclerViewWorkoutProgramDate;
    @BindView(R.id.recyclerViewWorkoutDetails)
    RecyclerView recyclerViewWorkoutProgram;

    // get in use days
    private List<Integer> mInUsedDays;
    private int lastDay = -1;
    private int indexCount = 0;
    private Activity mActivity;
    private View mView;
    private ProgramViewModel mProgramViewModel;
    private int mProgramId = 0;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        mActivity = getActivity();
        mView = inflater.inflate(R.layout.fragment_details_workout, container, false);
        ButterKnife.bind(this, mView);
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        build();
    }

    void setDependency(ProgramViewModel programViewModel, int programId) {
        mProgramViewModel = programViewModel;
        mProgramId = programId;
    }

    private void build() {
        if (mProgramViewModel == null) {
            mProgramViewModel = new ViewModelProvider(this).get(ProgramViewModel.class);
        }
        setUpWorkout();
        setUpDayRecyclerView();
        setUpDay(ProgramType.WORKOUT);
    }

    private void setUpDay(ProgramType programType) {
        mInUsedDays = new ArrayList<>();
        mProgramViewModel.getEnabledPrograms().observe(getViewLifecycleOwner(), new Observer<ProgramsEntity>() {
            @Override
            public void onChanged(ProgramsEntity programsEntity) {
                Observable
                        .create(new ObservableOnSubscribe<Object>() {
                            @Override
                            public void subscribe(ObservableEmitter<Object> emitter) throws Exception {
                                if (!disposable.isDisposed()) {
                                    char[] chars = programsEntity.getProgramWorkoutDays().toCharArray();
                                    int i = 0 ;
                                    for (char aChar : chars) {
                                        mInUsedDays.add(Integer.valueOf(String.valueOf(aChar)));
                                    }
                                    emitter.onComplete();
                                }
                            }
                        })
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new io.reactivex.Observer<Object>() {
                            @Override
                            public void onSubscribe(Disposable d) {
                                disposable.add(d);
                            }

                            @Override
                            public void onNext(Object o) {

                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onComplete() {
                                programDayRec.setData(mInUsedDays);
                            }
                        });
            }
        });
    }

    private void setUpDayRecyclerView() {
        programDayRec.build(ProgramType.WORKOUT, this);
        helper.setUpRecyclerView(mActivity, recyclerViewWorkoutProgramDate, programDayRec,
                true);
        recyclerViewWorkoutProgram.smoothScrollToPosition(0);
    }

    private void setUpWorkout() {
        List<WorkoutModel> workoutModels = new ArrayList<>();
        workoutRecAdapter.setData(workoutModels);
        helper.setUpRecyclerView(mActivity, recyclerViewWorkoutProgram,
                workoutRecAdapter, false);
    }

    private void setUpMoves(int dayPosition) {
        mProgramViewModel.getDayByProgramId(dayPosition, mProgramId).observe(getViewLifecycleOwner(), new Observer<List<WorkoutModel>>() {
            @Override
            public void onChanged(List<WorkoutModel> workoutModels) {
                try {
                    workoutRecAdapter.updateData(workoutModels);
                } catch (Exception e) {}
            }
        });
    }

    @Override
    public void onWorkoutDayChangeListener(int position) {
        setUpMoves(position);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        disposable.clear();
    }
}