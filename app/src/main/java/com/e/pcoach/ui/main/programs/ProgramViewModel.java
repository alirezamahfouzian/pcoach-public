package com.e.pcoach.ui.main.programs;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.e.pcoach.db.entities.DietProgramEntity;
import com.e.pcoach.db.entities.ProgramsEntity;
import com.e.pcoach.db.entities.WorkoutProgramEntity;
import com.e.pcoach.repository.ProgramRepository;
import com.e.pcoach.ui.main.history.HistoryModel;
import com.e.pcoach.ui.main.model.MealModel;
import com.e.pcoach.ui.main.model.WorkoutModel;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class ProgramViewModel extends AndroidViewModel {

    private ProgramRepository programRepository;

    public ProgramViewModel(@NonNull Application application) {
        super(application);
        programRepository = new ProgramRepository(application, new CompositeDisposable());
    }

    public void insertProgram(ProgramsEntity programsEntity) {
        programRepository.insertProgram(programsEntity);
    }

    public void updateProgram(ProgramsEntity programsEntity) {
        programRepository.updateProgram(programsEntity);
    }

    public LiveData<ProgramsEntity> getEnabledPrograms() {
        return programRepository.getEnabledPrograms();
    }

    public Maybe<ProgramsEntity> getEnabledProgramsObservable() {
        return programRepository.getEnabledProgramsObservable();
    }

    public Maybe<ProgramsEntity> getProgramById(int programId) {
        return programRepository.getProgramById(programId);
    }

    public Maybe<Integer> getLastProgramId() {
        return programRepository.getLastProgramId();
    }

    public Maybe<List<ProgramsEntity>> getAllPrograms() {
        return programRepository.getAllPrograms();
    }

    public Maybe<List<ProgramsEntity>> getAllProgramsLimited() {
        return programRepository.getAllPrograms();
    }

    public LiveData<List<HistoryModel>> getHistoryPrograms() {
        return programRepository.getHistoryPrograms();
    }

    public void insertWorkoutProgram(WorkoutProgramEntity workoutProgramEntity) {
        programRepository.insertWorkoutProgram(workoutProgramEntity);
    }

    public Maybe<List<WorkoutProgramEntity>> getWorkoutProgram(int programId) {
        return programRepository.getWorkoutPrograms(programId);
    }

    public LiveData<List<WorkoutModel>> getDayMoves(int dayPosition) {
        return programRepository.getDayMoves(dayPosition);
    }

    public Maybe<List<WorkoutModel>> getDayMovesObservable(int dayPosition) {
        return programRepository.getDayMovesObservable(dayPosition);
    }

    public LiveData<List<WorkoutModel>> getDayByProgramId(int dayPosition, int programId) {
        return programRepository.getDayByProgramId(dayPosition, programId);
    }

    public Maybe<List<WorkoutProgramEntity>> getAllWorkoutPrograms() {
        return programRepository.getAllWorkoutPrograms();
    }

    public Completable insertDietProgram(DietProgramEntity dietProgramEntity) {
        return programRepository.insertDietProgram(dietProgramEntity);
    }

    public LiveData<List<MealModel>> getDayMeals(int dayPosition) {
        return programRepository.getDayMeals(dayPosition);
    }

    public LiveData<List<MealModel>> getDayMealsByType(int dayPosition, int mealType) {
        return programRepository.getDayMealsByType(dayPosition, mealType);
    }

    public LiveData<List<MealModel>> getDayMealsByTypeByProgramId(int dayPosition, int programId) {
        return programRepository.getDayMealsByTypeByProgramId(dayPosition, programId);
    }

    public Maybe<List<DietProgramEntity>> getDietPrograms(int programId) {
        return programRepository.getDietPrograms(programId);
    }

    public Maybe<List<DietProgramEntity>> getAllDietPrograms() {
        return programRepository.getAllDietPrograms();
    }

    public LiveData<List<Integer>> getDietProgramDays() {
        return programRepository.getDietProgramDays();
    }

    public LiveData<List<Integer>> getWorkoutProgramDays() {
        return programRepository.getWorkoutProgramDays();
    }

    public LiveData<List<Integer>> getWorkoutProgramDaysById(int programId) {
        return programRepository.getWorkoutProgramDaysById(programId);
    }
}