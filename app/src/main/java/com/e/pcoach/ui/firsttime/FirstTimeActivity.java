package com.e.pcoach.ui.firsttime;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.viewpager.widget.ViewPager;

import com.e.pcoach.R;
import com.e.pcoach.di.BaseActivity;
import com.e.pcoach.ui.helper.ActivityHelper;
import com.e.pcoach.ui.helper.FragmentListener;
import com.e.pcoach.ui.firsttime.intro.MyPagerAdapter;
import com.e.pcoach.ui.firsttime.login.LoginFragment;
import com.e.pcoach.ui.firsttime.signup.SignUpFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.tbuonomo.viewpagerdotsindicator.WormDotsIndicator;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerAppCompatActivity;

public class FirstTimeActivity extends BaseActivity implements FragmentListener {

    private static final String TAG = "FirstTimeActivity";
    public final String IS_FIRST_TIME = "IS_FIRST_TIME";
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.wormDotsIndicatorProduct)
    WormDotsIndicator indicator;
    @BindView(R.id.fabForward)
    FloatingActionButton buttonNext;
    @BindView(R.id.fabBackward)
    FloatingActionButton buttonSkip;
    @BindView(R.id.root)
    RelativeLayout root;

    @Inject
    ActivityHelper.Helper helper;
    @Inject
    LoginFragment loginFragment;
    @Inject
    SignUpFragment signUpFragment;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    int[] mIntroSliderlayouts;
    @Inject
    MyPagerAdapter mPagerAdapter;

    private Bundle mInstanceState;
    private Boolean mIsIntroSliderDone = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_time);
        ButterKnife.bind(this);
        helper.setRotationPortrait(this);
        build();
    }

    private void build() {
        Log.d(TAG, "build: " + checkTheFirstTime());
        mIsIntroSliderDone = checkTheFirstTime();

        /* checks if intro slider is finished till now
         or not if yes starts login if not starts the intro slider */
        if (mIsIntroSliderDone) {
            // go to login page view
            startLoginFragment();
        } else {
            // start intro slider
            setOnClickListener();
            setUpViewPager();
        }
    }

    private void setOnClickListener() {
        buttonSkip.setOnClickListener(v -> {
            root.setVisibility(View.GONE);
            setFirstTimeIsDone();
            startLoginFragment();
        });

        buttonNext.setOnClickListener(v -> {
            int currentPage = viewPager.getCurrentItem() + 1;
            if (currentPage < mIntroSliderlayouts.length) {
                //move to next page
                viewPager.setCurrentItem(currentPage);
            } else {
                startLoginFragment();
                root.setVisibility(View.GONE);
                setFirstTimeIsDone();
            }

        });
    }

    public void setUpViewPager() {
        viewPager.setAdapter(mPagerAdapter);
        indicator.setViewPager(viewPager);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == mIntroSliderlayouts.length - 1) {
                    //LAST PAGE
                    buttonSkip.setVisibility(View.GONE);
                } else {
                    buttonSkip.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    /**
     * starts login when Activity gets started
     * for the first time in this run
     */
    private void startLoginFragment() {
        helper.transitToFragment(this, loginFragment, false);
    }

    /**
     * opens {@link LoginFragment} when user clicks
     * and sends the previous data to it
     */
    @Override
    public void openLoginFragment() {
        loginFragment.setArguments(mInstanceState);
        helper.transitToFragment(this, loginFragment, true);
    }

    /**
     * opens {@link SignUpFragment} when user clicks
     * and sends the previous data to it
     */
    @Override
    public void openSignUpFragment() {
        signUpFragment.setArguments(mInstanceState);
        helper.transitToFragment(this, signUpFragment, true);
    }

    /**
     * saves {@link LoginFragment} and
     * {@link SignUpFragment} state when ever
     * onStop method of it get called
     */
    @Override
    public void onBackPressedInstance(Bundle savedInstance) {
        mInstanceState = savedInstance;
    }

    /**
     * checks if this is intro slider
     * is finished before or not
     */
    public boolean checkTheFirstTime() {
        return sharedPreferences.getBoolean(
                IS_FIRST_TIME, false);
    }

    /**
     * it would save a boolean when intro slider finishes
     * so next time it only start the {@link LoginFragment}
     */
    private void setFirstTimeIsDone() {
        sharedPreferences.edit().putBoolean(IS_FIRST_TIME, true).apply();
    }

}
