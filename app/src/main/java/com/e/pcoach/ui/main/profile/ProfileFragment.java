package com.e.pcoach.ui.main.profile;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.e.pcoach.R;
import com.e.pcoach.db.entities.UserEntity;
import com.e.pcoach.ui.firsttime.UserViewModel;
import com.e.pcoach.ui.helper.ActivityHelper;
import com.e.pcoach.ui.helper.DarkModePrefManager;
import com.e.pcoach.ui.main.MainActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;
import io.reactivex.functions.Consumer;

public class ProfileFragment extends DaggerFragment implements View.OnClickListener {

    @Inject
    ActivityHelper.Helper mHelper;
    @Inject
    DarkModePrefManager darkModePrefManager;
    @BindView(R.id.imageViewBack)
    ImageView imageViewBack;
    @BindView(R.id.textViewPhoneNumber)
    TextView textViewPhoneNumber;
    @BindView(R.id.textViewLastName)
    TextView textViewLastName;
    @BindView(R.id.textViewName)
    TextView textViewName;
    @BindView(R.id.switchTheme)
    Switch switchTheme;
    private UserViewModel userViewModel;
    private MainActivity mActivity;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        userViewModel = new ViewModelProvider(this).get(UserViewModel.class);
        View root = inflater.inflate(R.layout.fragment_profile, container, false);
        mActivity = (MainActivity) getActivity();
        ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        build();
    }

    private void build() {
        init();
        setThemeChange();
        setData();
    }

    private void setThemeChange() {
        if (darkModePrefManager.isNightMode()) {
            switchTheme.setChecked(true);
        } else {
            switchTheme.setChecked(false);
        }
        switchTheme.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    darkModePrefManager.setDarkMode(true);
                    darkModePrefManager.setThemeChanged(true);
                    mActivity.restartApp();
                } else {
                    darkModePrefManager.setDarkMode(false);
                    darkModePrefManager.setThemeChanged(true);
                    mActivity.restartApp();
                }
            }
        });
    }

    @SuppressLint("CheckResult")
    private void setData() {
        userViewModel.getUser().subscribe(new Consumer<UserEntity>() {
            @Override
            public void accept(UserEntity userEntity) throws Exception {
                if (userEntity != null) {
                    textViewName.setText(userEntity.getFirstName());
                    textViewLastName.setText(userEntity.getLastName());
                    textViewPhoneNumber.setText(userEntity.getPhoneNumber());
                }
            }
        });
    }

    private void init() {
        imageViewBack.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.setViewPagerVisibility(false);
        mActivity.setNavViewVisibility(false);
//        Toast.makeText(mActivity, "profile", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mActivity.setNavViewVisibility(true);
        mActivity.setViewPagerVisibility(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageViewBack:
                mHelper.removeFragment(mActivity, this);
                return;
        }
    }
}