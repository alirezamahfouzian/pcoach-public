package com.e.pcoach.db.entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "workout_date")
public class WorkoutDateEntity {

    @PrimaryKey()
    @NonNull
    @ColumnInfo(name = "workout_in_month")
    private int workoutInMonth;

    @ColumnInfo(name = "workout_in_weak")
    private int workoutInWeek;

    public WorkoutDateEntity(@NonNull int workoutInMonth, int workoutInWeek) {
        this.workoutInMonth = workoutInMonth;
        this.workoutInWeek = workoutInWeek;
    }

    public int getWorkoutInMonth() {
        return workoutInMonth;
    }

    public void setWorkoutInMonth(int workoutInMonth) {
        this.workoutInMonth = workoutInMonth;
    }

    public int getWorkoutInWeek() {
        return workoutInWeek;
    }

    public void setWorkoutInWeek(int workoutInWeek) {
        this.workoutInWeek = workoutInWeek;
    }
}
