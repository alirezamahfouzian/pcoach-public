package com.e.pcoach.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.e.pcoach.db.entities.ProgramsEntity;
import com.e.pcoach.db.entities.WorkoutProgramEntity;
import com.e.pcoach.ui.main.model.MealModel;
import com.e.pcoach.ui.main.model.WorkoutModel;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Maybe;

@Dao
public interface WorkoutProgramDao {

    @Insert
    Completable insertWorkoutProgram(WorkoutProgramEntity workoutProgramEntity);

    @Query("select * from workout_program where program_id = :programId")
    Maybe<List<WorkoutProgramEntity>> getWorkoutPrograms(int programId);

    @Query("select * from workout_program")
    Maybe<List<WorkoutProgramEntity>> getAllWorkoutPrograms();

    @Query("SELECT workout_day_position FROM workout_program inner join programs " +
            "on programs.program_id = workout_program.program_id " +
            "where programs.is_enabled = 1")
    LiveData<List<Integer>> getWorkoutProgramDays();

    @Query("SELECT workout_day_position FROM workout_program inner join programs " +
            "on programs.program_id = workout_program.program_id " +
            "where programs.program_id = :programId")
    LiveData<List<Integer>> getWorkoutProgramDaysById(int programId);

    @Query("SELECT moves.move_title," +
            " moves.move_url, workout_program.workout_day_position " +
            ", workout_program.workout_set , workout_program.workout_rep " +
            ", workout_program.workout_percentage " +
            "FROM programs inner join workout_program " +
            "on programs.program_id = workout_program.program_id " +
            "inner join " +
            "moves on workout_program.move_id = moves.move_id " +
            "where workout_program.workout_day_position = :dayPosition AND " +
            "programs.is_enabled = 1")
    LiveData<List<WorkoutModel>> getDayMoves(int dayPosition);

    @Query("SELECT moves.move_title," +
            " moves.move_url, workout_program.workout_day_position " +
            ", workout_program.workout_set , workout_program.workout_rep " +
            ", workout_program.workout_percentage " +
            "FROM programs inner join workout_program " +
            "on programs.program_id = workout_program.program_id " +
            "inner join " +
            "moves on workout_program.move_id = moves.move_id " +
            "where workout_program.workout_day_position = :dayPosition AND " +
            "programs.is_enabled = 1")
    Maybe<List<WorkoutModel>> getDayMovesObservable(int dayPosition);

    @Query("SELECT moves.move_title," +
            " moves.move_url, workout_program.workout_day_position " +
            ", workout_program.workout_set , workout_program.workout_rep " +
            ", workout_program.workout_percentage " +
            "FROM programs inner join workout_program " +
            "on programs.program_id = workout_program.program_id " +
            "inner join " +
            "moves on workout_program.move_id = moves.move_id " +
            "where workout_program.workout_day_position = :dayPosition AND " +
            "programs.program_id = :programId")
    LiveData<List<WorkoutModel>> getDayByProgramId(int dayPosition, int programId);
}
