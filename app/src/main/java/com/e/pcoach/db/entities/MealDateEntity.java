package com.e.pcoach.db.entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "meal_date")
public class MealDateEntity {

    @PrimaryKey()
    @NonNull
    @ColumnInfo(name = "meal_in_month")
    private int mealInMonth;

    @ColumnInfo(name = "meal_in_weak")
    private int mealInWeek;

    public MealDateEntity(@NonNull int mealInMonth, int mealInWeek) {
        this.mealInMonth = mealInMonth;
        this.mealInWeek = mealInWeek;
    }

    public void setMealInMonth(int mealInMonth) {
        this.mealInMonth = mealInMonth;
    }

    public void setMealInWeek(int mealInWeek) {
        this.mealInWeek = mealInWeek;
    }

    public int getMealInMonth() {
        return mealInMonth;
    }

    public int getMealInWeek() {
        return mealInWeek;
    }
}
