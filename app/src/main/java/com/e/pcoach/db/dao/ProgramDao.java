package com.e.pcoach.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.e.pcoach.db.entities.ProgramsEntity;
import com.e.pcoach.ui.main.history.HistoryModel;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Single;

@Dao
public interface ProgramDao {

    @Insert
    Completable insertProgram(ProgramsEntity programsEntity);

    @Update
    Completable updateProgram(ProgramsEntity programsEntity);

    @Query("SELECT * FROM programs WHERE is_enabled = 1")
    LiveData<ProgramsEntity> getEnabledPrograms();

    @Query("SELECT * FROM programs WHERE is_enabled = 1")
    Maybe<ProgramsEntity> getEnabledProgramsObservable();

    @Query("SELECT * FROM programs WHERE program_id = :programId")
    Maybe<ProgramsEntity> getProgramById(int programId);

    @Query("SELECT program_id FROM programs ORDER BY program_id  DESC LIMIT 1")
    Maybe<Integer> getLastProgramId();

    @Query("SELECT * FROM programs ORDER BY program_id ASC")
    Maybe<List<ProgramsEntity>> getAllPrograms();

    @Query("SELECT * FROM programs LIMIT 1")
    Maybe<List<ProgramsEntity>> getAllProgramsLimited();

    @Query("SELECT program_id,program_created_time, program_title FROM programs ORDER BY program_id Desc")
    LiveData<List<HistoryModel>> getHistoryPrograms();

}
