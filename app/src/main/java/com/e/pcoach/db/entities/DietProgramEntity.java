package com.e.pcoach.db.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "diet_program")
public class DietProgramEntity {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "diet_program_id")
    private int dietProgramId;

    @ColumnInfo(name = "program_id")
    private int programId;

    @ColumnInfo(name = "meal_id")
    private String mealId;

    @ColumnInfo(name = "meal_amount")
    private int mealAmount;

    @ColumnInfo(name = "meal_day_position")
    private int mealDayPosition;

    @ColumnInfo(name = "meal_type")
    private int mealType;

    public int getDietProgramId() {
        return dietProgramId;
    }

    public void setDietProgramId(int dietProgramId) {
        this.dietProgramId = dietProgramId;
    }

    public int getProgramId() {
        return programId;
    }

    public void setProgramId(int programId) {
        this.programId = programId;
    }

    public String getMealId() {
        return mealId;
    }

    public void setMealId(String mealId) {
        this.mealId = mealId;
    }

    public int getMealAmount() {
        return mealAmount;
    }

    public void setMealAmount(int mealAmount) {
        this.mealAmount = mealAmount;
    }

    public int getMealDayPosition() {
        return mealDayPosition;
    }

    public void setMealDayPosition(int mealDayPosition) {
        this.mealDayPosition = mealDayPosition;
    }

    public int getMealType() {
        return mealType;
    }

    public void setMealType(int mealType) {
        this.mealType = mealType;
    }
}
