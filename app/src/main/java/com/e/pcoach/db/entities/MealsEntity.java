package com.e.pcoach.db.entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "meals")
public class MealsEntity {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "meal_id")
    private String mealId;

    @ColumnInfo(name = "meal_title")
    private String mealTitle;

    @ColumnInfo(name = "meal_amount_type")
    private String mealType;

    public MealsEntity(String mealId, String mealTitle, String mealType) {
        this.mealId = mealId;
        this.mealTitle = mealTitle;
        this.mealType = mealType;
    }

    public String getMealId() {
        return mealId;
    }

    public void setMealId(String mealId) {
        this.mealId = mealId;
    }

    public String getMealTitle() {
        return mealTitle;
    }

    public void setMealTitle(String mealTitle) {
        this.mealTitle = mealTitle;
    }

    public String getMealType() {
        return mealType;
    }

    public void setMealType(String mealType) {
        this.mealType = mealType;
    }
}
