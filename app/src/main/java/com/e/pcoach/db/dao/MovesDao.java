package com.e.pcoach.db.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.e.pcoach.db.entities.DietProgramEntity;
import com.e.pcoach.db.entities.MealsEntity;
import com.e.pcoach.db.entities.MovesEntity;
import com.e.pcoach.db.entities.WorkoutProgramEntity;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Maybe;

@Dao
public interface MovesDao {

    @Insert
    void insertMoves(MovesEntity movesEntity);

    @Query("select * from moves where move_id = :move_id")
    Maybe<MovesEntity> getMove(int move_id);


}
