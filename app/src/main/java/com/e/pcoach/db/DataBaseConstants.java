package com.e.pcoach.db;

public class DataBaseConstants {
    public static final String DATABASE_NAME = "pcouch_database";
    public static final int DATABASE_VERSION = 1;
}
