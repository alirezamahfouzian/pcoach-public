package com.e.pcoach.db.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "user_general_info")
public class UserGeneralInfoEntity {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "user_general_info_id")
    private int userGeneralInfoId;

    @ColumnInfo(name = "program_id")
    private int programId;

    @ColumnInfo(name = "gender")
    private int gender;

    @ColumnInfo(name = "height")
    private int height;

    @ColumnInfo(name = "weight")
    private double weight;

    @ColumnInfo(name = "wrist")
    private double wrist;

    @ColumnInfo(name = "age")
    private int age;

    @ColumnInfo(name = "experience")
    private int experience;

    @ColumnInfo(name = "activity")
    private int activity;

    @ColumnInfo(name = "allergies_value")
    private String allergiesValues;

    @ColumnInfo(name = "body_type_values")
    private String bodyTypeValues;

    @ColumnInfo(name = "info_inserted_time")
    private String infoInsertedTime;

    public String getInfoInsertedTime() {
        return infoInsertedTime;
    }

    public void setInfoInsertedTime(String infoInsertedTime) {
        this.infoInsertedTime = infoInsertedTime;
    }

    public int getProgramId() {
        return programId;
    }

    public void setProgramId(int programId) {
        this.programId = programId;
    }

    public int getUserGeneralInfoId() {
        return userGeneralInfoId;
    }

    public void setUserGeneralInfoId(int userGeneralInfoId) {
        this.userGeneralInfoId = userGeneralInfoId;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getWrist() {
        return wrist;
    }

    public void setWrist(double wrist) {
        this.wrist = wrist;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public int getActivity() {
        return activity;
    }

    public void setActivity(int activity) {
        this.activity = activity;
    }

    public String getAllergiesValues() {
        return allergiesValues;
    }

    public void setAllergiesValues(String allergiesValues) {
        this.allergiesValues = allergiesValues;
    }

    public String getBodyTypeValues() {
        return bodyTypeValues;
    }

    public void setBodyTypeValues(String bodyTypeValues) {
        this.bodyTypeValues = bodyTypeValues;
    }
}
