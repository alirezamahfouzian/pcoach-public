package com.e.pcoach.db.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "workout_program")
public class WorkoutProgramEntity {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "workout_program_id")
    private int workoutProgramId;

    @ColumnInfo(name = "program_id")
    private int programId;

    @ColumnInfo(name = "move_id")
    private String moveId;

    @ColumnInfo(name = "workout_set")
    private int workoutSet;

    @ColumnInfo(name = "workout_rep")
    private int workoutRep;

    @ColumnInfo(name = "workout_percentage")
    private int workoutPercentage;

    @ColumnInfo(name = "workout_day_position")
    private int workoutDayPosition;

    public int getWorkoutProgramId() {
        return workoutProgramId;
    }

    public void setWorkoutProgramId(int workoutProgramId) {
        this.workoutProgramId = workoutProgramId;
    }

    public int getProgramId() {
        return programId;
    }

    public void setProgramId(int programId) {
        this.programId = programId;
    }

    public String getMoveId() {
        return moveId;
    }

    public void setMoveId(String moveId) {
        this.moveId = moveId;
    }

    public int getWorkoutSet() {
        return workoutSet;
    }

    public void setWorkoutSet(int workoutSet) {
        this.workoutSet = workoutSet;
    }

    public int getWorkoutRep() {
        return workoutRep;
    }

    public void setWorkoutRep(int workoutRep) {
        this.workoutRep = workoutRep;
    }

    public int getWorkoutPercentage() {
        return workoutPercentage;
    }

    public void setWorkoutPercentage(int workoutPercentage) {
        this.workoutPercentage = workoutPercentage;
    }

    public int getWorkoutDayPosition() {
        return workoutDayPosition;
    }

    public void setWorkoutDayPosition(int workoutDayPosition) {
        this.workoutDayPosition = workoutDayPosition;
    }
}
