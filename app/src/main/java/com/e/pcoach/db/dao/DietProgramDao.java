package com.e.pcoach.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.e.pcoach.db.entities.DietProgramEntity;
import com.e.pcoach.ui.main.model.MealModel;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Maybe;

@Dao
public interface DietProgramDao {

    @Insert
    Completable insertDietProgram(DietProgramEntity dietProgramEntity);

    @Query("SELECT * FROM diet_program WHERE program_id = :programId")
    Maybe<List<DietProgramEntity>> getDietPrograms(int programId);

    @Query("SELECT * FROM diet_program")
    Maybe<List<DietProgramEntity>> getAllDietPrograms();

    @Query("SELECT meal_day_position FROM diet_program inner join programs " +
            "on programs.program_id = diet_program.program_id " +
            "where programs.is_enabled = 1")
    LiveData<List<Integer>> getDietProgramDays();

    @Query("SELECT meals.meal_title," +
            " meals.meal_amount_type, diet_program.meal_amount " +
            ", diet_program.meal_type , diet_program.meal_day_position " +
            "FROM programs inner join diet_program " +
            "on programs.program_id = diet_program.program_id " +
            "inner join " +
            "meals on diet_program.meal_id = meals.meal_id " +
            "where diet_program.meal_day_position = :dayPosition " +
            "AND programs.is_enabled = 1 AND " +
            "programs.is_enabled = 1")
    LiveData<List<MealModel>> getDayMeals(int dayPosition);

    @Query("SELECT meals.meal_title," +
            " meals.meal_amount_type, diet_program.meal_amount " +
            ", diet_program.meal_type , diet_program.meal_day_position " +
            "FROM programs inner join diet_program " +
            "on programs.program_id = diet_program.program_id " +
            "inner join " +
            "meals on diet_program.meal_id = meals.meal_id " +
            "where diet_program.meal_day_position = :dayPosition " +
            "AND diet_program.meal_type = :mealType AND " +
            "programs.is_enabled = 1")
    LiveData<List<MealModel>> getDayMealsByType(int dayPosition, int mealType);

    @Query("SELECT meals.meal_title," +
            " meals.meal_amount_type, diet_program.meal_amount " +
            ", diet_program.meal_type , diet_program.meal_day_position " +
            "FROM programs inner join diet_program " +
            "on programs.program_id = diet_program.program_id " +
            " inner join " +
            "meals on diet_program.meal_id = meals.meal_id " +
            "where diet_program.meal_day_position = :dayPosition " +
            "AND programs.program_id = :programId")
    LiveData<List<MealModel>> getDayMealsByTypeByProgramId(int dayPosition, int programId);

}
