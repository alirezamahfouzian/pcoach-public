package com.e.pcoach.db.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.e.pcoach.db.entities.DietProgramEntity;
import com.e.pcoach.db.entities.UserGeneralInfoEntity;
import com.e.pcoach.db.entities.WorkoutProgramEntity;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Single;

@Dao
public interface UserGeneralInfoDao {

    @Insert
    void insertGeneralInfo(UserGeneralInfoEntity userGeneralInfoEntity);

    @Update
    void updateGeneralInfo(UserGeneralInfoEntity userGeneralInfoEntity);

    @Query("select * from user_general_info where user_general_info_id = :generalInfoId")
    Maybe<UserGeneralInfoEntity> getGeneralInfo(int generalInfoId);

    @Query("select * from user_general_info")
    Maybe<List<UserGeneralInfoEntity>> getAllGeneralInfo();

}
