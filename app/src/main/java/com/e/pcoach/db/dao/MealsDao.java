package com.e.pcoach.db.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.e.pcoach.db.entities.DietProgramEntity;
import com.e.pcoach.db.entities.MealsEntity;
import com.e.pcoach.db.entities.WorkoutProgramEntity;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Maybe;

@Dao
public interface MealsDao {

    @Insert
    void insertMeals(MealsEntity mealsEntity);

    @Query("select * from meals where meal_id = :meal_id")
    Maybe<MealsEntity> getMeal(int meal_id);

}
