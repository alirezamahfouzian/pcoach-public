package com.e.pcoach.db.entities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "moves")
public class MovesEntity {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "move_id")
    private String moveId;

    @ColumnInfo(name = "move_title")
    private String moveTitle;

    @ColumnInfo(name = "move_url")
    private String moveUrl;

    public MovesEntity(@NonNull String moveId, String moveTitle, String moveUrl) {
        this.moveId = moveId;
        this.moveTitle = moveTitle;
        this.moveUrl = moveUrl;
    }

    @NonNull
    public String getMoveId() {
        return moveId;
    }

    public String getMoveTitle() {
        return moveTitle;
    }

    public String getMoveUrl() {
        return moveUrl;
    }
}
