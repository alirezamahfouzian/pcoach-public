package com.e.pcoach.db;


import android.util.Log;

import com.e.pcoach.db.dao.DateDao;
import com.e.pcoach.db.dao.MealsDao;
import com.e.pcoach.db.dao.MovesDao;
import com.e.pcoach.db.entities.MealDateEntity;
import com.e.pcoach.db.entities.MealsEntity;
import com.e.pcoach.db.entities.MovesEntity;
import com.e.pcoach.db.entities.WorkoutDateEntity;

import io.reactivex.MaybeObserver;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class ProgramPartInsert {

    private int workoutCount = 0;

    public void insertMeals(MealsDao mealDao) {
        CompositeDisposable disposable = new CompositeDisposable();
        Observable.create(new ObservableOnSubscribe<Object>() {
            @Override
            public void subscribe(ObservableEmitter<Object> emitter) throws Exception {
                mealDao.insertMeals(new MealsEntity("aa", "پنیر لیقوان", "گرم"));
                mealDao.insertMeals(new MealsEntity("ab", "شیر", "لیوان"));
                mealDao.insertMeals(new MealsEntity("ac", "نان سنگک", "کف دست"));
                mealDao.insertMeals(new MealsEntity("ad", "جوجه", "عدد"));
                mealDao.insertMeals(new MealsEntity("ae", "سبزی", "گرم"));
                mealDao.insertMeals(new MealsEntity("af", "ماست", "قاشق"));
                mealDao.insertMeals(new MealsEntity("ag", "پلو", "قاشق"));
                mealDao.insertMeals(new MealsEntity("ah", "تخم مرغ", "عدد"));
                mealDao.insertMeals(new MealsEntity("ai", "ماست", "قاشق"));
                mealDao.insertMeals(new MealsEntity("aj", "سالاد", "قاشق"));
            }
        })
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Object>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable.add(d);
                    }

                    @Override
                    public void onNext(Object o) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        disposable.clear();
                    }
                });
    }

    public void insertMoves(MovesDao movesDao) {
        CompositeDisposable disposable = new CompositeDisposable();
        Observable.create(new ObservableOnSubscribe<Object>() {
            @Override
            public void subscribe(ObservableEmitter<Object> emitter) throws Exception {
                movesDao.insertMoves(new MovesEntity("aa", "شانه آرنولدی", "i00002"));
                movesDao.insertMoves(new MovesEntity("ab", "نشر از بقل", "i00001"));
                movesDao.insertMoves(new MovesEntity("ac", "سرشانه هالتر", "i00002"));
                movesDao.insertMoves(new MovesEntity("ad", "نشر از جلو", "i00001"));
                movesDao.insertMoves(new MovesEntity("ae", "سرشانه سیم کش بقل", "i00002"));
                movesDao.insertMoves(new MovesEntity("af", "سرشانه سیم کش از جلو", "i00001"));
                movesDao.insertMoves(new MovesEntity("ag", "سرشانه خم", "i00002"));
                movesDao.insertMoves(new MovesEntity("ah", "جلو بازو هالتر", "i00001"));
            }
        })
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Object>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable.add(d);
                    }

                    @Override
                    public void onNext(Object o) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        disposable.clear();
                    }
                });
    }

    public void insertMealDate(DateDao dateDao) {
        CompositeDisposable disposable = new CompositeDisposable();
        Observable
                .create(new ObservableOnSubscribe<Object>() {
                    @Override
                    public void subscribe(ObservableEmitter<Object> emitter) throws Exception {
                        for (int i = 0; i < 30; i++) {
                            dateDao.insertMealDate(new MealDateEntity(i, 0));
                        }
                    }
                })
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Object>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable.add(d);
                    }

                    @Override
                    public void onNext(Object o) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        disposable.clear();
                    }
                });
    }

    public void insertWorkoutDate(DateDao dateDao) {
        CompositeDisposable disposable = new CompositeDisposable();
        Observable
                .create(new ObservableOnSubscribe<Object>() {
                    @Override
                    public void subscribe(ObservableEmitter<Object> emitter) throws Exception {
                        for (int i = 0; i < 30; i++) {
                            dateDao.insertWorkoutDate(new WorkoutDateEntity(i, workoutCount));
                            if (workoutCount == 6) {
                                workoutCount = 0;
                            } else {
                                workoutCount++;
                            }
                        }
                    }
                })
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Object>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable.add(d);
                    }

                    @Override
                    public void onNext(Object o) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
//                        getall(dateDao);
                    }
                });
    }

}
