package com.e.pcoach.db.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "programs")
public class ProgramsEntity {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "program_id")
    private int programId;

    @ColumnInfo(name = "user_id")
    private int userId;

    @ColumnInfo(name = "program_title")
    private String programTitle;

    @ColumnInfo(name = "program_minute")
    private int programMinute;

    @ColumnInfo(name = "program_goal")
    private int programGoal;

    @ColumnInfo(name = "program_created_time")
    private String programCreatedTime;

    @ColumnInfo(name = "program_start_time")
    private String programStartTime;

    @ColumnInfo(name = "is_enabled")
    private int isEnabled;

    @ColumnInfo(name = "program_end_time")
    private String programEndTime;

    @ColumnInfo(name = "program_workout_days")
    private String programWorkoutDays;

    @ColumnInfo(name = "program_diet_days")
    private String programDietDays;

    public String getProgramWorkoutDays() {
        return programWorkoutDays;
    }

    public void setProgramWorkoutDays(String programWorkoutDays) {
        this.programWorkoutDays = programWorkoutDays;
    }

    public String getProgramDietDays() {
        return programDietDays;
    }

    public void setProgramDietDays(String programDietDays) {
        this.programDietDays = programDietDays;
    }

    public int getIsEnabled() {
        return isEnabled;
    }

    public void setIsEnabled(int isEnabled) {
        this.isEnabled = isEnabled;
    }

    public int getProgramId() {
        return programId;
    }

    public void setProgramId(int programId) {
        this.programId = programId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getProgramTitle() {
        return programTitle;
    }

    public void setProgramTitle(String programTitle) {
        this.programTitle = programTitle;
    }

    public int getProgramMinute() {
        return programMinute;
    }

    public void setProgramMinute(int programMinute) {
        this.programMinute = programMinute;
    }

    public int getProgramGoal() {
        return programGoal;
    }

    public void setProgramGoal(int programGoal) {
        this.programGoal = programGoal;
    }

    public String getProgramCreatedTime() {
        return programCreatedTime;
    }

    public void setProgramCreatedTime(String programCreatedTime) {
        this.programCreatedTime = programCreatedTime;
    }

    public String getProgramStartTime() {
        return programStartTime;
    }

    public void setProgramStartTime(String programStartTime) {
        this.programStartTime = programStartTime;
    }

    public String getProgramEndTime() {
        return programEndTime;
    }

    public void setProgramEndTime(String programEndTime) {
        this.programEndTime = programEndTime;
    }
}
