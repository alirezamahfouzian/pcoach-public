package com.e.pcoach.db.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.e.pcoach.db.entities.DietProgramEntity;
import com.e.pcoach.db.entities.MealDateEntity;
import com.e.pcoach.db.entities.WorkoutDateEntity;
import com.e.pcoach.ui.main.model.WorkoutDate;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Maybe;

@Dao
public interface DateDao {

    @Insert
    void insertMealDate(MealDateEntity mealDateEntity);

    @Update
    void updateMealDate(MealDateEntity mealDateEntity);

    @Query("SELECT * FROM meal_date WHERE meal_in_month = :positionInMonth")
    Maybe<MealDateEntity> getMealByDayInMonth(int positionInMonth);

    @Insert
    void insertWorkoutDate(WorkoutDateEntity workoutDate);

    @Update
    void updateWorkoutDate(WorkoutDateEntity workoutDate);

    @Query("SELECT workout_in_weak FROM workout_date WHERE workout_in_month = :positionInMonth")
    Maybe<Integer> getWorkoutByDayInMonth(int positionInMonth);

}
