package com.e.pcoach.db;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.e.pcoach.db.dao.DateDao;
import com.e.pcoach.db.dao.DietProgramDao;
import com.e.pcoach.db.dao.MealsDao;
import com.e.pcoach.db.dao.MovesDao;
import com.e.pcoach.db.dao.ProgramDao;
import com.e.pcoach.db.dao.UserDao;
import com.e.pcoach.db.dao.UserGeneralInfoDao;
import com.e.pcoach.db.dao.WorkoutProgramDao;
import com.e.pcoach.db.entities.DietProgramEntity;
import com.e.pcoach.db.entities.MealDateEntity;
import com.e.pcoach.db.entities.MealsEntity;
import com.e.pcoach.db.entities.MovesEntity;
import com.e.pcoach.db.entities.ProgramsEntity;
import com.e.pcoach.db.entities.UserEntity;
import com.e.pcoach.db.entities.UserGeneralInfoEntity;
import com.e.pcoach.db.entities.WorkoutDateEntity;
import com.e.pcoach.db.entities.WorkoutProgramEntity;

import io.reactivex.disposables.CompositeDisposable;

@Database(entities = {DietProgramEntity.class, WorkoutProgramEntity.class, ProgramsEntity.class,
        UserEntity.class, UserGeneralInfoEntity.class, MovesEntity.class, MealsEntity.class,
        MealDateEntity.class, WorkoutDateEntity.class},
        version = DataBaseConstants.DATABASE_VERSION, exportSchema = false)
public abstract class PcoachDataBase extends RoomDatabase {

    public static PcoachDataBase instance;

    private static RoomDatabase.Callback callback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            ProgramPartInsert programPartInsert = new ProgramPartInsert();
            programPartInsert.insertWorkoutDate(instance.dateDao());
            programPartInsert.insertMealDate(instance.dateDao());
            programPartInsert.insertMeals(instance.mealsDao());
            programPartInsert.insertMoves(instance.movesDao());
            super.onCreate(db);
        }
    };

    public static synchronized PcoachDataBase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(), PcoachDataBase.class,
                    DataBaseConstants.DATABASE_NAME)
                    .addCallback(callback)
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

    public abstract ProgramDao programDao();

    public abstract DietProgramDao dietProgramDao();

    public abstract WorkoutProgramDao workoutProgramDao();

    public abstract UserDao userDao();

    public abstract UserGeneralInfoDao userGeneralInfoDao();

    public abstract MovesDao movesDao();

    public abstract MealsDao mealsDao();

    public abstract DateDao dateDao();
}
