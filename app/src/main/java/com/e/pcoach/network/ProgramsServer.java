package com.e.pcoach.network;

public class ProgramsServer {
    private int programId;
    private int userId;
    private String programTitle;
    private int programMinute;
    private int programGoal;
    private String programCreatedTime;
    private String programStartTime;
    private String programEndTime;
    private int isEnabled;

    public int getIsEnabled() {
        return isEnabled;
    }

    public void setIsEnabled(int isEnabled) {
        this.isEnabled = isEnabled;
    }

    public int getProgramId() {
        return programId;
    }

    public void setProgramId(int programId) {
        this.programId = programId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getProgramTitle() {
        return programTitle;
    }

    public void setProgramTitle(String programTitle) {
        this.programTitle = programTitle;
    }

    public int getProgramMinute() {
        return programMinute;
    }

    public void setProgramMinute(int programMinute) {
        this.programMinute = programMinute;
    }

    public int getProgramGoal() {
        return programGoal;
    }

    public void setProgramGoal(int programGoal) {
        this.programGoal = programGoal;
    }

    public String getProgramCreatedTime() {
        return programCreatedTime;
    }

    public void setProgramCreatedTime(String programCreatedTime) {
        this.programCreatedTime = programCreatedTime;
    }

    public String getProgramStartTime() {
        return programStartTime;
    }

    public void setProgramStartTime(String programStartTime) {
        this.programStartTime = programStartTime;
    }

    public String getProgramEndTime() {
        return programEndTime;
    }

    public void setProgramEndTime(String programEndTime) {
        this.programEndTime = programEndTime;
    }
}
