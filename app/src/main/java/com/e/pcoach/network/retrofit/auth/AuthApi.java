package com.e.pcoach.network.retrofit.auth;


import com.e.pcoach.db.entities.UserEntity;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public interface AuthApi {

    @POST("/register.php")
    Call<ResponseBody> addUser(@Body User user);

}
