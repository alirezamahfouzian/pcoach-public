package com.e.pcoach.network;

public class DietProgramServer {

    private int dietProgramId;
    private int programId;
    private String meals;

    private String mealAmount;
    private String mealDayPositions;
    private String mealDayOrder;
    private String mealTypesOrder;

    public String getMealAmount() {
        return mealAmount;
    }

    public void setMealAmount(String mealAmount) {
        this.mealAmount = mealAmount;
    }

    public String getMealDayOrder() {
        return mealDayOrder;
    }

    public void setMealDayOrder(String mealDayOrder) {
        this.mealDayOrder = mealDayOrder;
    }

    public int getDietProgramId() {
        return dietProgramId;
    }

    public void setDietProgramId(int dietProgramId) {
        this.dietProgramId = dietProgramId;
    }

    public int getProgramId() {
        return programId;
    }

    public void setProgramId(int programId) {
        this.programId = programId;
    }

    public String getMeals() {
        return meals;
    }

    public void setMeals(String meals) {
        this.meals = meals;
    }

    public String getMealAmounts() {
        return mealAmount;
    }

    public String getMealDayPositions() {
        return mealDayPositions;
    }

    public void setMealDayPositions(String mealDayPositions) {
        this.mealDayPositions = mealDayPositions;
    }

    public String getMealTypesOrder() {
        return mealTypesOrder;
    }

    public void setMealTypesOrder(String mealTypesOrder) {
        this.mealTypesOrder = mealTypesOrder;
    }
}
