package com.e.pcoach.network;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "workout_program_server")
public class WorkoutProgramServer {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "workout_program_id")
    private int workoutProgramId;

    @ColumnInfo(name = "program_id")
    private int programId;

    @ColumnInfo(name = "moves")
    private String moves;

    @ColumnInfo(name = "workout_sets")
    private String workoutSets;

    @ColumnInfo(name = "workout_reps")
    private String workoutReps;

    @ColumnInfo(name = "workout_percentages")
    private String workoutPercentages;

    @ColumnInfo(name = "workout_day_positions")
    private String workoutDayPositions;

    @ColumnInfo(name = "workout_program_orders")
    private String workoutProgramOrders;

    public String getWorkoutProgramOrders() {
        return workoutProgramOrders;
    }

    public void setWorkoutProgramOrders(String workoutProgramOrders) {
        this.workoutProgramOrders = workoutProgramOrders;
    }

    public int getWorkoutProgramId() {
        return workoutProgramId;
    }

    public void setWorkoutProgramId(int workoutProgramId) {
        this.workoutProgramId = workoutProgramId;
    }

    public int getProgramId() {
        return programId;
    }

    public void setProgramId(int programId) {
        this.programId = programId;
    }

    public String getMoves() {
        return moves;
    }

    public void setMoves(String moves) {
        this.moves = moves;
    }

    public String getWorkoutSets() {
        return workoutSets;
    }

    public void setWorkoutSets(String workoutSets) {
        this.workoutSets = workoutSets;
    }

    public String getWorkoutReps() {
        return workoutReps;
    }

    public void setWorkoutReps(String workoutReps) {
        this.workoutReps = workoutReps;
    }

    public String getWorkoutPercentages() {
        return workoutPercentages;
    }

    public void setWorkoutPercentages(String workoutPercentages) {
        this.workoutPercentages = workoutPercentages;
    }

    public String getWorkoutDayPositions() {
        return workoutDayPositions;
    }

    public void setWorkoutDayPositions(String workoutDayPosition) {
        this.workoutDayPositions = workoutDayPosition;
    }
}
