package com.e.pcoach.network.retrofit.auth;

import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("name")
    private String name;
    @SerializedName("family")
    private String family;
    @SerializedName("gmail")
    private String gmail;
    @SerializedName("password")
    private String password;
    @SerializedName("phone_number")
    private String phone_number;

    public User(String name, String family, String gmail, String password, String phone_number) {
        this.name = name;
        this.family = family;
        this.gmail = gmail;
        this.password = password;
        this.phone_number = phone_number;
    }
}
