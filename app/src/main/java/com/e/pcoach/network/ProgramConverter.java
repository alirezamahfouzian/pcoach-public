package com.e.pcoach.network;

import org.jetbrains.annotations.NotNull;

public class ProgramConverter {

    public int convertPercentage(@NotNull String item) {
        switch (item) {
            case "a":
                return 100;
            case "b":
                return 95;
            case "c":
                return 90;
            case "d":
                return 85;
            case "e":
                return 80;
            case "f":
                return 75;
            case "g":
                return 70;
            case "h":
                return 65;
            case "i":
                return 60;
            case "j":
                return 55;
            case "k":
                return 50;
            case "l":
                return 45;
            case "m":
                return 40;
            default:
                return 0;
        }
    }

    public int convertToNumber(@NotNull String item) {
        switch (item) {
            case "a":
                return 1;
            case "b":
                return 2;
            case "c":
                return 3;
            case "d":
                return 4;
            case "e":
                return 5;
            case "f":
                return 6;
            case "g":
                return 7;
            case "h":
                return 8;
            case "i":
                return 9;
            case "j":
                return 10;
            case "k":
                return 11;
            case "l":
                return 12;
            case "m":
                return 13;
            case "n":
                return 14;
            case "o":
                return 15;
            case "p":
                return 16;
            case "q":
                return 17;
            case "r":
                return 18;
            case "s":
                return 19;
            default:
                return 0;
        }
    }

    public int convertSet(char item) {
        switch (item) {
            case 'a':
                return 1;
            case 'b':
                return 2;
            case 'c':
                return 3;
            case 'd':
                return 4;
            case 'e':
                return 5;
            case 'f':
                return 6;
            case 'g':
                return 7;
            default:
                return 0;
        }
    }

    public int convertMealAmount(@NotNull String item) {
        switch (item) {
            case "a":
                return 1;
            case "b":
                return 2;
            case "c":
                return 3;
            case "d":
                return 4;
            case "e":
                return 5;
            case "f":
                return 6;
            case "g":
                return 7;
            case "h":
                return 8;
            case "i":
                return 9;
            case "j":
                return 10;
            case "k":
                return 11;
            case "l":
                return 12;
            case "m":
                return 13;
            case "n":
                return 14;
            case "o":
                return 15;
            case "p":
                return 16;
            case "q":
                return 17;
            case "r":
                return 18;
            case "s":
                return 19;
            case "t":
                return 20;
            case "u":
                return 21;
            case "v":
                return 22;
            case "w":
                return 23;
            case "x":
                return 24;
            case "y":
                return 25;
            case "z":
                return 26;
            case "A":
                return 27;
            case "B":
                return 28;
            case "C":
                return 29;
            case "D":
                return 30;
            case "E":
                return 50;
            case "F":
                return 100;
            case "G":
                return 150;
            case "H":
                return 200;
            case "I":
                return 250;
            case "J":
                return 300;
            case "K":
                return 350;
            case "L":
                return 400;
            case "M":
                return 500;
            case "N":
                return 600;
            case "O":
                return 700;
            case "P":
                return 800;
            case "Q":
                return 900;
            case "R":
                return 1000;
            case "S":
                return 1500;
            case "T":
                return 2000;
            case "U":
                return 2500;
            case "V":
                return 3000;
            case "W":
                return 3500;
            case "X":
                return 4000;
            case "Y":
                return 4500;
            case "Z":
                return 5000;
            case "ا":
                return 5500;
            case "ب":
                return 6000;
            default:
                return 0;
        }
    }

}
