package com.e.pcoach.network;

import javax.inject.Inject;

import saman.zamani.persiandate.PersianDate;

public class ServerDateConverter {

    @Inject
    public ServerDateConverter() {
    }

    private static final String TAG = "ServerDateConverter";

    public int[] convertDateToPersianDate(String date) {
        String enDay = date.substring(8, 10);
        String enMonth = date.substring(5, 7);
        String enYear = date.substring(0, 4);
        if (enMonth.charAt(0) == '0')
            enMonth = String.valueOf(enMonth.charAt(1));
        if (enDay.charAt(0) == '0')
            enDay = String.valueOf(enDay.charAt(1));
        int[] ints = new PersianDate().toJalali(Integer.parseInt(enYear),
                Integer.parseInt(enMonth), Integer.parseInt(enDay));
        return ints;
    }

}
