package com.e.pcoach.di.getinfo;

import com.e.pcoach.ui.getinfo.fragments.AllergenicFragment;
import com.e.pcoach.ui.getinfo.fragments.BodyTypeFragment;
import com.e.pcoach.ui.getinfo.fragments.ExerciseTargetFragment;
import com.e.pcoach.ui.getinfo.fragments.GeneralInfoFragment;
import com.e.pcoach.ui.getinfo.fragments.PlanningFragment;
import com.e.pcoach.ui.getinfo.fragments.loading.LoadingFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class GetInfoFragmentBuilderModule {

    /**
     * this is a place to add every fragment
     */

    @ContributesAndroidInjector()
    abstract GeneralInfoFragment contributeGeneralInfoFragment();

    @ContributesAndroidInjector
    abstract BodyTypeFragment contributeBodyStyleFragment();

    @ContributesAndroidInjector
    abstract ExerciseTargetFragment contributeExerciseTargetFragment();

    @ContributesAndroidInjector
    abstract AllergenicFragment contributeAllergenicFragment();

    @ContributesAndroidInjector
    abstract PlanningFragment contributePlanningFragment();

    @ContributesAndroidInjector
    abstract LoadingFragment contributeLoadingFragment();

}