package com.e.pcoach.di;

import com.e.pcoach.di.first.FirstScope;
import com.e.pcoach.di.first.FirstTimeFragmentBuilderModule;
import com.e.pcoach.di.first.FirstTimeModule;
import com.e.pcoach.di.getinfo.GetInfoFragmentBuilderModule;
import com.e.pcoach.di.getinfo.GetInfoModule;
import com.e.pcoach.di.getinfo.GetInfoScope;
import com.e.pcoach.di.main.MainFragmentBuilderModule;
import com.e.pcoach.di.main.MainModule;
import com.e.pcoach.di.main.MainScope;
import com.e.pcoach.di.main.TodayProgramsFragmentModule;
import com.e.pcoach.ui.firsttime.FirstTimeActivity;
import com.e.pcoach.ui.getinfo.GetInformationActivity;
import com.e.pcoach.ui.main.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilderModule {
    /**
     * this class is a place to add all activities in application
     */

    @MainScope
    @ContributesAndroidInjector(
            modules = {
                    MainFragmentBuilderModule.class,
                    MainModule.class,
                    TodayProgramsFragmentModule.class

//                    FragmentModule.class,
            })
    abstract MainActivity contributeMainActivity();

    @FirstScope
    @ContributesAndroidInjector(
            modules = {
                    FirstTimeFragmentBuilderModule.class,
                    FirstTimeModule.class
            }
    )
    abstract FirstTimeActivity contributeFirstTimeActivity();

    @GetInfoScope
    @ContributesAndroidInjector(
            modules = {
                    GetInfoFragmentBuilderModule.class,
                    GetInfoModule.class
            }
    )
    abstract GetInformationActivity contributeGetInformationActivity();

}
