package com.e.pcoach.di.getinfo;


import com.e.pcoach.algorithm.HandleGetInfo;
import com.e.pcoach.algorithm.ProgramDbInteraction;
import com.e.pcoach.ui.getinfo.GetInformationActivity;
import com.e.pcoach.ui.getinfo.fragments.AllergenicFragment;
import com.e.pcoach.ui.getinfo.fragments.BodyTypeFragment;
import com.e.pcoach.ui.getinfo.fragments.ExerciseTargetFragment;
import com.e.pcoach.ui.getinfo.fragments.GeneralInfoFragment;
import com.e.pcoach.ui.getinfo.fragments.PlanningFragment;
import com.e.pcoach.ui.getinfo.fragments.bottomsheet.GiModelCaster;
import com.e.pcoach.ui.getinfo.fragments.bottomsheet.InfoBottomSheetFragment;
import com.e.pcoach.ui.getinfo.fragments.loading.LoadingFragment;
import com.e.pcoach.ui.helper.DarkModePrefManager;
import com.e.pcoach.ui.helper.ThemeChangeHelper;
import com.e.pcoach.ui.main.MainActivity;
import com.e.pcoach.ui.main.adapter.bottomnavigation.PagerSaveStateHelpAdapter;

import dagger.Module;
import dagger.Provides;

@Module
public class GetInfoModule {

    @Provides
    static PagerSaveStateHelpAdapter provideInfoBottomBarAdapter(GetInformationActivity mainActivity) {
        return new PagerSaveStateHelpAdapter(mainActivity.getSupportFragmentManager());
    }

    @Provides
    static GeneralInfoFragment provideGeneralInfoFragment() {
        return new GeneralInfoFragment();
    }

    @Provides
    static BodyTypeFragment provideBodyStyleFragment() {
        return new BodyTypeFragment();
    }

    @Provides
    static ExerciseTargetFragment provideExerciseTargetFragment() {
        return new ExerciseTargetFragment();
    }

    @Provides
    static AllergenicFragment provideAllergenicFragment() {
        return new AllergenicFragment();
    }

    @Provides
    static PlanningFragment providePlanningFragment() {
        return new PlanningFragment();
    }

    @Provides
    static LoadingFragment provideLoadingFragment() {
        return new LoadingFragment();
    }

    @Provides
    static InfoBottomSheetFragment provideInfoBottomSheetFragment
            (GetInformationActivity getInformationActivity) {
        return new InfoBottomSheetFragment(getInformationActivity);
    }

    @Provides
    static GiModelCaster provideGiModelCaster() {
        return new GiModelCaster();
    }

    @Provides
    static ProgramDbInteraction provideProgramDbInteraction(GiModelCaster giModelCaster) {
        return new ProgramDbInteraction(giModelCaster);
    }

    @Provides
    static HandleGetInfo provideHandleGetInfo(ProgramDbInteraction programDbInteraction) {
        return new HandleGetInfo(programDbInteraction);
    }

    @Provides
    static ThemeChangeHelper provideThemeChangeHelper(GetInformationActivity context, DarkModePrefManager sharedPref) {
        return new ThemeChangeHelper(context, sharedPref);
    }

}
