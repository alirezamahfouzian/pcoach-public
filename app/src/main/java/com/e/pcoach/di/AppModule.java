package com.e.pcoach.di;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.e.pcoach.libraries.persiandatepicker.util.PersianCalendar;
import com.e.pcoach.network.retrofit.Constants;
import com.e.pcoach.ui.helper.ActivityHelper;
import com.e.pcoach.ui.helper.DarkModePrefManager;
import com.e.pcoach.ui.main.adapter.customtabchanger.CustomTabChanger;
import com.e.pcoach.ui.main.model.WorkoutDate;
import com.e.pcoach.ui.main.model.WorkoutDaysModel;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class AppModule {

    /**
     * this class is a place to add every application level Objects
     */

    @Singleton
    @Provides
    static Retrofit provideRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Singleton
    @Provides
    static ActivityHelper.Helper provideActivityHelper() {
        return new ActivityHelper.Helper();
    }

    @Singleton
    @Provides
    static SharedPreferences provideSharedPreferences(BaseApplication baseApplication) {
        return PreferenceManager.getDefaultSharedPreferences(baseApplication);
    }

    @Provides
    static Bundle provideBundle() {
        return new Bundle();
    }

    @Singleton
    @Provides
    static WorkoutDaysModel provideWorkoutDaysModel() {
        return new WorkoutDaysModel();
    }

    @Singleton
    @Provides
    static DarkModePrefManager provideDarkModePrefManager(BaseApplication baseApplication) {
        return new DarkModePrefManager(baseApplication);
    }

    @Provides
    static CustomTabChanger provideCustomTabChanger() {
        return new CustomTabChanger();
    }

}

