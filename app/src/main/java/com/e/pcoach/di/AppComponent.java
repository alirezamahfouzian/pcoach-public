package com.e.pcoach.di;


import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(
        modules = {
                AndroidSupportInjectionModule.class,
                ActivityBuilderModule.class,
                AppModule.class,
//                ViewModelFactoryModule.class,
        }
)
public interface AppComponent extends AndroidInjector<BaseApplication> {
    /**
     * this class is a place to add every application level Parts
     */

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(BaseApplication baseApplication);

        AppComponent Build();
    }
}
