package com.e.pcoach.di.main;

import com.e.pcoach.ui.main.history.HistoryFragment;
import com.e.pcoach.ui.main.history.details.DetailsHistoryFragment;
import com.e.pcoach.ui.main.history.details.MealDetailsFragment;
import com.e.pcoach.ui.main.history.details.WorkoutDetailsFragment;
import com.e.pcoach.ui.main.home.HomeFragment;
import com.e.pcoach.ui.main.profile.ProfileFragment;
import com.e.pcoach.ui.main.programs.ProgramsFragment;
import com.e.pcoach.ui.main.programs.meal.ProgramMealFragment;
import com.e.pcoach.ui.main.home.programtoday.TodayProgramMealFragment;
import com.e.pcoach.ui.main.home.programtoday.TodayProgramWorkoutFragment;
import com.e.pcoach.ui.main.home.programtoday.TodayProgramsFragment;
import com.e.pcoach.ui.main.programs.workout.ProgramWorkoutFragment;
import com.e.pcoach.ui.main.question.QuestionFragment;
import com.e.pcoach.ui.main.tools.ToolsFragment;
import com.e.pcoach.ui.main.subscription.SubscriptionFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MainFragmentBuilderModule {

    /**
     * this is a place to add every fragment
     */
    @ContributesAndroidInjector(
            modules = {
                    HomeFragmentModule.class,
            })
    abstract HomeFragment contributeHomeFragment();

    @ContributesAndroidInjector
    abstract ProgramsFragment contributeProgramsFragment();

    @ContributesAndroidInjector
    abstract ProgramMealFragment contributeProgramMealFragment();

    @ContributesAndroidInjector
    abstract ProgramWorkoutFragment contributeProgramWorkoutFragment();

    @ContributesAndroidInjector()
    abstract TodayProgramsFragment contributeTodayProgramsFragment();

    @ContributesAndroidInjector
    abstract TodayProgramWorkoutFragment contributeTodayProgramWorkoutFragment();

    @ContributesAndroidInjector
    abstract TodayProgramMealFragment contributeTodayProgramMealFragment();

    @ContributesAndroidInjector
    abstract ToolsFragment contributeToolsFragment();

    @ContributesAndroidInjector
    abstract ProfileFragment contributeProfileFragment();

    @ContributesAndroidInjector
    abstract SubscriptionFragment contributeSubscriptionFragment();

    @ContributesAndroidInjector
    abstract QuestionFragment contributeQuestionFragment();

    @ContributesAndroidInjector
    abstract HistoryFragment contributeHistoryFragment();

    @ContributesAndroidInjector
    abstract DetailsHistoryFragment contributeDetailsHistoryFragment();

    @ContributesAndroidInjector
    abstract MealDetailsFragment contributeMealDetailsFragment();

    @ContributesAndroidInjector
    abstract WorkoutDetailsFragment contributeWorkoutDetailsFragment();
}