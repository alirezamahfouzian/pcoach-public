package com.e.pcoach.di.main;

import androidx.appcompat.app.AppCompatActivity;

import com.e.pcoach.libraries.persiandatepicker.util.PersianCalendar;
import com.e.pcoach.network.ServerDateConverter;
import com.e.pcoach.ui.helper.DarkModePrefManager;
import com.e.pcoach.ui.helper.MealTypeCaster;
import com.e.pcoach.ui.helper.ThemeChangeHelper;
import com.e.pcoach.ui.main.MainActivity;
import com.e.pcoach.ui.main.adapter.bottomnavigation.PagerSaveStateHelpAdapter;
import com.e.pcoach.ui.main.adapter.bottomnavigation.TodayPagerSaveStateHelpAdapter;
import com.e.pcoach.ui.main.history.details.DetailsHistoryFragment;
import com.e.pcoach.ui.main.history.details.MealDetailsFragment;
import com.e.pcoach.ui.main.history.details.WorkoutDetailsFragment;
import com.e.pcoach.ui.main.home.HomeFragment;
import com.e.pcoach.ui.main.home.programdate.DateHandler;
import com.e.pcoach.ui.main.home.programtoday.TodayProgramMealFragment;
import com.e.pcoach.ui.main.home.programtoday.TodayProgramWorkoutFragment;
import com.e.pcoach.ui.main.home.programtoday.TodayProgramsFragment;
import com.e.pcoach.ui.main.model.WorkoutDate;
import com.e.pcoach.ui.main.profile.ProfileFragment;
import com.e.pcoach.ui.main.programs.ProgramsFragment;
import com.e.pcoach.ui.main.programs.adapter.ProgramDayRec;
import com.e.pcoach.ui.main.programs.adapter.WorkoutRecAdapter;
import com.e.pcoach.ui.main.programs.meal.ProgramMealFragment;
import com.e.pcoach.ui.main.programs.workout.ProgramWorkoutFragment;
import com.e.pcoach.ui.main.splash.SplashFragment;
import com.e.pcoach.ui.main.tools.ToolsFragment;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Module
public class MainModule {

    @MainScope
    @Provides
    static SplashFragment provideSplashFragment() {
        return new SplashFragment();
    }

    @MainScope
    @Provides
    static HomeFragment provideHomeFragment() {
        return new HomeFragment();
    }

    @MainScope
    @Provides
    static ToolsFragment provideStatsFragment() {
        return new ToolsFragment();
    }

    @Provides
    static ProgramsFragment provideProgramsFragment() {
        return new ProgramsFragment();
    }

    @Provides
    static ProgramMealFragment provideProgramMealFragment() {
        return new ProgramMealFragment();
    }

    @Provides
    static ProgramWorkoutFragment provideProgramWorkoutFragment() {
        return new ProgramWorkoutFragment();
    }

    @Provides
    static ProfileFragment provideProfileFragment() {
        return new ProfileFragment();
    }

    @Provides
    static TodayProgramsFragment provideTodayProgramsFragment() {
        return new TodayProgramsFragment();
    }

    @Provides
    static TodayProgramMealFragment provideTodayProgramMealFragment() {
        return new TodayProgramMealFragment();
    }

    @Provides
    static TodayProgramWorkoutFragment provideTodayProgramWorkoutFragment() {
        return new TodayProgramWorkoutFragment();
    }

    @Provides
    static WorkoutDetailsFragment provideWorkoutDetailsFragment() {
        return new WorkoutDetailsFragment();
    }

    @Provides
    static MealDetailsFragment provideMealDetailsFragment() {
        return new MealDetailsFragment();
    }

    @Provides
    static CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    static PagerSaveStateHelpAdapter provideBottomBarAdapter(MainActivity mainActivity) {
        return new PagerSaveStateHelpAdapter(mainActivity.getSupportFragmentManager());
    }

    @Provides
    static TodayPagerSaveStateHelpAdapter provideTodayPagerSaveStateHelpAdapter(MainActivity mainActivity) {
        return new TodayPagerSaveStateHelpAdapter(mainActivity.getSupportFragmentManager());
    }

    @MainScope
    @Provides
    static MealTypeCaster provideMealTypeCaster() {
        return new MealTypeCaster();
    }

    @MainScope
    @Provides
    static WorkoutDate provideWorkoutDate() {
        return new WorkoutDate();
    }

    @Provides
    static ProgramDayRec provideProgramDateRec(MainActivity mainActivity) {
        return new ProgramDayRec(mainActivity);
    }

    @Provides
    static WorkoutRecAdapter provideWorkoutRecAdapter(MainActivity mainActivity) {
        return new WorkoutRecAdapter(mainActivity);
    }

    @Provides
    static DetailsHistoryFragment provideDetailsHistoryFragment() {
        return new DetailsHistoryFragment();
    }

    @MainScope
    @Provides
    static DateHandler provideDateHandler(PersianCalendar calendar) {
        return new DateHandler(calendar);
    }

    @MainScope
    @Provides
    static PersianCalendar providePersianCalender() {
        return new PersianCalendar();
    }

    @Provides
    static ServerDateConverter provideServerDateConverter() {
        return new ServerDateConverter();
    }

    @Provides
    static ThemeChangeHelper provideThemeChangeHelper(MainActivity context, DarkModePrefManager sharedPref) {
        return new ThemeChangeHelper(context, sharedPref);
    }

}
