package com.e.pcoach.di.first;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.e.pcoach.R;
import com.e.pcoach.network.retrofit.auth.AuthApi;
import com.e.pcoach.ui.firsttime.FirstTimeActivity;
import com.e.pcoach.ui.firsttime.intro.MyPagerAdapter;
import com.e.pcoach.ui.firsttime.login.LoginFragment;
import com.e.pcoach.ui.firsttime.signup.SignUpFragment;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class FirstTimeModule {

    @FirstScope
    @Provides
    static LoginFragment providesLoginFragment() {
        return new LoginFragment();
    }

    @FirstScope
    @Provides
    static SignUpFragment providesSignUpFragment() {
        return new SignUpFragment();
    }

    @FirstScope
    @Provides
    static int[] ProvideIntroSliderlayouts() {
        return new int[]{R.layout.slider_page_one, R.layout.slider_page_two,
                R.layout.slider_page_three, R.layout.slider_page_four};
    }

    @FirstScope
    @Provides
    static MyPagerAdapter ProvidePagerAdapter(int[] layouts, FirstTimeActivity context) {
        return new MyPagerAdapter(layouts, context);
    }

    @FirstScope
    @Provides
    static AuthApi ProvideAuthApi(Retrofit retrofit) {
        return retrofit.create(AuthApi.class);
    }

}
