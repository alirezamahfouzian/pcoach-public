package com.e.pcoach.di.main;

import com.e.pcoach.libraries.persiandatepicker.util.PersianCalendar;
import com.e.pcoach.ui.main.MainActivity;
import com.e.pcoach.ui.main.adapter.customtabchanger.CustomTabChanger;
import com.e.pcoach.ui.main.home.datepicker.BottomSheetFragment;
import com.e.pcoach.ui.main.home.workout.HomeWorkoutRecAdapter;
import com.e.pcoach.ui.main.model.WorkoutDate;

import dagger.Module;
import dagger.Provides;

@Module
class HomeFragmentModule {

    @Provides
    static BottomSheetFragment provideBottomSheetFragment
            (MainActivity activity, PersianCalendar leapCalender, WorkoutDate date) {
        return new BottomSheetFragment(activity, leapCalender, date);
    }

    @Provides
    static HomeWorkoutRecAdapter provideHomeWorkoutRecyclerViewAdapter(MainActivity activity) {
        return new HomeWorkoutRecAdapter(activity);
    }

}
