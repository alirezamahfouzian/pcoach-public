package com.e.pcoach.di.first;

import com.e.pcoach.ui.firsttime.login.LoginFragment;
import com.e.pcoach.ui.firsttime.signup.SignUpFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FirstTimeFragmentBuilderModule {

    /**
     * this is a place to add every fragment
     */

    @ContributesAndroidInjector()
    abstract LoginFragment contributeLoginFragment();

    @ContributesAndroidInjector
    abstract SignUpFragment contributeSignUpFragment();

}