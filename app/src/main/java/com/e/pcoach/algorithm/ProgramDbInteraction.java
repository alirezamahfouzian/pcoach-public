package com.e.pcoach.algorithm;

import android.annotation.SuppressLint;
import android.util.Log;

import com.e.pcoach.db.entities.DietProgramEntity;
import com.e.pcoach.db.entities.ProgramsEntity;
import com.e.pcoach.db.entities.UserGeneralInfoEntity;
import com.e.pcoach.db.entities.WorkoutProgramEntity;
import com.e.pcoach.ui.getinfo.GetInfoViewModel;
import com.e.pcoach.ui.getinfo.GetInformationActivity;
import com.e.pcoach.ui.getinfo.fragments.bottomsheet.GiModel;
import com.e.pcoach.ui.getinfo.fragments.bottomsheet.GiModelCaster;
import com.e.pcoach.ui.main.programs.ProgramViewModel;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

@SuppressLint("CheckResult")
public class ProgramDbInteraction {

    private GiModelCaster mModelCaster;
    private GetInformationActivity mActivity;
    private GetInfoViewModel mGetInfoViewModel;
    private ProgramViewModel mProgramViewModel;
    private Integer mLastProgramId = 0;

    @Inject
    public ProgramDbInteraction(GiModelCaster modelCaster) {
        mModelCaster = modelCaster;
    }

    public ProgramDbInteraction build(GetInformationActivity activity, GetInfoViewModel viewModel
            , ProgramViewModel programViewModel) {
        mActivity = activity;
        mGetInfoViewModel = viewModel;
        mProgramViewModel = programViewModel;
        return this;
    }

    public ProgramDbInteraction insertInfoToDb() {
        Observable observable = Observable.create(emitter -> {
            getLastProgram();
            GiModel giModel = mActivity.generalInformation;
            UserGeneralInfoEntity entity = new UserGeneralInfoEntity();
            entity.setProgramId(mLastProgramId + 1);
            entity.setInfoInsertedTime(mModelCaster.getDateTime());
            entity.setAge(giModel.getAge());
            entity.setHeight(giModel.getHeight());
            entity.setWeight(mModelCaster.combineWeight(giModel.getWeightKg(), giModel.getWeightG()));
            entity.setGender(giModel.getGenderType());
            entity.setWrist(mModelCaster.combineWrist(giModel.getWristCm(), giModel.getWristMm()));
            entity.setExperience(giModel.getExperience());
            entity.setActivity(giModel.getActivityType());
            entity.setBodyTypeValues(mModelCaster
                    .castIntegerListToString(mActivity.bodyTypeInformation.getBodyTypeQuestions()));
            entity.setAllergiesValues(mModelCaster
                    .castIntegerListToString(mActivity.allergenicFragmentInformation
                            .getAllergenicFoods()));
            mGetInfoViewModel.insertGeneralInfo(entity);
        })
                .subscribeOn(Schedulers.io());
        observable.subscribe();
        return this;
    }

    public ProgramDbInteraction insertProgramToDb() {
//        Observable.create(emitter -> {
//            getLastProgram();
//            ProgramsEntity entity = new ProgramsEntity();
//            // program
//            entity.setUserId(1);
//            entity.setProgramGoal(mActivity.goalType);
//            entity.setIsEnabled(ProgramState.ENABLE);
//            entity.setProgramDay(mModelCaster
//                    .extractSelectedDays(mActivity.planingTimeInformation
//                            .getDayStatues()));
//            entity.setProgramMinute(mActivity.generalInformation.getTrainingMin());
//            entity.setProgramCreatedTime(mModelCaster.getDateTime());
//            entity.setProgramTitle("برنامه شماره1");
//            mProgramViewModel.insertProgram(entity);
//            // diet
//            DietProgramEntity dietEntity = new DietProgramEntity();
//            dietEntity.setProgramId(mLastProgramId + 1);
//            dietEntity.setMeals("ffkeekvhdkbmcdd");
//            dietEntity.setMealAmount("adeyffervvcdsfh");
//            dietEntity.setMealTypesOrder("oriopiotpro");
//            dietEntity.setMealDayPositions(0);
//            mProgramViewModel.insertDietProgram(dietEntity);
//            dietEntity.setMealDayPositions(2);
//            mProgramViewModel.insertDietProgram(dietEntity);
//            dietEntity.setMealDayPositions(4);
//            mProgramViewModel.insertDietProgram(dietEntity);
//            // workout
//            WorkoutProgramEntity workoutEntity = new WorkoutProgramEntity();
//            workoutEntity.setMoves("fdsfsdfdsfdsfds");
//            workoutEntity.setProgramId(mLastProgramId + 1);
//            workoutEntity.setWorkoutPercentage("gftrerthcbxzxx");
//            workoutEntity.setWorkoutRep("ohlkbikrpkvvkf");
//            workoutEntity.setWorkoutSet("faewetrghvbncvx");
//            workoutEntity.setWorkoutDayPositions(0);
//            mProgramViewModel.insertWorkoutProgram(workoutEntity);
//            workoutEntity.setWorkoutDayPositions(1);
//            mProgramViewModel.insertWorkoutProgram(workoutEntity);
//            workoutEntity.setWorkoutDayPositions(2);
//            mProgramViewModel.insertWorkoutProgram(workoutEntity);
//        })
//                .subscribeOn(Schedulers.io())
//                .subscribe();
        return this;
    }

    public ProgramDbInteraction showInformation() {
        mGetInfoViewModel.getAllGeneralInfo().subscribe(new Consumer<List<UserGeneralInfoEntity>>() {
            @Override
            public void accept(List<UserGeneralInfoEntity> generalInfoEntities) throws Exception {
                for (UserGeneralInfoEntity generalInfo : generalInfoEntities) {
                    Log.d("informationssssss", "\n\n generalId  |  proid  |  age  |  height  |  weight  |  wrist  |  gender  |  experience  |  mActivity  |  mActivity  |  body type values  |  allergies\n"
                            + generalInfo.getUserGeneralInfoId() + "  |  "
                            + generalInfo.getProgramId() + "  |  "
                            + generalInfo.getAge() + "  |  "
                            + generalInfo.getHeight() + "  |  "
                            + generalInfo.getWeight() + "  |  "
                            + generalInfo.getWrist() + "  |  "
                            + generalInfo.getGender() + "  |  "
                            + generalInfo.getExperience() + "  |  "
                            + generalInfo.getActivity() + "  |  "
                            + generalInfo.getBodyTypeValues() + "  |  "
                            + generalInfo.getAllergiesValues() + "  |  "
                    );
                }
            }
        });
        return this;
    }

    public ProgramDbInteraction showProgram() {
//        mProgramViewModel.getAllPrograms().subscribe(programsEntities -> {
//            for (ProgramsEntity programsEntity : programsEntities) {
//                Log.d("programsssss", "programs \n"
//                        + programsEntity.getProgramId() + "  |  "
//                        + programsEntity.getProgramTitle() + "  |  "
//                        + programsEntity.getProgramMinute() + "  |  "
//                        + programsEntity.getProgramCreatedTime() + "  |  "
//                        + programsEntity.getProgramGoal() + "  |  "
//                        + programsEntity.getIsEnabled() + "  |  "
//                );
//            }
//        });
//        mProgramViewModel.getAllDietPrograms().subscribe(dietProgramEntities -> {
//            for (DietProgramEntity dietEntity : dietProgramEntities) {
//                Log.d("informationssssss", "diet \n"
//                        + dietEntity.getDietProgramId() + "  |  "
//                        + dietEntity.getProgramId() + "  |  "
//                        + dietEntity.getMealId() + "  |  "
//                        + dietEntity.getMealAmounts() + "  |  "
//                        + dietEntity.getMealType() + "  |  "
//                        + dietEntity.getMealDayPositions() + "  |  "
//                );
//            }
//
//        });
        mProgramViewModel.getAllWorkoutPrograms().subscribe(workoutProgramEntities -> {
            for (WorkoutProgramEntity workoutEntity : workoutProgramEntities) {
                Log.d("workouttttt", "workout \n"
                        + workoutEntity.getWorkoutProgramId() + "  |  "
                        + workoutEntity.getProgramId() + "  |  "
                        + workoutEntity.getMoveId() + "  |  "
                        + workoutEntity.getWorkoutRep() + "  |  "
                        + workoutEntity.getWorkoutSet() + "  |  "
                        + workoutEntity.getWorkoutPercentage() + "  |  "
                        + workoutEntity.getWorkoutDayPosition() + "  |  "
                );
            }
        });
        return this;
    }

    private void getLastProgram() {
        mProgramViewModel.getLastProgramId().subscribe(new Consumer<Integer>() {
            @Override
            public void accept(Integer integer) throws Exception {
                mLastProgramId = integer;
            }
        });
    }

}
