package com.e.pcoach.algorithm;

import com.e.pcoach.ui.getinfo.GetInfoViewModel;
import com.e.pcoach.ui.getinfo.GetInformationActivity;
import com.e.pcoach.ui.getinfo.fragments.bottomsheet.GiModelCaster;
import com.e.pcoach.ui.main.programs.ProgramViewModel;

import javax.inject.Inject;

public class HandleGetInfo {

    @Inject
    GiModelCaster modelCaster;
    private ProgramDbInteraction mProgramDbInteraction;
    private GetInformationActivity mActivity;
    private GetInfoViewModel mGetInformationViewModel;
    private ProgramViewModel mProgramViewModel;

    @Inject
    public HandleGetInfo(ProgramDbInteraction programDbInteraction) {
        mProgramDbInteraction = programDbInteraction;
    }

    public void build(GetInformationActivity activity,
                      GetInfoViewModel getInformationViewModel,
                      ProgramViewModel programViewModel) {
        mActivity = activity;
        mGetInformationViewModel = getInformationViewModel;
        mProgramViewModel = programViewModel;
        mProgramDbInteraction.build(mActivity, mGetInformationViewModel, mProgramViewModel);
        generateFakeProgram();
        saveInformation();
    }

    private void generateFakeProgram() {
        mProgramDbInteraction
                .insertProgramToDb()
                .showProgram();
    }

    private void saveInformation() {
        mProgramDbInteraction.insertInfoToDb()
                .showInformation();
    }

    public void showProgram() {
        mProgramDbInteraction.showInformation();
    }

}
