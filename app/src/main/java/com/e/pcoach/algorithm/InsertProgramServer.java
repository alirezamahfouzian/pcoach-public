package com.e.pcoach.algorithm;

import android.annotation.SuppressLint;
import android.util.Log;

import com.e.pcoach.db.entities.DietProgramEntity;
import com.e.pcoach.db.entities.ProgramsEntity;
import com.e.pcoach.db.entities.WorkoutProgramEntity;
import com.e.pcoach.network.DietProgramServer;
import com.e.pcoach.network.ProgramConverter;
import com.e.pcoach.network.ProgramsServer;
import com.e.pcoach.network.WorkoutProgramServer;
import com.e.pcoach.ui.main.programs.ProgramViewModel;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class InsertProgramServer {

    private static final String TAG = "InsertProgramServer";
    private ProgramConverter mProgramConverter;
    private CompositeDisposable mDisposable;
    private ProgramViewModel mProgramViewModel;
    private int mProgramId;
    // workout
    private int mWorkoutEndP;
    private String mMoves;
    private String mSets;
    private String mReps;
    private String mPercentages;
    private char[] mWorkoutProgramOrders;
    private int mWorkoutOrderIndex = 0;
    private int mWorkoutCount = 0;
    private int mWorkoutOrderCount = 0;
    private char[] mWorkoutDayPositions;
    private int wI = 0;
    // diet
    private int mDietEndP;
    private String mMeals;
    private String mAmount;
    private char[] mDietDayOrderList;
    private char[] mMealTypesOrderList;
    private char[] mMealDayPositionsList;
    private int mDietOrderIndex = 0;
    private int mMealTypeIndex = 0;
    private int mMealTypeOrderIndex = 0;
    private int mDietAddedCount = 0;
    private int mDietTypeOrderCount = 0;
    private int mDietDayOrder = 0;
    private int mMealTypeOrderCount = 0;
    private int dI = 0;

    public InsertProgramServer(CompositeDisposable disposable, ProgramViewModel programViewModel) {
        mDisposable = disposable;
        mProgramViewModel = programViewModel;
        mProgramConverter = new ProgramConverter();

    }

    /**
     * like #insertServerDiet
     */
    public void insertServerProgram(ProgramsServer programsServer) {
        Observable
                .create(new ObservableOnSubscribe<Object>() {
                    @Override
                    public void subscribe(ObservableEmitter<Object> emitter) throws Exception {
                        ProgramsEntity entity = new ProgramsEntity();
                        entity.setIsEnabled(programsServer.getIsEnabled());
                        entity.setProgramCreatedTime(programsServer.getProgramCreatedTime());
                        entity.setProgramStartTime(programsServer.getProgramStartTime());
                        entity.setProgramEndTime(programsServer.getProgramEndTime());
                        entity.setProgramGoal(programsServer.getProgramGoal());
                        entity.setProgramMinute(programsServer.getProgramMinute());
                        entity.setProgramTitle(programsServer.getProgramTitle());
                        entity.setUserId(programsServer.getUserId());
                        entity.setProgramDietDays("012");
                        entity.setProgramWorkoutDays("135");
                        mProgramViewModel.insertProgram(entity);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Object>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        mDisposable.add(d);
                    }

                    @Override
                    public void onNext(Object o) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void setWorkoutParts(WorkoutProgramServer workoutServer) {
        mProgramId = workoutServer.getProgramId();
        mMoves = workoutServer.getMoves();
        mSets = workoutServer.getWorkoutSets();
        mReps = workoutServer.getWorkoutReps();
        mPercentages = workoutServer.getWorkoutPercentages();
        mWorkoutDayPositions = workoutServer.getWorkoutDayPositions().toCharArray();
        mWorkoutProgramOrders = workoutServer.getWorkoutProgramOrders().toCharArray();
    }

    /**
     * like #insertServerDiet
     */
    public void insertServerWorkout(WorkoutProgramServer workoutServer) {
        setWorkoutParts(workoutServer);
        mWorkoutEndP = (mMoves.length() / 2);
        mWorkoutOrderCount = mProgramConverter.convertToNumber(String.valueOf(mWorkoutProgramOrders[mWorkoutOrderIndex]));
        Disposable disposable = Observable
                .range(0, mWorkoutEndP)
                .map(new Function<Integer, Integer>() {
                    @Override
                    public Integer apply(Integer item) throws Exception {
                        WorkoutProgramEntity entity = new WorkoutProgramEntity();
                        entity.setProgramId(mProgramId);
                        entity.setMoveId(mMoves.substring(wI, wI + 2));
                        entity.setWorkoutSet(Integer.parseInt(String.valueOf(mSets.charAt(item))));
                        entity.setWorkoutPercentage(mProgramConverter
                                .convertPercentage(String.valueOf(mPercentages.charAt(item))));
                        entity.setWorkoutRep(mProgramConverter.convertToNumber(String.valueOf(mReps.charAt(item))));
                        wI = wI + 2;
                        if (mWorkoutCount == mWorkoutOrderCount) {
                            mWorkoutOrderIndex++;
                            if (mWorkoutOrderIndex != mWorkoutProgramOrders.length - 1) {
                                mWorkoutOrderCount = mWorkoutOrderCount +
                                        mProgramConverter.convertToNumber(String.valueOf(mWorkoutProgramOrders[mWorkoutOrderIndex]));
                            }
                        }
                        mWorkoutCount++;
                        entity.setWorkoutDayPosition(Integer.parseInt(String.valueOf(mWorkoutDayPositions[mWorkoutOrderIndex])));
                        mProgramViewModel.insertWorkoutProgram(entity);
                        return item;
                    }
                })
                .subscribeOn(Schedulers.io())
                .subscribe();
        mDisposable.add(disposable);
    }

    private void setDietParts(DietProgramServer dietServer) {
        mProgramId = dietServer.getProgramId();
        mMeals = dietServer.getMeals();
        mAmount = dietServer.getMealAmounts();
        mDietDayOrderList = dietServer.getMealDayOrder().toCharArray();
        mMealTypesOrderList = dietServer.getMealTypesOrder().toCharArray();
        mMealDayPositionsList = dietServer.getMealDayPositions().toCharArray();
    }

    /**
     * in this algorithm app get a
     * big string (mMeals) and it divide it into two pics of string
     * that is the mealId and meal amount too but it doesn't have two characters like MealId
     * it divides the meal ids based on the day orders so it sets the day position and later
     * it divides them into the mealType so it have both day position and mealType like lunch
     */
    public void insertServerDiet(DietProgramServer dietServer) {
        setDietParts(dietServer);
        mDietEndP = (mMeals.length() / 2);
        mDietDayOrder = mProgramConverter.convertToNumber(String.valueOf(mDietDayOrderList[mDietOrderIndex]));
        mMealTypeOrderCount = Integer.parseInt(String.valueOf(mMealTypesOrderList[mMealTypeOrderIndex]));
        Observable
                .create(new ObservableOnSubscribe<Integer>() {
                    @Override
                    public void subscribe(ObservableEmitter<Integer> emitter) throws Exception {
                        for (int item = 0; item < mDietEndP; item++) {
                            DietProgramEntity entity = new DietProgramEntity();
                            entity.setProgramId(mProgramId);
                            entity.setMealId(mMeals.substring(wI, wI + 2));
                            wI = wI + 2;
                            entity.setMealAmount(mProgramConverter
                                    .convertMealAmount(String.valueOf(mAmount.charAt(item))));
                            if (mDietAddedCount == mDietDayOrder) {
                                mMealTypeIndex = -1;
                                mDietAddedCount = 0;
                                if (mDietOrderIndex + 1 <= mDietDayOrderList.length - 1) {
                                    mDietOrderIndex++;
                                    mDietDayOrder = mProgramConverter.convertToNumber(String
                                            .valueOf(mDietDayOrderList[mDietOrderIndex]));
                                }
                            }
                            entity.setMealDayPosition(Integer.parseInt(String
                                    .valueOf(mMealDayPositionsList[mDietOrderIndex])));
                            mDietAddedCount++;
                            if (mDietTypeOrderCount == mMealTypeOrderCount) {
                                mDietTypeOrderCount = 0;
                                mMealTypeIndex++;
                                mMealTypeOrderIndex++;
                                if (mMealTypeIndex == 4) {
                                    mMealTypeIndex = 0;
                                }
                                if (mMealTypeIndex <= mMealTypesOrderList.length - 1) {
                                    mMealTypeOrderCount = Integer.parseInt(String
                                            .valueOf(mMealTypesOrderList[mMealTypeOrderIndex]));
                                }
                            }
                            mDietTypeOrderCount++;
                            entity.setMealType(mMealTypeIndex);
                            mProgramViewModel.insertDietProgram(entity);
                        }
                    }
                })
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Object>() {
            @Override
            public void onSubscribe(Disposable d) {
                mDisposable.add(d);
            }

            @Override
            public void onNext(Object o) {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void setTheDietData(Integer item) {

    }

}
